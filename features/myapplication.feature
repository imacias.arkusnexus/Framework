#Author: zohaib@onestop.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Feature
Feature: Test Turtle Beach Product Bundle Scenario
  I want to use this template for my feature file

  @PLP
  Scenario: Test product bundle
    Given Open Chrome and go to product detail page on Turtle Beach US site
    When Product bundle loads
    Then User should see two products as one bundle

  @PLP
  Scenario: Test product bundle and checkout
    Given Open Chrome and go to product detail page on Turtle Beach US site
    When Product bundle loads
    Then User checkouts
    