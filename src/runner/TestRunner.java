package runner;

//import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import ConfigFiles.Config;

//@RunWith(Cucumber.class)
@CucumberOptions (tags= "@PLP", features = "features", glue = "TestCases")

public class TestRunner extends AbstractTestNGCucumberTests{
}
