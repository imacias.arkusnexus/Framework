package Scripts;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ConfigFiles.Config;

import TestCases.DLL_QA_Test_Suite;
import TestCases.QA_Test_Suite;

	/**
	 * @author Neha Chowdhry
	 */

	public class DLL_QA_Script extends DLL_QA_Test_Suite {
		
		@Parameters({"username", "key", "site", "env"})
		@BeforeMethod
	    public void setUp(@Optional("onestopqa") String username,
	        @Optional("6ee2c83d-febb-4fc3-83d7-651a4af1c672") String key,
	        @Optional String site, 
	        @Optional String env,
	        Method method) throws Exception, IOException {
			try {
			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			capabilities.setCapability("platform", "Windows 10");
			capabilities.setCapability("version", "38.0");
	        capabilities.setCapability("name", "DLL_" + site + "_" + env);
	        capabilities.setCapability("maxDuration", 10800);
	        capabilities.setCapability("commandTimeout", 600);
	        capabilities.setCapability("acceptSslCerts", true);
	        
	        deleteOldFiles();
	        createLogFile(site + "_" + env + "_");

	        QA_Test_Suite.driver = new RemoteWebDriver(
	                new URL("http://" + username + ":" + key + "@ondemand.saucelabs.com:80/wd/hub"),
	                capabilities);
	        driver.manage().timeouts().pageLoadTimeout(pageLoadTime, TimeUnit.SECONDS);
	        
	        sitename = site;
	        envname = env;
	        SiteURLs = readTestDataFromExcel(siteURLsExcel, 2, sitename);
			} catch (Exception e) {
				captureScreenShot(sitename +"_" + envname + "_");
				captureHeaders(sitename +"_" + envname + "_");
			}
	    }

	    @Test
		public void testcases() throws Exception {
	    	LocatorProps = new Properties();
	    	LocatorProps.load(new FileInputStream(QA_LocatorRep + "DLL_QA.properties"));
	    	browsername = " Browser: Mozilla Firefox 31.0";
			scriptStartTime=System.currentTimeMillis();	
 
			DLL_QA_Test_Suite.HomePageLoad();
			DLL_QA_Test_Suite.GoToBM();	    
		//	DLL_QA_Test_Suite.SearchOrder();	   
			DLL_QA_Test_Suite.OrderInProgress();
			DLL_QA_Test_Suite.OrderShipped();
			DLL_QA_Test_Suite.CancelOrder();
			DLL_QA_Test_Suite.Return_RMA();
			//DLL_QA_Test_Suite.PromoCode();
			DLL_QA_Test_Suite.ProductConfig();
			DLL_QA_Test_Suite.CategoryConfig();
			DLL_QA_Test_Suite.SiteConfig();
			DLL_QA_Test_Suite.ProductOrderConfig();
	    	scriptStatus = "Automation Test Successful";
	    	System.out.println("--> Automation Test Successful");
			APP_LOGS.debug("--> Automation Test Successful");
	    }
	    
	    @AfterMethod
	    public void tearDown() throws Exception {
	    	scriptEndTime=System.currentTimeMillis();
			scriptExecutionTime=(scriptEndTime-scriptStartTime)/1000;
			sExecutionTime=((int)(scriptExecutionTime/60))+" Minute "+ ((int)(scriptExecutionTime%60))+" Seconds";
	    	Config.sendEmail();
	    	System.out.println("Exiting the test");
	    	driver.quit();
	}
	}
