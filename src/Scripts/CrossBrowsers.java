package Scripts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Parameters;


import ConfigFiles.Config;

public class CrossBrowsers extends Config{
	
	private static WebDriver driver;
	
	@Parameters("browser")
	//public void Setup(String browser) throws Exception{
	
		public WebDriver Setup(String browser){
				
		try {
			
			switch(browser) {
				case "chrome":
					System.setProperty("webdriver.chrome.driver", "C:\\Saucelabs_WD_Ilse\\JarFiles\\chromedriver.exe");
					driver = new ChromeDriver();
				break;
				
				case "firefox":
					System.setProperty("webdriver.gecko.driver", "C:\\Saucelabs_WD_Ilse\\JarFiles\\geckodriver (1).exe");	
				    driver = new FirefoxDriver();
				break;
				
				case "opera":
					System.setProperty("webdriver.opera.driver", "C:\\Saucelabs_WD_Ilse\\JarFiles\\operadriver.exe");	
				    driver = new FirefoxDriver();
				break;
				
				default:
					System.out.println("Doesn't exist the browser selected: " + browser );
				break;
			}
			
	        driver.manage().window().maximize(); //Maximize the window.
		}	
	     
		catch(Exception ex) {
			LogInfo(ex.getMessage());
		}
	  
		return driver;
	}
}
