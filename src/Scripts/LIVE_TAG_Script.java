package Scripts;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ConfigFiles.Config;
import TestCases.LIVE_Test_Suite;

	/**
	 * @author Neha Chowdhry
	 */

	public class LIVE_TAG_Script extends LIVE_Test_Suite {
		
		@Parameters({"username", "key", "site", "env"})
		@BeforeMethod
 public void setUp(@Optional("onestopmac") String username,
        @Optional("0b90cc11-6b57-4cd8-b7e6-59d49ee70562") String key,
	        @Optional String site, 
	        @Optional String env,
	        Method method) throws Exception, IOException {
			try {
			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			//capabilities.setCapability("platform", "Windows 8.1");
			capabilities.setCapability("platform", "Windows 7");
			capabilities.setCapability("version", "31");
	        capabilities.setCapability("name", site + "_" + env);
	        capabilities.setCapability("maxDuration", 10800);
	        capabilities.setCapability("commandTimeout", 600);
	        
	        //deleteOldFiles();
	        createLogFile(site + "_" + env + "_");


	        LIVE_Test_Suite.driver = new RemoteWebDriver(
	                new URL("http://" + username + ":" + key + "@ondemand.saucelabs.com:80/wd/hub"),
	                capabilities);
	        driver.manage().timeouts().pageLoadTimeout(pageLoadTime, TimeUnit.SECONDS);
	        
	        sitename = site;
	        envname = env;
	        SiteURLs = readTestDataFromExcel(siteURLsExcel, 1, sitename);
			} catch (Exception e) {
				captureScreenShot(sitename +"_" + envname + "_");
				captureHeaders(sitename +"_" + envname + "_");
			}
	    }
				
		@Test
		public void testcases() throws Exception {
	    	LocatorProps = new Properties();
	    	LocatorProps.load(new FileInputStream(LIVE_LocatorRep + sitename + ".properties"));
	    	browsername = " Browser: Mozilla Firefox 31";
			scriptStartTime=System.currentTimeMillis();
	    	LIVE_Test_Suite.HomePageLoad();
			LIVE_Test_Suite.TagVerification();
			LIVE_Test_Suite.MyAccountSignIn();
	    	LIVE_Test_Suite.GoToPLP();
	    	LIVE_Test_Suite.TagVerification();
	    	LIVE_Test_Suite.GoToPDP();
	    	LIVE_Test_Suite.TagVerification();
	       	scriptStatus = "Automation Test Successful";
	    	System.out.println("--> Automation Test Successful");
			APP_LOGS.debug("--> Automation Test Successful");
	    }
	    
	    @AfterMethod
	    public void tearDown() throws Exception {
	    	scriptEndTime=System.currentTimeMillis();
			scriptExecutionTime=(scriptEndTime-scriptStartTime)/1000;
			sExecutionTime=((int)(scriptExecutionTime/60))+" Minute "+ ((int)(scriptExecutionTime%60))+" Seconds";
	    	Config.sendEmail();
	    	driver.quit();
			}
	}
