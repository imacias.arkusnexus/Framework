package Scripts;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Properties;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ConfigFiles.Config;
import TestCases.LIVE_Test_Suite;

	/**
	 * @author Neha Chowdhry
	 */

	public class Node_Regression_Script extends LIVE_Test_Suite {
		
		@Parameters({"username", "key", "site", "env", "host", "url"})
		@BeforeMethod
 public void setUp(@Optional("onestopqa") String username,
        @Optional("6ee2c83d-febb-4fc3-83d7-651a4af1c672") String key,
	        @Optional String site, 
	        @Optional String env,
	        @Optional String host,
	        @Optional String url,
	        Method method) throws Exception, IOException {
			try {
			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			capabilities.setCapability("platform", "Windows 8.1");
			capabilities.setCapability("version", "31");
	        capabilities.setCapability("name", "NodeTest_" + site);
	        capabilities.setCapability("maxDuration", 10800);
	        capabilities.setCapability("commandTimeout", 600);
	        
	        deleteOldFiles();
	        createLogFile(site + "_" + env + "_");

	        LIVE_Test_Suite.driver = new RemoteWebDriver(
	                new URL("http://" + username + ":" + key + "@ondemand.saucelabs.com:80/wd/hub"),
	                capabilities);
	        sitename = site;
	        envname = env;
	        hostname = host;
	        nodeURLname = url;
			} catch (Exception e) {
				captureScreenShot(sitename +"_" + envname + "_");
				captureHeaders(sitename +"_" + envname + "_");
			}
	    }

	    @Test
		public void testcases() throws Exception {
	    	LocatorProps = new Properties();
	    	LocatorProps.load(new FileInputStream(LIVE_LocatorRep + sitename + ".properties"));
	    	browsername = " Browser: Mozilla Firefox 31.0";
			scriptStartTime=System.currentTimeMillis();
			
			System.out.println("--> Host: " + hostname + " SiteURL: " + nodeURLname);
	        APP_LOGS.debug("--> Host: " + hostname + " SiteURL: " + nodeURLname);
	        Writer writer = null;
	        try{
	        	writer = new PrintWriter(("Host_IP.txt"), "utf-8");
	        writer.write(hostname + " " + nodeURLname);
	        }catch (IOException ex) {
	        	ex.printStackTrace();
	        } finally {
	            writer.close();
	        }
			
			System.out.println("--> Flushing the DNS");
            APP_LOGS.debug("--> Flushing the DNS");
            Process p1 = Runtime.getRuntime().exec("cmd /c ipconfig /flushdns"); 
            p1.waitFor();
            APP_LOGS.debug("--> Executing the batch file");
			System.out.println("--> Executing the batch file");
            APP_LOGS.debug("--> Executing the batch file");
            Process pr = Runtime.getRuntime().exec("C:\\Saucelabs_WD\\EditDNS.bat");
            pr.waitFor();
            Thread.sleep(20000);
            Process p2 = Runtime.getRuntime().exec("cmd /c ipconfig /flushdns"); 
            p2.waitFor();
			
            Thread.sleep(30000);
			System.out.println("--> Navigating to the site");
            APP_LOGS.debug("--> Navigating to the site");
    		SiteURLs=readTestDataFromExcel(siteURLsExcel, 0, sitename);
    		driver.get(SiteURLs.get(1));
			
			Thread.sleep(7000);			
			System.out.println("--> Verifying the machine name");
            APP_LOGS.debug("--> Verifying the machine name");
            driver.get(SiteURLs.get(1) + "/status");
            Thread.sleep(7000);
            String siteNode = returnLocator("NODE_STATUS").getText();
            System.out.println("--> Machine name is: " + siteNode);
            APP_LOGS.debug("--> Machine name is: " + siteNode);
            
            Thread.sleep(20000);
            
	    	LIVE_Test_Suite.HomePageLoad();
	    	/*LIVE_Test_Suite.MyAccountSignIn();
	    	LIVE_Test_Suite.SearchForAProduct();
	    	LIVE_Test_Suite.GoToPLP();
	    	LIVE_Test_Suite.GoToPDP();
	    	LIVE_Test_Suite.SelectProductSize();
	    	LIVE_Test_Suite.AddToCartAndCheckout();
	    	LIVE_Test_Suite.FinalCheckout();*/
			scriptStatus = "Automation Test Successful";
	    	System.out.println("--> Automation Test Successful");
			APP_LOGS.debug("--> Automation Test Successful");
	    }
	    
	    @AfterMethod
	    public void tearDown() throws Exception {
	    	scriptEndTime=System.currentTimeMillis();
			scriptExecutionTime=(scriptEndTime-scriptStartTime)/1000;
			sExecutionTime=((int)(scriptExecutionTime/60))+" Minute "+ ((int)(scriptExecutionTime%60))+" Seconds";
	    	Config.sendEmail();
	    	System.out.println("Exiting the test");
	    	driver.quit();
	}
	}