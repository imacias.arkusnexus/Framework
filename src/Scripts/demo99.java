package Scripts;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;
import java.util.*;
import java.util.NoSuchElementException;

public class demo99 {
 private WebDriver driver;
 private String baseUrl;
 private boolean acceptNextAlert = true;
 private StringBuffer verificationErrors = new StringBuffer();

 @Before
 public void setUp() throws InterruptedException  {
 	System.out.println("--> entro");
    
 //	System.setProperty("webdriver.gecko.driver", "C:\\Saucelabs_WD_Ilse\\JarFiles\\geckodriver (1).exe");
/* 	DesiredCapabilities capabilities = DesiredCapabilities.firefox();
    //FirefoxOptions firefoxOptions = new FirefoxOptions();
    //firefoxOptions.setCapability("marionette", true);
 	capabilities.setCapability("marionette", true);*/
  	System.out.println("--> marionette y antes de abrir");
  //	driver = new FirefoxDriver();

  	// driver = new FirefoxDriver();
 	
  	System.setProperty("webdriver.opera.driver", "C:\\Saucelabs_WD_Ilse\\JarFiles\\operadriver.exe");	
  	driver = new OperaDriver();
  	
  	 System.out.println("--> test");
     baseUrl = "https://www.volaris.com/";
     System.out.print(baseUrl);
     
     driver.get(baseUrl);    
     driver.manage().window().maximize();

   // driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
 }


 @Test
 public void testUntitled2() throws Exception {
 
 
        driver.get(baseUrl + "");

   
 }


 @After
 public void tearDown() throws Exception {
   driver.quit();
   String verificationErrorString = verificationErrors.toString();
   if (!"".equals(verificationErrorString)) {
     fail(verificationErrorString);
   }
 }

 private boolean isElementPresent(By by) {
   try {
     driver.findElement(by);
     return true;
   } catch (NoSuchElementException e) {
     return false;
   }
 }

 private boolean isAlertPresent() {
   try {
     driver.switchTo().alert();
     return true;
   } catch (NoAlertPresentException e) {
     return false;
   }
 }

 private String closeAlertAndGetItsText() {
   try {
     Alert alert = driver.switchTo().alert();
     String alertText = alert.getText();
     if (acceptNextAlert) {
       alert.accept();
     } else {
       alert.dismiss();
     }
     return alertText;
   } finally {
     acceptNextAlert = true;
   }
 }
}