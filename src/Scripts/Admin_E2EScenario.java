package Scripts;

/*import keywords.Admin_Keywords;
import util.Constants;
import util.EmailReports;*/
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
//import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import TestCases.Admin_Keywords;
import ConfigFiles.Config;


@Test public class Admin_E2EScenario extends Config{

	//============================================Admin Script Execution Locally======================================================
	//@Parameters({ "site", "env", "module" })
	//@BeforeTest
	//public void setUp(String site, String env, String module) throws Exception, IOException {
	
		/* @HELP
		@class:			QA_E2EScenario
		@method:		setUp()
		@parameter:	String site, String env
		@notes:			1. Deletes the previous execution results
								2. Takes the parameters like site & env from command prompts and store it into global variable for further usage.
								3. Initializes the driver object and opens the Firefox browser.
								4. Sets the implicit timeouts and maximizes the browser.
		@returns:		None 
		@END
		 */
		//============================================Admin Script Execution on Saucelabs======================================================
		
	@Parameters({"username", "key", "site", "env", "module"})
		@BeforeMethod
		public void setUp(@Optional("onestopmac") String username,
        @Optional("0b90cc11-6b57-4cd8-b7e6-59d49ee70562") String key,
	        @Optional String site, 
	        @Optional String env,
	        @Optional String module,
	        Method method) throws Exception, IOException {
			try {
			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			//capabilities.setCapability("platform", "Windows 8.1");
			capabilities.setCapability("platform", "Windows 7");
			capabilities.setCapability("version", "31");
	        capabilities.setCapability("name", "ADMIN_"+ site + "_" + env);
	        capabilities.setCapability("maxDuration", 10800);
	        capabilities.setCapability("commandTimeout", 600);
	        
	        createLogFile("ADMIN_" + site + "_" + env + "_");
	        Adminmessage = "Admin_E2EScenario";

	        Admin_Keywords.driver = new RemoteWebDriver(
	                new URL("http://" + username + ":" + key + "@ondemand.saucelabs.com:80/wd/hub"),
	                capabilities);
		//======================================================================================================================================
		
		
		sitename=site;
		envname=env;
		globalmod=module;
		browsername = " Browser: Mozilla Firefox 31";
		Adminmessage = "Admin_E2EScenario";
		//try {                                    // Uncomment this for local Admin script execution.  
			scriptStartTime=System.currentTimeMillis();
			if(envname.equals("QA"))
			{
				SiteURLs=readTestDataFromExcel(siteURLsExcel, 2, sitename);
			}
			else if(envname.equals("STAGE"))
			{
				SiteURLs=readTestDataFromExcel(siteURLsExcel, 4, sitename);
			}
			else if(envname.equals("DEV"))
			{
				SiteURLs=readTestDataFromExcel(siteURLsExcel, 6, sitename);
			}
			System.out.println(customLogMessage="Test Scenario: " + envname + " Admin Automation Test Run for: " + SiteURLs);
			APP_LOGS.debug(customLogMessage);
			
			//===============================Uncomment this code to run Admin Script locally================================================
			/*driver=new FirefoxDriver();
			driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);
			driver.manage().window().maximize();*/
			//==============================================================================================================================
		} catch (Exception e) {
			captureScreenShot(sitename+"_"+envname+"_");
			System.out.println(customLogMessage="RESULT: FAIL - Automation Test failed to start");
			APP_LOGS.debug(customLogMessage);
		}
		System.out.println(customLogMessage="RESULT:PASS");
		APP_LOGS.debug(customLogMessage);
	}

//===================================================================================
	public void testCase() throws Exception {
		/* @HELP
		@class:			LIVE_E2EScenario
		@method:		testCase()
		@parameter:	None
		@notes:			1. Loads the Object Repository files and fetches the Xpath values from that file
								2. Calls one by one methods from LIVE_Keyowords.java file
		@returns:		 None
		@END
		 */
		
		String[] modules={"AdminHome","OrderProcessing","Summaries","Details","Misc","General","SiteAdmin","NavigationMenu","EngineeringPM"};
		//Bifurcating environments to capture respective OR files.
		System.out.println(customLogMessage="Starting Test Execution");
		APP_LOGS.debug(customLogMessage);
		LocatorProps = new Properties();
		if(envname.equals("QA"))
		{
			LocatorProps.load(new FileInputStream(OR+QA_OR+sitename+".properties"));
		} 
		else if(envname.equals("STAGE"))
		{
			LocatorProps.load(new FileInputStream(OR+STAGE_OR+sitename+".properties"));
		}
		else if(envname.equals("DEV"))
		{
			LocatorProps.load(new FileInputStream(OR+DEV_OR+sitename+".properties"));
		}
		else
		{
			System.out.println(customLogMessage="INFO: Please Check OR file path for respective environment.");
			APP_LOGS.debug(customLogMessage);
		}
			
			Admin_Keywords.GoToAdminHomePageAndVerifyLoaded();
			
			if(globalmod.equals("All")){
				for (String string : modules) {
					Admin_Keywords.ReadAdminModulesFromExcelAndPerformAction(string);
				}
			}else{
				Admin_Keywords.ReadAdminModulesFromExcelAndPerformAction(globalmod);
			}
			Admin_Keywords.LogOutAdminSite();
			scriptStatus = "Automation Test Successful";
			System.out.println("--> Automation Test Successful");
			APP_LOGS.debug("--> Automation Test Successful");
	}

//===================================================================================	
	@AfterTest
	public void tearDown() throws Exception {
		/* @HELP
		@class:			LIVE_E2EScenario
		@method:		tearDown()
		@parameter:	None
		@notes:			1. Closes the browser
								2. Copy the reports and zip it.
					    		3. Sends an email to specific stakeholders
		@returns:		 None
		@END
		 */
		
		try {
			System.out.println(customLogMessage="\nACTION: Exiting the browser");
		    APP_LOGS.debug(customLogMessage);
			driver.quit();
			System.out.println(customLogMessage="RESULT: PASS");
			APP_LOGS.debug(customLogMessage);	
		} catch (Exception e) {
			captureScreenShot(sitename+"_"+envname+"_");
		}
		finally{
			scriptEndTime=System.currentTimeMillis();
			scriptExecutionTime=(scriptEndTime-scriptStartTime)/1000;
			sExecutionTime=((int)(scriptExecutionTime/60))+" Minute "+ ((int)(scriptExecutionTime%60))+" Seconds";
			System.out.println(customLogMessage= "INFO  : Script Execution Time: "+sExecutionTime);
		    APP_LOGS.debug(customLogMessage);
		    Config.sendEmail();
		}
	}	
}
