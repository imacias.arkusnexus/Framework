package Scripts;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ConfigFiles.Config;
import TestCases.TestCode;


	/**
	 * @author Daniel Solis
	 */

	public class BDD_LIVE_Script extends TestCode {
		
		@Parameters({"username", "key", "site", "env"})
		@BeforeMethod
		public void setUp(@Optional("onestopmac") String username,
        @Optional("0b90cc11-6b57-4cd8-b7e6-59d49ee70562") String key,
	        @Optional String site, 
	        @Optional String env,
	        
	        Method method) throws Exception, IOException {
			try {
			//System.setProperty("webdriver.gecko.driver", "C:\\Saucelabs_WD\\JarFiles\\geckodriver.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			capabilities.setCapability("platform", "Windows 10");
			capabilities.setCapability("version", "61.0");
			capabilities.setCapability("screenResolution", "1280x960");
	        capabilities.setCapability("name", site + "_" + env);
	        capabilities.setCapability("maxDuration", 10800);
	        capabilities.setCapability("commandTimeout", 600);

	        //deleteOldFiles();
	        createLogFile(site + "_" + env + "_");

	       webDriver.set( TestCode.driver = new RemoteWebDriver(
	                new URL("http://" + username + ":" + key + "@ondemand.saucelabs.com:80/wd/hub"),
	                capabilities));
	        driver.manage().timeouts().pageLoadTimeout(pageLoadTime, TimeUnit.SECONDS);
	        
	        sitename = site;
	        envname = env;
	    //    SiteURLs = readTestDataFromExcel(siteURLsExcel, 1, sitename);
			} catch (Exception e) {
				captureScreenShot(sitename +"_" + envname + "_");
				captureHeaders(sitename +"_" + envname + "_");
			}
	    }
		  private ThreadLocal<WebDriver> webDriver = new ThreadLocal<WebDriver>();
		  
		  public WebDriver getWebDriver() {
		        return webDriver.get();
		    }
		  
		@Test
		public void testcases() throws Throwable {
	    	LocatorProps = new Properties();
	    	LocatorProps.load(new FileInputStream(LIVE_LocatorRep + sitename + ".properties"));
	    	browsername = " Browser: Google Chrome 61.0";
			scriptStartTime=System.currentTimeMillis();			

			/*Tests called*/
			System.out.println(LIVE_LocatorRep + sitename + ".properties");
			APP_LOGS.debug(LIVE_LocatorRep + sitename + ".properties");
			TestCode.open_Chrome_and_go_to_product_detail_page_on_Turtle_Beach_US_site();
			TestCode.product_bundle_loads();
			TestCode.user_should_see_two_products_as_one_bundle();
	    	scriptStatus = "Automation Test Successful";
	    	System.out.println("--> Automation Test Successful");
			APP_LOGS.debug("--> Automation Test Successful");
	    }
	    
	    @AfterMethod
	    public void tearDown(ITestResult result) throws Exception {	    
	    	((JavascriptExecutor) webDriver.get()).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
	    	scriptEndTime=System.currentTimeMillis();
			scriptExecutionTime=(scriptEndTime-scriptStartTime)/1000;
			sExecutionTime=((int)(scriptExecutionTime/60))+" Minute "+ ((int)(scriptExecutionTime%60))+" Seconds";
	    					    	
			// When the script fail, adding Slack mail
			if ( ! result.isSuccess() )
			{   
				int intMailToLength = Config.mailTO.length;
				String[] tempStrArray = new String[ intMailToLength + 1 ];
				
				for( int i=0; i<intMailToLength; i++ )
				   { tempStrArray[i] = Config.mailTO[i]; }
				
				tempStrArray[intMailToLength] = "s3t5o6s8e9o6f6r3@onestop.slack.com";
				Config.mailTO = tempStrArray;					
			}
			
			Config.sendEmail();
	    	System.out.println("--> Exiting the test");
	    	APP_LOGS.debug("--> Exiting the test");
	    	driver.quit();
			}
	}
