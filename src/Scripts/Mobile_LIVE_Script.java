package Scripts;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Properties;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ConfigFiles.Config;
import TestCases.Mobile_LIVE_Test_Suite;

/**
 * @author Neha Chowdhry
 */

public class Mobile_LIVE_Script extends Mobile_LIVE_Test_Suite {
	
	@Parameters({"username", "key", "site", "env"})
	@BeforeMethod
    public void setUp(@Optional("onestopmac") String username,
        @Optional("0b90cc11-6b57-4cd8-b7e6-59d49ee70562") String key,
        @Optional String site, 
        @Optional String env,
        Method method) throws Exception, IOException {
		try {
			DesiredCapabilities caps = DesiredCapabilities.iphone();
			caps.setCapability("appiumVersion", "1.7.1");
			caps.setCapability("deviceName","iPhone 8 Simulator");
			caps.setCapability("deviceOrientation", "portrait");
			caps.setCapability("platformVersion","11.0");
			caps.setCapability("platformName", "iOS");
			caps.setCapability("browserName", "Safari");
			caps.setCapability("name", site + "_" + env);
        
        deleteOldFiles();
        createLogFile(site + "_" + env + "_");

        webDriver.set(  Mobile_LIVE_Test_Suite.driver = new RemoteWebDriver(
                new URL("http://" + username + ":" + key + "@ondemand.saucelabs.com:80/wd/hub"),
                caps));
        sitename = site;
        envname = env;
        SiteURLs = readTestDataFromExcel(siteURLsExcel, 1, sitename);
		} catch (Exception e) {
			captureScreenShot(sitename +"_" + envname + "_");
			captureHeaders(sitename +"_" + envname + "_");
		}
    }
	  private ThreadLocal<WebDriver> webDriver = new ThreadLocal<WebDriver>();
	  
	  public WebDriver getWebDriver() {
	        return webDriver.get();
	    }

    @Test
	public void testcases() throws Exception {
    	LocatorProps = new Properties();
    	LocatorProps.load(new FileInputStream(Mobile_LIVE_LocatorRep + sitename + ".properties"));
	    browsername = " Browser: Safari on iOS 8.1";
		scriptStartTime=System.currentTimeMillis();
    	
		Mobile_LIVE_Test_Suite.HomePageLoad();    	  	
    	Mobile_LIVE_Test_Suite.SearchForAProduct();        
    	Mobile_LIVE_Test_Suite.GoToPLP();
    	Mobile_LIVE_Test_Suite.GoToPDP();
    	Mobile_LIVE_Test_Suite.SelectProductSizeOrColor();
    	Mobile_LIVE_Test_Suite.AddToCartAndCheckout();
    	
    	scriptStatus = "Automation Test Successful";
    	System.out.println("--> Automation Test Successful");
		APP_LOGS.debug("--> Automation Test Successful");
	    }
    
    @AfterMethod
    public void tearDown(ITestResult result) throws Exception {
    	((JavascriptExecutor) webDriver.get()).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
    	scriptEndTime=System.currentTimeMillis();
		scriptExecutionTime=(scriptEndTime-scriptStartTime)/1000;
		sExecutionTime=((int)(scriptExecutionTime/60))+" Minute "+ ((int)(scriptExecutionTime%60))+" Seconds";

		// When the script fail, adding Slack mail
		if ( ! result.isSuccess() )
		   {   
			 int intMailToLength = Config.mailTO.length;
			String[] tempStrArray = new String[ intMailToLength + 1 ];
						
			for( int i=0; i<intMailToLength; i++ )
			   { tempStrArray[i] = Config.mailTO[i]; }
						
				 tempStrArray[intMailToLength] = "s3t5o6s8e9o6f6r3@onestop.slack.com";
				 Config.mailTO = tempStrArray;					
		   }			
			
    	Config.sendEmail();
    	System.out.println("Exiting the test");
    	driver.quit();
}
}
