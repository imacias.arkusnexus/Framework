package Scripts;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import ConfigFiles.Config;
import TestCases.LIVE_Test_Suite;

	/**
	 * @author Neha Chowdhry
	 */

	public class FRYE_ExtraTCs_Script extends LIVE_Test_Suite {
		
		@Parameters({"username", "key", "site", "env"})
		@BeforeMethod
	    public void setUp(@Optional("onestopqa") String username,
	        @Optional("6ee2c83d-febb-4fc3-83d7-651a4af1c672") String key,
	        @Optional String site, 
	        @Optional String env,
	        
	        Method method) throws Exception, IOException {
			try {
			//System.setProperty("webdriver.gecko.driver", "C:\\Saucelabs_WD\\JarFiles\\geckodriver.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			capabilities.setCapability("platform", "Windows 10");
			capabilities.setCapability("version", "61.0");
			capabilities.setCapability("screenResolution", "1280x960");
		    capabilities.setCapability("name", site + "_ExtraTCs" + "_" + env);
		    capabilities.setCapability("maxDuration", 10800);
		    capabilities.setCapability("commandTimeout", 600);

		    //deleteOldFiles();
	        createLogFile(site + "_ExtraTCs"+ "_" + env + "_");

	        webDriver.set( LIVE_Test_Suite.driver = new RemoteWebDriver(
	                new URL("http://" + username + ":" + key + "@ondemand.saucelabs.com:80/wd/hub"),
	                capabilities));
	        driver.manage().timeouts().pageLoadTimeout(pageLoadTime, TimeUnit.SECONDS);
	        
	        sitename = site;
	        envname = env;
	        SiteURLs = readTestDataFromExcel(siteURLsExcel, 1, sitename);
			} catch (Exception e) {
				captureScreenShot(sitename + "_ExtraTCs"  + "_" + envname + "_");
				captureHeaders(sitename + "_ExtraTCs" + "_" + envname + "_");
			}
	    }
		 private ThreadLocal<WebDriver> webDriver = new ThreadLocal<WebDriver>();
		  
		  public WebDriver getWebDriver() {
		        return webDriver.get();
		    }
	    @Test
		public void testcases() throws Exception {
	    	LocatorProps = new Properties();
	    	LocatorProps.load(new FileInputStream(LIVE_LocatorRep + sitename + ".properties"));
	    	browsername = " Browser: Google Chrome 61.0";
			scriptStartTime=System.currentTimeMillis();	
		
			LIVE_Test_Suite.HomePageLoad();			
			LIVE_Test_Suite.VerifyTrackOrder();      
			LIVE_Test_Suite.ReturnOrder();
			LIVE_Test_Suite.SignUpNewsletter();
            //E-gift cart disabled for the moment
			//LIVE_Test_Suite.CheckEGiftCard();
			LIVE_Test_Suite.CheckGiftCardBalance();
			LIVE_Test_Suite.BorderFree();

	    	scriptStatus = "Automation Test Successful";
	    	System.out.println("--> Automation Test Successful");
			APP_LOGS.debug("--> Automation Test Successful");
	    }
	    
	    @AfterMethod
	    public void tearDown(ITestResult result) throws Exception {
	    	((JavascriptExecutor) webDriver.get()).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
	    	scriptEndTime=System.currentTimeMillis();
			scriptExecutionTime=(scriptEndTime-scriptStartTime)/1000;
			sExecutionTime=((int)(scriptExecutionTime/60))+" Minute "+ ((int)(scriptExecutionTime%60))+" Seconds";

			// When the script fail, adding Slack mail
			if ( ! result.isSuccess() )
			{   
				int intMailToLength = Config.mailTO.length;
				String[] tempStrArray = new String[ intMailToLength + 1 ];
				
				for( int i=0; i<intMailToLength; i++ )
				   { tempStrArray[i] = Config.mailTO[i]; }
				
				tempStrArray[intMailToLength] = "s3t5o6s8e9o6f6r3@onestop.slack.com";
				Config.mailTO = tempStrArray;					
			}

			sitename += "_ExtraTCs";
			Config.sendEmail();
	    	System.out.println("Exiting the test");
	    	driver.quit();
	}
	}
