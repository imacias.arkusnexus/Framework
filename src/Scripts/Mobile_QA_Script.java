package Scripts;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Properties;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ConfigFiles.Config;
import TestCases.Mobile_LIVE_Test_Suite;
import TestCases.Mobile_QA_Test_Suite;

public class Mobile_QA_Script extends Mobile_LIVE_Test_Suite {
	
	@Parameters({"username", "key", "site", "env"})
	
	   		@BeforeMethod
 public void setUp(@Optional("onestopqa") String username,
        @Optional("6ee2c83d-febb-4fc3-83d7-651a4af1c672") String key,
        @Optional String site, 
        @Optional String env,
        Method method) throws Exception, IOException {
		try {
			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			capabilities.setCapability("platform", "Windows 8.1");
			capabilities.setCapability("version", "31");
	        capabilities.setCapability("name", site + "_" + env);
	        capabilities.setCapability("maxDuration", 10800);
	        capabilities.setCapability("commandTimeout", 600);
        
        //deleteOldFiles();
        createLogFile(site + "_" + env + "_");

        Mobile_QA_Test_Suite.driver = new RemoteWebDriver(
                new URL("http://" + username + ":" + key + "@ondemand.saucelabs.com:80/wd/hub"),
                capabilities);
        sitename = site;
        envname = env;
        SiteURLs = readTestDataFromExcel(siteURLsExcel, 1, sitename);
		} catch (Exception e) {
			captureScreenShot(sitename +"_" + envname + "_");
			captureHeaders(sitename +"_" + envname + "_");
		}
    }

    @Test
	public void testcases() throws Exception {
    	LocatorProps = new Properties();
    	LocatorProps.load(new FileInputStream(Mobile_QA_LocatorRep + sitename + ".properties"));
	    browsername = " Browser: Mozilla Firefox 31.0";
		scriptStartTime=System.currentTimeMillis();
    	Mobile_QA_Test_Suite.HomePageLoad();
    	Mobile_QA_Test_Suite.MyAccountSignIn();
    	Mobile_QA_Test_Suite.EmptyShoppingCart();
    	Mobile_QA_Test_Suite.SearchForAProduct();
    	Mobile_QA_Test_Suite.GoToPLP();
    	Mobile_QA_Test_Suite.GoToPDP();
    	Mobile_QA_Test_Suite.SelectProductSizeOrColor();
    	Mobile_QA_Test_Suite.AddToCartAndCheckout();
    	Mobile_QA_Test_Suite.enterShippingInfo();
    	Mobile_QA_Test_Suite.VerifySalesTax();  
    	Mobile_QA_Test_Suite.VerifyPaypalPage(); 
    	Mobile_QA_Test_Suite.placeOrder();
    	scriptStatus = "Automation Test Successful";
    	System.out.println("--> Automation Test Successful");
		APP_LOGS.debug("--> Automation Test Successful");
	    }
    
    @AfterMethod
    public void tearDown() throws Exception {
	scriptEndTime=System.currentTimeMillis();
			scriptExecutionTime=(scriptEndTime-scriptStartTime)/1000;
			sExecutionTime=((int)(scriptExecutionTime/60))+" Minute "+ ((int)(scriptExecutionTime%60))+" Seconds";
    	Config.sendEmail();
    	System.out.println("Exiting the test");
    	driver.quit();
}
}
