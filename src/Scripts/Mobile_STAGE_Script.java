package Scripts;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Properties;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ConfigFiles.Config;

import TestCases.Mobile_STAGE_Test_Suite;

public class Mobile_STAGE_Script extends Mobile_STAGE_Test_Suite {
	
	/*@Parameters({"username", "key", "site", "env"})
	
	   		@BeforeMethod
 public void setUp(@Optional("onestopqa") String username,
        @Optional("6ee2c83d-febb-4fc3-83d7-651a4af1c672") String key,
        @Optional String site, 
        @Optional String env,
        Method method) throws Exception, IOException {
		try {
			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			capabilities.setCapability("platform", "Windows 8.1");
			capabilities.setCapability("version", "31");
	        capabilities.setCapability("name", site + "_" + env);
        
        //deleteOldFiles();
        createLogFile(site + "_" + env + "_");

        Mobile_STAGE_Test_Suite.driver = new RemoteWebDriver(
                new URL("http://" + username + ":" + key + "@ondemand.saucelabs.com:80/wd/hub"),
                capabilities);
        sitename = site;
        envname = env;
        SiteURLs = readTestDataFromExcel(siteURLsExcel, 6, sitename);
		} catch (Exception e) {
			captureScreenShot(sitename +"_" + envname + "_");
			captureHeaders(sitename +"_" + envname + "_");
		}
    }*/

	@Parameters({ "site", "env" })
	@BeforeTest
	public void setUp(String site, String env) throws Exception, IOException {
		/* @HELP
		@class:			LIVE_E2EScenario
		@method:		setUp()
		@parameter:	String site, String env
		@notes:			1. Deletes the previous execution results
								2. Takes the parameters like site & env from command prompts and store it into global variable for further usage.
								3. Initializes the driver object and opens the Firefox browser.
								4. Sets the implicit timeouts and maximizes the browser.
		@returns:		None 
		@END*/
		 

		sitename=site;
		envname=env;
		createLogFile(site + "_" + env + "_");
		try {
			scriptStartTime=System.currentTimeMillis();
			SiteURLs=readTestDataFromExcel(siteURLsExcel, 6, sitename);
			System.out.println("Test Scenario: " + sitename + " Automation Test Run for: " + SiteURLs);
			APP_LOGS.debug("Test Scenario: " + envname + " Automation Test Run for: " + SiteURLs);
			driver = new FirefoxDriver();
			//driver.manage().timeouts().implicitlyWait(implicitTimeout, TimeUnit.SECONDS);
			driver.manage().window().maximize();
		} catch (Exception e) {
			captureScreenShot(sitename+"_"+envname+"_");
			System.out.println("RESULT: FAIL - Automation Test failed to start");
			APP_LOGS.debug("RESULT: FAIL - Automation Test failed to start");
		}
		System.out.println("RESULT:PASS");
		APP_LOGS.debug("RESULT: PASS");
	}

    @Test
	public void testcases() throws Exception {
    	LocatorProps = new Properties();
    	LocatorProps.load(new FileInputStream(Mobile_STAGE_LocatorRep + sitename + ".properties"));
	    browsername = " Browser: Mozilla Firefox 31.0";
		scriptStartTime=System.currentTimeMillis();
		Mobile_STAGE_Test_Suite.HomePageLoad();
		Mobile_STAGE_Test_Suite.MyAccountSignIn();
		Mobile_STAGE_Test_Suite.EmptyShoppingCart();
		Mobile_STAGE_Test_Suite.SearchForAProduct();
		Mobile_STAGE_Test_Suite.GoToPLP();
		Mobile_STAGE_Test_Suite.GoToPDP();
		Mobile_STAGE_Test_Suite.SelectProductSizeOrColor();
		Mobile_STAGE_Test_Suite.AddToCartAndCheckout();
		Mobile_STAGE_Test_Suite.enterShippingInfo();
		Mobile_STAGE_Test_Suite.VerifySalesTax();  
		Mobile_STAGE_Test_Suite.VerifyPaypalPage();
    	scriptStatus = "Automation Test Successful";
    	System.out.println("--> Automation Test Successful");
		APP_LOGS.debug("--> Automation Test Successful");
	    }
    
    @AfterMethod
    public void tearDown() throws Exception {
	scriptEndTime=System.currentTimeMillis();
			scriptExecutionTime=(scriptEndTime-scriptStartTime)/1000;
			sExecutionTime=((int)(scriptExecutionTime/60))+" Minute "+ ((int)(scriptExecutionTime%60))+" Seconds";
    	Config.sendEmail();
    	System.out.println("Exiting the test");
    	driver.quit();
}
}
