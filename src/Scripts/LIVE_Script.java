package Scripts; //Namespaces.

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.varia.NullAppender;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


//Clases por usar.
import Scripts.CrossBrowsers;
import ConfigFiles.Config;

import TestCases.LIVE_Test_Suite;

	/**
	 * @author Neha Chowdhry
	 */

	public class LIVE_Script extends LIVE_Test_Suite {
//		private static WebDriver driver; 
		
		@Parameters({"username", "key", "site", "env"})
		@BeforeMethod
	
		public void setUp(@Optional("onestopmac") String username,
        @Optional("0b90cc11-6b57-4cd8-b7e6-59d49ee70562") String key,
	        @Optional String site, 
	        @Optional String env,
 
	        Method method) throws Exception, IOException {
			try {
				createLogFile(site + "_" + env + "_");
				System.out.println("*********");
				
			    /************************Local environment***************************/
				CrossBrowsers crossBrowers = new CrossBrowsers();
				
				driver = crossBrowers.Setup("chrome"); 
				
				System.out.println("Browser: " + driver);
		    	driver.manage().deleteAllCookies();
	
		        driver.manage().timeouts().pageLoadTimeout(pageLoadTime, TimeUnit.SECONDS);
		        driver.manage().window().maximize(); //Maximize the window.
	      
		        sitename = site;
		        envname = env;
		        
		       // SiteURLs = readTestDataFromExcel(siteURLsExcel, 1, sitename);
			} //try-end
			
			catch (Exception e) {
				captureScreenShot(sitename +"_" + envname + "_");
				captureHeaders(sitename +"_" + envname + "_");
			}
	    } //setUp method.
		
		  private ThreadLocal<WebDriver> webDriver = new ThreadLocal<WebDriver>();
		  
		  public WebDriver getWebDriver() {
		        return webDriver.get();
		  }
		  
		@Test
		public void testcases() throws Exception {
	    	LocatorProps = new Properties();
	    	LocatorProps.load(new FileInputStream(LIVE_LocatorRep + sitename + ".properties"));
			scriptStartTime=System.currentTimeMillis();			
	
			
			LIVE_Test_Suite.HomePageLoad();
			LIVE_Test_Suite.MyAccountSignIn();
			LIVE_Test_Suite.AccountVerification();
			LIVE_Test_Suite.EmptyShoppingCart();
	    	//LIVE_Test_Suite.SearchForAProduct();
	    	LIVE_Test_Suite.GoToPLP();
			LIVE_Test_Suite.GoToPDP();
	    	LIVE_Test_Suite.SelectProductSize();
//	    	LIVE_Test_Suite.AddToCartAndCheckout();
	    	
	    	scriptStatus = "Automation Test Successful";
	    	LogInfo("--> Automation Test Successful");
	    }//testcases method.
	    
	    @AfterMethod
	    public void tearDown(ITestResult result) throws Exception {	    
	    	((JavascriptExecutor) webDriver.get()).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
	    	scriptEndTime=System.currentTimeMillis();
			scriptExecutionTime=(scriptEndTime-scriptStartTime)/1000;
			sExecutionTime=((int)(scriptExecutionTime/60))+" Minute "+ ((int)(scriptExecutionTime%60))+" Seconds";
	    					    	
	    	LogInfo("--> Exiting the test");
	    	driver.quit();
		}//tearDown method.
	}//LIVE_Script
