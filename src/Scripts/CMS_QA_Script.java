package Scripts;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Properties;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ConfigFiles.Config;
import TestCases.CMS_Test_Suite;

	/**
	 * @author Neha Chowdhry
	 */

	public class CMS_QA_Script extends CMS_Test_Suite {
		
		@Parameters({"username", "key", "site", "env"})
		@BeforeMethod
	    public void setUp(@Optional("onestopqa") String username,
	        @Optional("6ee2c83d-febb-4fc3-83d7-651a4af1c672") String key,
	        @Optional String site, 
	        @Optional String env,
	        Method method) throws Exception, IOException {
			try {
			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			capabilities.setCapability("platform", "Windows 8.1");
			capabilities.setCapability("version", "31");
	        capabilities.setCapability("name", site + "_" + env);
	        
	        deleteOldFiles();
	        createLogFile(site + "_" + env + "_");

	        CMS_Test_Suite.driver = new RemoteWebDriver(
	                new URL("http://" + username + ":" + key + "@ondemand.saucelabs.com:80/wd/hub"),
	                capabilities);
	        sitename = site;
	        envname = env;	
	        
	        System.out.println("--> Navigating to the home page");
			APP_LOGS.debug("--> Navigating to the home page");
			pageLoadStartTime = System.currentTimeMillis();
			driver.get("http://acme.qa.onestop.com/admin");		
			} catch (Exception e) {
				captureScreenShot(sitename +"_" + envname + "_");
				captureHeaders(sitename +"_" + envname + "_");
			}
	    }

	    @Test
		public void testcases() throws Exception {
	    	LocatorProps = new Properties();
	    	LocatorProps.load(new FileInputStream(CMS_LocatorRep + "CMS.properties"));
	    	browsername = " Browser: Mozilla Firefox 31.0";
			scriptStartTime=System.currentTimeMillis();
	    	CMS_Test_Suite.ACMEAdminVerification();
	    	CMS_Test_Suite.ACMEAdminLogInVerification();
	    	CMS_Test_Suite.LayoutPageLoadVerification();
	    	CMS_Test_Suite.deleteOldLayout();
	    	CMS_Test_Suite.createLayout();
	    	CMS_Test_Suite.deleteOldTemplate();
	    	CMS_Test_Suite.createTemplate();
	    	CMS_Test_Suite.deleteOldSlideshow();
	    	CMS_Test_Suite.createSlideshow();
	    	CMS_Test_Suite.addSlides();
	    		    	
	    	scriptStatus = "Automation Test Successful";
	    	System.out.println("--> Automation Test Successful");
			APP_LOGS.debug("--> Automation Test Successful");
	    }
	    
	    @AfterMethod
	    public void tearDown() throws Exception {
	    	scriptEndTime=System.currentTimeMillis();
			scriptExecutionTime=(scriptEndTime-scriptStartTime)/1000;
			sExecutionTime=((int)(scriptExecutionTime/60))+" Minute "+ ((int)(scriptExecutionTime%60))+" Seconds";
	    	Config.sendEmail();
	    	System.out.println("Exiting the test");
	    	driver.quit();
	}
	}
