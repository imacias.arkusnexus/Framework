package Scripts;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ConfigFiles.Config;
import TestCases.LIVE_Test_Suite;

	/**
	 * @author Neha Chowdhry
	 */

	public class ODIE_LIVE_Script extends LIVE_Test_Suite {
		
		@Parameters({"username", "key", "site", "env", "host", "url"})
		@BeforeMethod
	    public void setUp(@Optional("onestopqa") String username,
	        @Optional("6ee2c83d-febb-4fc3-83d7-651a4af1c672") String key,
	        @Optional String site, 
	        @Optional String env,
	        @Optional String host,
	        @Optional String url,
	        Method method) throws Exception, IOException {
			try {
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				capabilities.setCapability("platform", "Windows 10");
				capabilities.setCapability("version", "61.0");
				capabilities.setCapability("screenResolution", "1280x960");
		        capabilities.setCapability("name", "ODIE_" + site + "_" + env);
		        capabilities.setCapability("maxDuration", 10800);
		        capabilities.setCapability("commandTimeout", 600);
	        
	      //  deleteOldFiles();
	        createLogFile(site + "_" + env + "_");

	        webDriver.set(  LIVE_Test_Suite.driver = new RemoteWebDriver(
	                new URL("http://" + username + ":" + key + "@ondemand.saucelabs.com:80/wd/hub"),
	                capabilities));
	        driver.manage().timeouts().pageLoadTimeout(pageLoadTime, TimeUnit.SECONDS);
	        
	        sitename = site;
	        envname = env;
			hostname = host;
	        nodeURLname = url;
			} catch (Exception e) {
				captureScreenShot(sitename +"_" + envname + "_");
				captureHeaders(sitename +"_" + envname + "_");
			}
	    }
		 private ThreadLocal<WebDriver> webDriver = new ThreadLocal<WebDriver>();
		  
		  public WebDriver getWebDriver() {
		        return webDriver.get();
		    }
		  	
		@Test
		public void testcases() throws Exception {
			
			LocatorProps = new Properties();
	    	LocatorProps.load(new FileInputStream(LIVE_LocatorRep + sitename + ".properties"));
	    	browsername = " Browser: Google Chrome 61.0";
			scriptStartTime=System.currentTimeMillis();			
			System.out.println("--> Host: " + hostname + " SiteURL: " + nodeURLname);
	        APP_LOGS.debug("--> Host: " + hostname + " SiteURL: " + nodeURLname);
	        Writer writer = null;
	        try{
	        	writer = new PrintWriter(("C:\\Saucelabs_WD\\Host_IP.txt"), "utf-8");
	        writer.write(hostname + " " + nodeURLname);
	        }catch (IOException ex) {
	        	ex.printStackTrace();
	        } finally {
	            writer.close();
	        }	        
			
	        System.out.println("--> Flushing the DNS");
            APP_LOGS.debug("--> Flushing the DNS");
            Process p1 = Runtime.getRuntime().exec("cmd /c ipconfig /flushdns"); 
            p1.waitFor();
            Thread.sleep(10000);
            APP_LOGS.debug("--> Executing the batch file");
			System.out.println("--> Executing the batch file");
            APP_LOGS.debug("--> Executing the batch file");
            Process pr = Runtime.getRuntime().exec("C:\\Saucelabs_WD\\EditDNS.bat");
            pr.waitFor();
            Thread.sleep(30000);
            Process p2 = Runtime.getRuntime().exec("cmd /c ipconfig /flushdns"); 
            p2.waitFor();
            Thread.sleep(30000);
			System.out.println("--> Flushing the DNS");
            APP_LOGS.debug("--> Flushing the DNS");
            Process p3 = Runtime.getRuntime().exec("cmd /c ipconfig /flushdns"); 
            p3.waitFor();
            Thread.sleep(10000);
            System.out.println("--> Navigating to the home page");
	        APP_LOGS.debug("--> Navigating to the home page");
	        SiteURLs = readTestDataFromExcel(siteURLsExcel, 0, sitename);
			System.out.println(SiteURLs.get(1));
			APP_LOGS.debug(SiteURLs.get(1));
			driver.get(SiteURLs.get(1));
			waitForLocator("MY_ACCOUNT");			
			
			//Disabled node validation for Shopify clients
			if( ! ("TRTL".equals(sitename) || "TRAU".equals(sitename) || "TRNZ".equals(sitename) || "TRFR".equals(sitename)  || "TRUK".equals(sitename) || "TRES".equals(sitename) || "TRNL".equals(sitename) || "TRDE".equals(sitename) || "TRIT".equals(sitename) || "TREU".equals(sitename))){
			
				System.out.println("--> Verifying the machine name");
				APP_LOGS.debug("--> Verifying the machine name");
				
				if("EAGL".equals(sitename)){
					driver.get("http://shop.eaglecreek.com/status");
				}else{
					String URL = driver.getCurrentUrl();
					driver.get(URL + "/status");
				}
            
				Thread.sleep(7000);
				String siteNode = returnLocator("NODE_STATUS").getText();
				System.out.println("--> Machine name is: " + siteNode);
				APP_LOGS.debug("--> Machine name is: " + siteNode);
				Thread.sleep(20000);            
			}//if  Disabled node validation for Shopify clients                     
            			
			LIVE_Test_Suite.HomePageLoad();
			if("EAGL".equals(sitename)){
            	driver.get("http://shop.eaglecreek.com/luggage-view-all/l/327");
            	if(isElementPresent("FANCYBOX") == true){
        			System.out.println("--> Closing the Fancy Box Pop-Up");
        			APP_LOGS.debug("--> Closing the Fancy Box Pop-Up");
        			returnLocator("FANCYBOX").click();
        			waitForLocator("SEARCH_LINK");
        		}
            }else if("FRYE".equals(sitename)){
            	System.out.println("--> Scrolling down on the page");
				APP_LOGS.debug("--> Scrolling down on the page");
				((JavascriptExecutor) driver).executeScript("scroll(0,500);");
				Thread.sleep(5000);
            }
            LIVE_Test_Suite.SearchForAProduct();
	    	LIVE_Test_Suite.GoToPLP();
	    	LIVE_Test_Suite.GoToPDP();
	    	LIVE_Test_Suite.SelectProductSize();
	    	// skipping Add to cart validation for TRAU and TRNZ
	    	if (! ("TRAU".equals(sitename) ||  "TRNZ".equals(sitename) ))
			{

	    		LIVE_Test_Suite.AddToCartAndCheckout();
			}
	    	
	    	scriptStatus = "Automation Test Successful";
	    	System.out.println("--> Automation Test Successful");
			APP_LOGS.debug("--> Automation Test Successful");
	    }
	    
	    @AfterMethod
	    public void tearDown(ITestResult result) throws Exception {
	    	((JavascriptExecutor) webDriver.get()).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
	    	scriptEndTime=System.currentTimeMillis();
			scriptExecutionTime=(scriptEndTime-scriptStartTime)/1000;
			sExecutionTime=((int)(scriptExecutionTime/60))+" Minute "+ ((int)(scriptExecutionTime%60))+" Seconds";
			sitename = "ODIE_" + sitename;
			Config.sendEmail();
	    	System.out.println("Exiting the test");
	    	driver.quit();
			}
	}
