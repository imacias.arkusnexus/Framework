package TestCases;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import ConfigFiles.Config;

public class Admin_Keywords extends Config {

	public static String GoToAdminHomePageAndVerifyLoaded() throws Exception {
		/* @HELP
		@class:			DEV_Keywords
		@method:		GoToAdminHomePageAndVerifyLoaded()
		@parameter:	None
		@notes:			Navigates to specific client URL & verifies admin home page is loaded
		@returns:		"PASS" or "FAIL" with Exception in case if method not got executed because of some runtime exception 
		@END*/
		 
		testcasename="GoToAdminHomePageAndVerifyLoaded";
		APP_LOGS.debug("------------------------------------------------------------");
		System.out.println("\nINFO  : Executing '"+testcasename +"' Method");
	    APP_LOGS.debug("INFO  : Executing '"+testcasename +"' Method");
		System.out.println(customLogMessage="ACTION: Navigating to the admin home page & verifying home page is loaded : "+SiteURLs.get(1));
		APP_LOGS.debug(customLogMessage);
		try{
			SiteURLs.clear();
			//Bifurcating environments according to 'globalenv' variable.
			if(envname.equals("QA"))
			{
				
				SiteURLs=readTestDataFromExcel(siteURLsExcel, 2, sitename);
			}
			else if(envname.equals("STAGE"))
			{
				SiteURLs=readTestDataFromExcel(siteURLsExcel, 4, sitename);
			}
			else if(envname.equals("DEV"))
			{
				SiteURLs=readTestDataFromExcel(siteURLsExcel, 6, sitename);
			}
			else
			{
				System.out.println(customLogMessage="INFO: Please Check the environment selected.");
				APP_LOGS.debug(customLogMessage);
			}
			loginCreds.clear();
			loginCreds=readTestDataFromExcel(adminInformationExcel,1,"AllClientLogin");
			driver.get(SiteURLs.get(1)+"admin");
			if(isElementPresent("AUTH_USERNAME")==true){
				loginCredsAdmin.clear();
				loginCredsAdmin=readTestDataFromExcel(adminInformationExcel,3,sitename);
				System.out.println(customLogMessage="INFO: Entering Auth Credentials");
				APP_LOGS.debug(customLogMessage);
				returnLocator("AUTH_USERNAME").sendKeys(loginCredsAdmin.get(1));
				returnLocator("AUTH_PASSWORD").sendKeys(loginCredsAdmin.get(2));
				returnLocator("AUTH_LOGIN").click();
				Thread.sleep(2000);
			}
			loginCreds.clear();
			loginCreds=readTestDataFromExcel(adminInformationExcel,6,"AllClientLogin");
			System.out.println(customLogMessage="INFO: Entering Username");
			APP_LOGS.debug(customLogMessage);
			returnLocator("USERNAME").sendKeys(loginCreds.get(1));
			System.out.println(customLogMessage="INFO: Entering Password");
			APP_LOGS.debug(customLogMessage);
			returnLocator("PASSWORD").sendKeys(loginCreds.get(2));
			System.out.println(customLogMessage="INFO: Clicking on Login button");
			APP_LOGS.debug(customLogMessage);
			returnLocator("LOGIN").click();
			System.out.println(customLogMessage="INFO: Verifying Admin Area");
			APP_LOGS.debug(customLogMessage);
			if(returnLocator("ADMIN_AREA").getText().contains("Web Site Administration Area")){
				System.out.println(customLogMessage="INFO  : Admin Home page loaded successfully");
			    APP_LOGS.debug(customLogMessage);
			}else{
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError, dropDownElementSelectionError);
				System.out.println("FAIL - Not able to navigate to Admin Home page as ADMIN_AREA is not avaialble");
				return "FAIL";
			}
		}catch(Exception e){
			pageloadErrorOrException=e.getMessage();
			failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError, dropDownElementSelectionError);
			Assert.fail();
			return "FAIL";
		}
		System.out.println(customLogMessage="RESULT: PASS");
		APP_LOGS.debug(customLogMessage);
		return "PASS";
	}
//===================================================================================================
	
	public static String ReadAdminModulesFromExcelAndPerformAction(String sheetName) throws Exception {
		/* @HELP
		@class:			LIVE_Keywords
		@method:		ReadAdminModulesFromExcelAndPerformAction()
		@parameter:	None
		@notes:			Reads admin modules from excel file and performs actions on specific or all modules
		@returns:		"PASS" or "FAIL" with Exception in case if method not got executed because of some runtime exception 
		@END*/
		
		testcasename="ReadAdminModulesFromExcelAndPerformAction";
		APP_LOGS.debug("------------------------------------------------------------");
		System.out.println("\nINFO  : Executing '"+testcasename +"' Method");
	    APP_LOGS.debug("INFO  : Executing '"+testcasename +"' Method");
		System.out.println(customLogMessage="ACTION: Reading modules from exel file and performing action on all links available on the web page");
		APP_LOGS.debug(customLogMessage);
			String pathArr[];
			String excelData;
			try {
				act= new Actions(driver);
				FileInputStream file = new FileInputStream(new File(adminModulesExcel));
				XSSFWorkbook workbook = new XSSFWorkbook(file);
				XSSFSheet sheet = workbook.getSheet(sheetName);
				Iterator<Row> rowIterator = null;
				try{
					rowIterator = sheet.iterator();
				}catch(Exception e){
					System.out.println(customLogMessage="INFO: Invalid Sheet Name");
					APP_LOGS.debug(customLogMessage);
					pageloadErrorOrException=e.getMessage();
					failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError, dropDownElementSelectionError);
					Assert.fail();
					System.exit(1);
					return "FAIL";
				}
				while (rowIterator.hasNext()) {
					Row row = rowIterator.next();
					Iterator<Cell> cellIterator = row.cellIterator();
					while (cellIterator.hasNext()) {
						Cell cell = cellIterator.next();
						excelData=cell.getStringCellValue().trim();
						pathArr=excelData.split("/");
				     //switch case starts from here for all modules
						switch (sheetName) {
				            case "AdminHome":  
				            	System.out.println(customLogMessage="INFO  : ==========>> \""+sheetName+"\" <<==========");
	            				APP_LOGS.debug(customLogMessage);
				            	if(pathArr.length==2){
				            		//do click
				            		category=pathArr[0].toString().trim();
				            		locator=pathArr[1].toString().trim();
				            		System.out.println(customLogMessage="ACTION: Performing click action on \""+sheetName+">"+category+"\"");
									APP_LOGS.debug(customLogMessage);
									 driver.findElement(By.linkText(locator)).click();
									 if(isElementPresent("VIEW_ORDER_GIRD")){
										 driver.navigate().back();
									 }
				            	}else{
				            		System.out.println(customLogMessage="INFO: Invalid length");
				            		APP_LOGS.debug(customLogMessage);
				            	}			            	
				                     break;
				            case "OrderProcessing":
				            	System.out.println(customLogMessage="INFO  : ==========>> \""+sheetName+"\" <<==========");
	            				APP_LOGS.debug(customLogMessage);
				            	if(pathArr.length==2){
				            		//do mouse hover and click
				            		category=pathArr[0].toString().trim();
				            		locator=pathArr[1].toString().trim();
				            		System.out.println(customLogMessage="ACTION: Performing click action on \""+sheetName+">"+category+"\"");
									APP_LOGS.debug(customLogMessage);
									singleMouseHover("ORDER_PROCESSING", locator);
				            	}else if(pathArr.length==3){
				            		//do mouse hover twice and click
				            		String subModName=pathArr[0].toString().trim();
				            		category=pathArr[1].toString().trim();
				            		locator=pathArr[2].toString().trim();
									 if(subModName.equals("View Orders")){
										 System.out.println(customLogMessage="ACTION: Performing click action on \""+sheetName+">"+subModName+">"+category+"\"");
										 APP_LOGS.debug(customLogMessage);
										 doubleMouseHover("ORDER_PROCESSING", "VIEW_ORDERS", locator);
									 }else if(subModName.equals("Pick Batches")){
										 System.out.println(customLogMessage="ACTION: Performing click action on \""+category+"\"");
										 APP_LOGS.debug(customLogMessage);
										 doubleMouseHover("ORDER_PROCESSING", "PICK_BATCHES", locator);
									 }
				            	}else{
				            		System.out.println(customLogMessage="INFO: Invalid length");
				            		APP_LOGS.debug(customLogMessage);
				            	}	
				            	break;
				            case "Summaries":
				            	System.out.println(customLogMessage="INFO  : ==========>> \""+sheetName+"\" <<==========");
	            				APP_LOGS.debug(customLogMessage);
				            	if(pathArr.length==2){
				            		//do mouse hover and click
				            		category=pathArr[0].toString().trim();
				            		locator=pathArr[1].toString().trim();
				            		System.out.println(customLogMessage="ACTION: Performing click action on \""+sheetName+">"+category+"\"");
									APP_LOGS.debug(customLogMessage);
									singleMouseHover("SUMMARIES", locator);
				            	}else if(pathArr.length==3){
				            		//do mouse hover twice and click
				            		String subModName=pathArr[0].toString().trim();
				            		category=pathArr[1].toString().trim();
				            		locator=pathArr[2].toString().trim();
									 if(subModName.equals("Daily")){
										 System.out.println(customLogMessage="ACTION: Performing click action on \""+sheetName+">"+subModName+">"+category+"\"");
										 APP_LOGS.debug(customLogMessage);
										 doubleMouseHover("SUMMARIES", "DAILY", locator);
									 }else if(subModName.equals("Monthly")){
										 System.out.println(customLogMessage="ACTION: Performing click action on \""+sheetName+">"+subModName+">"+category+"\"");
										 APP_LOGS.debug(customLogMessage);
										 doubleMouseHover("SUMMARIES", "MONTHLY", locator);
									 }
				            	}else{
				            		System.out.println(customLogMessage="INFO: Invalid length");
				            		APP_LOGS.debug(customLogMessage);
				            	}	
				            	break;
				            case "Details":
				            	System.out.println(customLogMessage="INFO  : ==========>> \""+sheetName+"\" <<==========");
	            				APP_LOGS.debug(customLogMessage);
				            	if(pathArr.length==2){
				            		//do mouse hover and click
				            		category=pathArr[0].toString().trim();
				            		locator=pathArr[1].toString().trim();
				            		System.out.println(customLogMessage="ACTION: Performing click action on \""+sheetName+">"+category+"\"");
									APP_LOGS.debug(customLogMessage);
									singleMouseHover("DETAILS", locator);
				            	}else if(pathArr.length==3){
				            		//do mouse hover twice and click
				            		String subModName=pathArr[0].toString().trim();
				            		category=pathArr[1].toString().trim();
				            		locator=pathArr[2].toString().trim();
									 if(subModName.equals("Sales")){
										 System.out.println(customLogMessage="ACTION: Performing click action on \""+sheetName+">"+subModName+">"+category+"\"");
										 APP_LOGS.debug(customLogMessage);
										 doubleMouseHover("DETAILS", "SALES", locator);
									 }else if(subModName.equals("Shipped")){
										 System.out.println(customLogMessage="ACTION: Performing click action on \""+sheetName+">"+subModName+">"+category+"\"");
										 APP_LOGS.debug(customLogMessage);
										 doubleMouseHover("DETAILS", "SHIPPED", locator);
									 }else if(subModName.equals("Returned")){
										 System.out.println(customLogMessage="ACTION: Performing click action on \""+sheetName+">"+subModName+">"+category+"\"");
										 APP_LOGS.debug(customLogMessage);
										 doubleMouseHover("DETAILS", "RETURNED", locator);
									 }else if(subModName.equals("Credit")){
										 System.out.println(customLogMessage="ACTION: Performing click action on \""+sheetName+">"+subModName+">"+category+"\"");
										 APP_LOGS.debug(customLogMessage);
										 doubleMouseHover("DETAILS", "CREDIT", locator);
									 }
				            	}else{
				            		System.out.println(customLogMessage="INFO: Invalid length");
				            		APP_LOGS.debug(customLogMessage);
				            	}	
				            	break;
				            case "Misc":
				            	System.out.println(customLogMessage="INFO  : ==========>> \""+sheetName+"\" <<==========");
	            				APP_LOGS.debug(customLogMessage);
				            	if(pathArr.length==2){
				            		//do mouse hover and click
				            		category=pathArr[0].toString().trim();
				            		locator=pathArr[1].toString().trim();
				            		System.out.println(customLogMessage="ACTION: Performing click action on \""+sheetName+">"+category+"\"");
									APP_LOGS.debug(customLogMessage);
									singleMouseHover("MISC", locator);
				            	}else if(pathArr.length==3){
				            		//do mouse hover twice and click
				            		String subModName=pathArr[0].toString().trim();
				            		category=pathArr[1].toString().trim();
				            		locator=pathArr[2].toString().trim();
									 if(subModName.equals("Inventory")){
										 System.out.println(customLogMessage="ACTION: Performing click action on \""+sheetName+">"+subModName+">"+category+"\"");
										 APP_LOGS.debug(customLogMessage);
										 doubleMouseHover("MISC", "INVENTORY", locator);
									 }
				            	}else{
				            		System.out.println(customLogMessage="INFO: Invalid length");
				            		APP_LOGS.debug(customLogMessage);
				            	}	
				                break;
				            case "General":
				            	System.out.println(customLogMessage="INFO  : ==========>> \""+sheetName+"\" <<==========");
	            				APP_LOGS.debug(customLogMessage);
				            	if(pathArr.length==2){
				            		//do mouse hover and click
				            		category=pathArr[0].toString().trim();
				            		locator=pathArr[1].toString().trim();
				            		System.out.println(customLogMessage="ACTION: Performing click action on \""+sheetName+">"+category+"\"");
									APP_LOGS.debug(customLogMessage);
									singleMouseHover("GENERAL", locator);
				            	}else if(pathArr.length==3){
				            		//do mouse hover twice and click
				            		String subModName=pathArr[0].toString().trim();
									category=pathArr[1].toString().trim();
									locator=pathArr[2].toString().trim();
									 if(subModName.equals("Show Reports")){
										 System.out.println(customLogMessage="ACTION: Performing click action on \""+sheetName+">"+subModName+">"+category+"\"");
										 APP_LOGS.debug(customLogMessage);
										 doubleMouseHover("GENERAL", "SHOW_REPORTS", locator);
									 }else if(subModName.equals("Banned Customers")){
										 System.out.println(customLogMessage="ACTION: Performing click action on \""+sheetName+">"+subModName+">"+category+"\"");
										 APP_LOGS.debug(customLogMessage);
										 doubleMouseHover("GENERAL", "BANNED_CUSTOMERS", locator);
									 }else if(subModName.equals("Clerks")){
										 System.out.println(customLogMessage="ACTION: Performing click action on \""+sheetName+">"+subModName+">"+category+"\"");
										 APP_LOGS.debug(customLogMessage);
										 doubleMouseHover("GENERAL", "CLERKS", locator);
									 }else if(subModName.equals("Correspondence")){
										 System.out.println(customLogMessage="ACTION: Performing click action on \""+sheetName+">"+subModName+">"+category+"\"");
										 APP_LOGS.debug(customLogMessage);
										 doubleMouseHover("GENERAL", "CORRESPONDENCE", locator);
									 }
				            	}else{
				            		System.out.println(customLogMessage="INFO: Invalid length");
				            		APP_LOGS.debug(customLogMessage);
				            	}
				            	break;
				            case "SiteAdmin":
				            	System.out.println(customLogMessage="INFO  : ==========>> \""+sheetName+"\" <<==========");
	            				APP_LOGS.debug(customLogMessage);
				            	if(pathArr.length==2){
				            		//do mouse hover and click
				            		category=pathArr[0].toString().trim();
				            		locator=pathArr[1].toString().trim();
				            		System.out.println(customLogMessage="ACTION: Performing click action on \""+sheetName+">"+category+"\"");
									APP_LOGS.debug(customLogMessage);
									singleMouseHover("SITE_ADMIN", locator);
				            	}else if(pathArr.length==3){
				            		//do mouse hover twice and click
				            		String subModName=pathArr[0].toString().trim();
				            		category=pathArr[1].toString().trim();
				            		locator=pathArr[2].toString().trim();
									 if(subModName.equals("Products")){
										 System.out.println(customLogMessage="ACTION: Performing click action on \""+sheetName+">"+subModName+">"+category+"\"");
										 APP_LOGS.debug(customLogMessage);
										 doubleMouseHover("SITE_ADMIN", "PRODUCTS", locator);
									 }else if(subModName.equals("Other")){
										 System.out.println(customLogMessage="ACTION: Performing click action on \""+sheetName+">"+subModName+">"+category+"\"");
										 APP_LOGS.debug(customLogMessage);
										 doubleMouseHover("SITE_ADMIN", "OTHERS", locator);
									 }else if(subModName.equals("Promos")){
										 System.out.println(customLogMessage="ACTION: Performing click action on \""+sheetName+">"+subModName+">"+category+"\"");
										 APP_LOGS.debug(customLogMessage);
										 doubleMouseHover("SITE_ADMIN", "PROMOS", locator);
									 }
				            	}else{
				            		System.out.println(customLogMessage="INFO: Invalid length");
				            		APP_LOGS.debug(customLogMessage);
				            	}
				            	break;
				            case "NavigationMenu":
				            	System.out.println(customLogMessage="INFO  : ==========>> \""+sheetName+"\" <<==========");
	            				APP_LOGS.debug(customLogMessage);
				            	break;
				            case "EngineeringPM":
				            	System.out.println(customLogMessage="INFO  : ==========>> \""+sheetName+"\" <<==========");
	            				APP_LOGS.debug(customLogMessage);
				            	if(pathArr.length==2){
				            		//do mouse hover and click
				            		category=pathArr[0].toString().trim();
				            		locator=pathArr[1].toString().trim();
				            		System.out.println(customLogMessage="ACTION: Performing click action on \""+sheetName+">"+category+"\"");
									 APP_LOGS.debug(customLogMessage);
									 singleMouseHover("ENGINEERING_PM", locator);
				            	}else if(pathArr.length==3){
				            		//do mouse hover twice and click
				            	}else{
				            		System.out.println(customLogMessage="INFO: Invalid length");
				            		APP_LOGS.debug(customLogMessage);
				            	}	
				            	break;
				            default: 
				            	System.out.println(customLogMessage="INFO: Invalid Sheet Name");
				            	APP_LOGS.debug(customLogMessage);
				        }
						verifyAdminUrlForErrors();
						verifyAdminHardErrors();
					}
				}
				file.close();
			} catch (Exception e) {
				pageloadErrorOrException=e.getMessage();
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError, dropDownElementSelectionError);
				return "FAIL";
			}
			System.out.println(customLogMessage="RESULT: PASS");
			APP_LOGS.debug(customLogMessage);
			return "PASS";
		}
//===================================================================================================
	
	public static String LogOutAdminSite() throws Exception {
			/* @HELP
			@class:			LIVE_Keywords
			@method:		LogOutAdminSite()
			@parameter:	None
			@notes:			Logout from admin site
			@returns:		"PASS" or "FAIL" with Exception in case if method not got executed because of some runtime exception 
			@END
			 */
		testcasename="LogOutAdminSite";
		APP_LOGS.debug("------------------------------------------------------------");
		System.out.println("\nINFO  : Executing '"+testcasename +"' Method");
	    APP_LOGS.debug("INFO  : Executing '"+testcasename +"' Method");
			System.out.println(customLogMessage="ACTION: Logging out from admin site");
			APP_LOGS.debug(customLogMessage);
			try{
				returnLocator("LOGOUT").click();
			}catch(Exception e){
				pageloadErrorOrException=e.getMessage();
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError, dropDownElementSelectionError);
				return "FAIL";
			}
			System.out.println(customLogMessage="RESULT: PASS");
			APP_LOGS.debug(customLogMessage);
			return "PASS";
		}
}