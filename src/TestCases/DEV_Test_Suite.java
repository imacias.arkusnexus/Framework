package TestCases;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import ConfigFiles.Config;

/**
 * @author Neha Chowdhry
 */

public class DEV_Test_Suite extends Config { 	

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ HOME PAGE LOAD VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
		public static void HomePageLoad() throws Exception {
	    	testcasename = "HomePageLoadVerification";
	    	try{
	    		pageLoadStartTime = System.currentTimeMillis();
	    		SiteURLs.clear();
	    		SiteURLs=readTestDataFromExcel(siteURLsExcel, 6, sitename);
	    		System.out.println("--> Navigating to the home page: "+SiteURLs.get(1));
	    		APP_LOGS.debug("--> Navigating to the home page: "+SiteURLs.get(1));
	    		driver.get(SiteURLs.get(1));
	        
	        if(isElementPresent("FANCY_BOX") == true){
				System.out.println("--> Closing the Fancy Box Pop-Up");
				APP_LOGS.debug("--> Closing the Fancy Box Pop-Up");
				returnLocator("FANCY_BOX").click();
				waitForLocator("PLP1");
			}
	        
	        if("DAVI".equals(sitename)){
	        	System.out.println("--> Refreshing the page");
				APP_LOGS.debug("--> Refreshing the page");
	        	driver.navigate().refresh();
	        	waitForLocator("MY_ACCOUNT");
	        }
	        
	        if(isElementPresent("FANCYBOX2") == true){
				System.out.println("--> Closing the Fancy Box Pop-Up");
				APP_LOGS.debug("--> Closing the Fancy Box Pop-Up");
				returnLocator("FANCY_BOX").click();
				waitForLocator("PLP1");
			}
			
	        if(isElementPresent("MY_ACCOUNT") == true) {
	        pageLoadEndTime = System.currentTimeMillis();
	        System.out.println(s9Space+"Home page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
	        APP_LOGS.debug(s9Space+"Home page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
	        }
	        
	        else {
	        	System.out.println("***** AUTOMATION TEST FAILED TO RUN");
	            APP_LOGS.debug("***** AUTOMATION TEST FAILED TO RUN");
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
	            Assert.fail();
	        }	        
			}catch(Exception e){
				pageloadErrorOrException=e.getMessage();
				System.out.println("***** EXCEPTION IN HomePageLoad METHOD --> "+pageloadErrorOrException);
				APP_LOGS.debug("***** EXCEPTION IN HomePageLoad METHOD --> "+pageloadErrorOrException);
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
				Assert.fail();
			}
	    } 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SEARCH VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	    public static void SearchForAProduct() throws Exception {
	    	testcasename = "SearchForAProduct";
	    	pageLoadStartTime = System.currentTimeMillis();
	    	String objectArray[]=null;
	    	String searchCountIdentifierValue;
	    	String sSearch=LocatorProps.getProperty("SEARCH_KEYWORD");
			
	    	try{
		    	System.out.println("--> Searching for a Product");
				APP_LOGS.debug("--> Searching for a Product");
				switch (sitename) {
				 case "SEVN":
				 case "FRYE":
				 case "JCTR":
					break;
					 
				 case "AQUA":
				 case "DLAM":
				 case "EAGL":
				 case "JOES":
				 case "NYDJ":
				 case "PAIG":
				 case "POLK":
				 case "SPDR":
					 			System.out.println("--> Clicking on search link");
					 			APP_LOGS.debug("--> Clicking on search link");
					 			waitForLocator("SEARCH_LINK");
					 			returnLocator("SEARCH_LINK").click();
					 			waitForLocator("SEARCH_BOX");
					 			System.out.println("--> Entering search keyword: "+ sSearch);
					 			APP_LOGS.debug("--> Entering search keyword: "+ sSearch);
					 			returnLocator("SEARCH_BOX").sendKeys(sSearch);
								System.out.println("--> Performing search by Hitting Enter");
								APP_LOGS.debug("--> Performing search by Hitting Enter");
					 			returnLocator("SEARCH_BOX").sendKeys(Keys.RETURN);
					 			waitForLocator("PLP1");
					 			pageLoadEndTime = System.currentTimeMillis();
					 			System.out.println(s9Space+"Search Results page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
					 			APP_LOGS.debug(s9Space+"Search Results page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
					 			System.out.println(s9Space+"Current URL: " + driver.getCurrentUrl());
					 			APP_LOGS.debug(s9Space+"Current URL: " + driver.getCurrentUrl());
					 			break;				
				 
				 default:					 		
					 		System.out.println("--> Clicking on search box");
							APP_LOGS.debug("--> Clicking on search box");
							waitForLocator("SEARCH_BOX");
					 		returnLocator("SEARCH_BOX").click();
					 		System.out.println("--> Clearing the search box field");
							APP_LOGS.debug("--> Clearing the search box field");
							returnLocator("SEARCH_BOX").clear();
					 		System.out.println("--> Entering search keyword: "+ sSearch);
				 			APP_LOGS.debug("--> Entering search keyword: "+ sSearch);
					 		returnLocator("SEARCH_BOX").sendKeys(sSearch);
							System.out.println("--> Performing search by Hitting Enter");
							APP_LOGS.debug("--> Performing search by Hitting Enter");
				 			returnLocator("SEARCH_BOX").sendKeys(Keys.RETURN);
					 		waitForLocator("PLP1");
					 		Thread.sleep(20000);
					 		pageLoadEndTime = System.currentTimeMillis();
					 		System.out.println(s9Space+"Search Results page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
					 		APP_LOGS.debug(s9Space+"Search Results page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
					 		System.out.println(s9Space+"Current URL: " + driver.getCurrentUrl());
					 		APP_LOGS.debug(s9Space+"Current URL: " + driver.getCurrentUrl());
				 break;
				}
				
				if(("SEVN".equals(sitename))|("JCTR".equals(sitename))|("FRYE".equals(sitename))){
					System.out.println("--> This client uses SLI for product search");
			 		APP_LOGS.debug("--> This client uses SLI for product search");					
				}else{
					waitForLocator("SEARCH_COUNT");
					objectArray = LocatorProps.getProperty("SEARCH_COUNT").split("__");
					searchCountIdentifierValue=objectArray[1].trim();
					
					List<WebElement> Count = driver.findElements(By.xpath(searchCountIdentifierValue));
					if (Count.size()>0)
					{
						System.out.println("--> Number of Search Results displayed on the page: "+ Count.size());		
						APP_LOGS.debug("--> Number of Search Results displayed on the page: "+ Count.size());	
					} else {
						failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
						Assert.fail();
						}
				}
			}
			catch(Exception e){
				pageloadErrorOrException=e.getMessage();
				System.out.println("***** GOT EXCEPTION IN SearchForAProduct METHOD --> "+pageloadErrorOrException);
				APP_LOGS.debug("***** GOT EXCEPTION IN SearchForAProduct METHOD --> "+pageloadErrorOrException);
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
				Assert.fail();
				}
	    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PLP VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void GoToPLP() throws Exception {
    	testcasename = "GoToPLP";
    	pageLoadStartTime = System.currentTimeMillis();
		
		try{	
			switch (sitename) {
			 case "ACME":
			 case "AQUA":
			 case "EAGL":
			 case "JONY":
			 case "LNTS":
			 case "NYDJ":
			 case "POLK":
			 case "SEVN":
				System.out.println("--> Navigating to the PLP");
				APP_LOGS.debug("--> Navigating to the PLP");
				returnLocator("PLP1").click();
				waitForLocator("PDP");
				pageLoadEndTime = System.currentTimeMillis();
				break;
				
			 case "JOES":
					System.out.println("--> Navigating to the PLP");
					APP_LOGS.debug("--> Navigating to the PLP");
					returnLocator("PLP1").click();
					waitForLocator("PLP2");
					returnLocator("PLP2").click();
					waitForLocator("PLP3");
					returnLocator("PLP3").click();
					waitForLocator("PDP");
					pageLoadEndTime = System.currentTimeMillis();
					break;
				
			 case "DAVI":
			 case "DLAM":
			 case "FRYE":
			 case "JCTR":
			 case "PAIG":
			 case "SPDR":
				System.out.println("--> Navigating to the PLP");
				APP_LOGS.debug("--> Navigating to the PLP");
				Actions builder = new Actions(driver);
			    builder.moveToElement(returnLocator("PLP1")).perform();
			    returnLocator("PLP2").click();
			    waitForLocator("PDP");
			    pageLoadEndTime = System.currentTimeMillis();
				break;
			}
			
			if(isElementPresent("PDP") == true) {
				System.out.println("--> PLP loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
				APP_LOGS.debug("--> PLP loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
				} else {
	    			failure(testcasename);
	    			Assert.fail();
	    		}
		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ QUICKVIEW VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void QuickviewVerification() throws Exception {
		testcasename = "QuickviewVerification";
    	pageLoadStartTime = System.currentTimeMillis();

		System.out.println("--> Clicking on quickview");
		APP_LOGS.debug("--> Clicking on quickview");
		try{		
			switch (sitename) {
			case "FRYE":
			case "JOES":
			case "JONY":
			case "LNTS":
			case "SEVN":
			case "SPDR":
				System.out.println("--> Quickview is disabled for this client");
				APP_LOGS.debug("--> Quickview is disabled for this client");
				break;

			case "EAGL":
			case "JCTR":
			 Thread.sleep(9000);
			 Actions builder1 = new Actions(driver);
		     builder1.moveToElement(returnLocator("QV_PDP")).perform();
		     returnLocator("QUICKVIEW").click();
		     waitForLocator("QV_ADD_TO_CART");
		    
		     System.out.println("--> Selecting the size on quickview pop-up");
			 APP_LOGS.debug("--> Selecting the size on quickview pop-up");
			 returnLocator("QV_SIZE").click();
			 waitForLocator("QV_ADD_TO_CART");	
			 
			 System.out.println("--> Closing the quickview pop-up");
			 APP_LOGS.debug("--> Closing the quickview pop-up");
			 returnLocator("QV_CLOSE").click();
			 waitForLocator("PDP");
			 break;
			 
			case "POLK":
				Thread.sleep(9000);
				Actions builder2 = new Actions(driver);
			    builder2.moveToElement(returnLocator("QV_PDP")).perform();
			    returnLocator("QUICKVIEW").click();
			    waitForLocator("QV_ADD_TO_CART");
				 
				System.out.println("--> Closing the quickview pop-up");
				APP_LOGS.debug("--> Closing the quickview pop-up");
				returnLocator("QV_CLOSE").click();
				waitForLocator("PDP");
				break;
				
				
			 default:
			 Thread.sleep(9000);
			 Actions builder = new Actions(driver);
		     builder.moveToElement(returnLocator("QV_PDP")).perform();
		     Thread.sleep(9000);
		     returnLocator("QUICKVIEW").click();
		     waitForLocator("QV_ADD_TO_CART");
		    
		     System.out.println("--> Selecting the size on quickview pop-up");
			 APP_LOGS.debug("--> Selecting the size on quickview pop-up");
			 returnLocator("QV_SIZE").click();
			 waitForLocator("QV_SIZE_VALUE");	
			 returnLocator("QV_SIZE_VALUE").click();
			 waitForLocator("QV_ADD_TO_CART");	
			 
			 if(isElementPresent("QV_CLOSE") == true){
			 System.out.println("--> Closing the quickview pop-up");
			 APP_LOGS.debug("--> Closing the quickview pop-up");
			 returnLocator("QV_CLOSE").click();
			 waitForLocator("PDP");
			 break;
			 }
			}
			 pageLoadEndTime = System.currentTimeMillis();
		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PDP VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void GoToPDP() throws Exception {
		testcasename = "GoToPDP";
    	pageLoadStartTime = System.currentTimeMillis();
		
		System.out.println("--> Navigating to the PDP");
		APP_LOGS.debug("--> Navigating to the PDP");
		try{
			if("JONY".equals(sitename)){
				driver.get("http://jony.qa.onestop.com/cosmetic-loaf/d/1024C9157?CategoryId=1001");
			    waitForLocator("ADD_TO_CART");
			    pageLoadEndTime = System.currentTimeMillis();
			}else{
			returnLocator("PDP").click();
			waitForLocator("ADD_TO_CART");
			pageLoadEndTime = System.currentTimeMillis();
			}
		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ RECOMMENDATIONS VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void RecommendationsVerification() throws Exception {
		testcasename = "RecommendationsVerification";
    	pageLoadStartTime = System.currentTimeMillis();
		
    	if (("LNTS".equals(sitename)) | ("SPDR".equals(sitename)) | ("JOES".equals(sitename))){
    		System.out.println("--> Product recommendations is not enabled for this client");
    		APP_LOGS.debug("--> Product recommendations is not enabled for this client");
    	}else{
		System.out.println("--> Clicking on the recommended product");
		APP_LOGS.debug("--> Clicking on the recommended product");
		Thread.sleep(9000);
		try{
			if(isElementPresent("RECOM_PDP_HOVER") == true){
			Actions builder = new Actions(driver);
		    builder.moveToElement(returnLocator("RECOM_PDP_HOVER")).perform();
		    Thread.sleep(3000);
		    returnLocator("RECOM_PDP").click();
		    waitForLocator("SIZE");
			}
			else {
				returnLocator("RECOM_PDP").click();
			    waitForLocator("ADD_TO_CART");
			}
		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    	}
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SIZE VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void SelectProductSize() throws Exception {
    	testcasename = "SelectProductSize";
		try{
			switch (sitename) {
			 case "EAGL":
			 case "LNTS":
			 case "POLK":
			 case "JONY":
				 break;
				 
			 case "AQUA":
			 case "DAVI":
			 case "FRYE":
			 case "JOES":
			 case "NYDJ":
			 case "PAIG":
			 case "SEVN":
			 case "SPDR":
				 if (isElementPresent("SIZE") == true) 
					{
					System.out.println("--> Selecting the size of the product");
					APP_LOGS.debug("--> Selecting the size of the product");
					returnLocator("SIZE").click();
					waitForLocator("SIZE_VALUE");	
					returnLocator("SIZE_VALUE").click();
					waitForLocator("ADD_TO_CART");	
					}else {
						System.out.println("--> No size is available for selection");
						APP_LOGS.debug("--> No size is available for selection");
					}
				 break;
				 
			 case "ACME":
			 case "DLAM":
			 case "JCTR":
			if (isElementPresent("SIZE") == true) 
			{
			System.out.println("--> Selecting the size of the product");
			APP_LOGS.debug("--> Selecting the size of the product");
			returnLocator("SIZE").click();
			waitForLocator("ADD_TO_CART");	
			}else {
				System.out.println("--> No size is available for selection");
				APP_LOGS.debug("--> No size is available for selection");
			}
			break;
			}
		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }    
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ADD TO CART / CHECKOUT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void AddToCartAndCheckout() throws Exception {
    	testcasename = "AddToCartAndCheckout";
		try{
			System.out.println("--> Clicking on the add to cart button");
			APP_LOGS.debug("--> Clicking on the add to cart button");
			returnLocator("ADD_TO_CART").click();
			waitForLocator("CHECKOUT");	
			
			pageLoadStartTime = System.currentTimeMillis();
			System.out.println("--> Navigating to the checkout page");
			APP_LOGS.debug("--> Navigating to the checkout page");
			
			switch (sitename) {
			 case "AQUA":
			 case "DLAM":
			 case "JOES":
			 case "LNTS":
			 case "SEVN":
				 Thread.sleep(9000);
				 Actions builder = new Actions(driver);
			     builder.moveToElement(returnLocator("CHECKOUT")).perform();
			     returnLocator("FINAL_CHECKOUT").click();
			     Thread.sleep(20000);
			     waitForLocator("SPCSHIPPING_EDIT");
			     pageLoadEndTime = System.currentTimeMillis();
			     break;			     

			 case "SPDR":
				 driver.get("https://spdr.qa.onestop.com/Checkout");
			     waitForLocator("SPCSHIPPING_EDIT");
			     pageLoadEndTime = System.currentTimeMillis();
			     break;
			     
			 case "PAIG":
				System.out.println("--> Navigating to the checkout page");
				APP_LOGS.debug("--> Navigating to the checkout page");
				driver.get("http://paig.qa.onestop.com/Cart");
				waitForLocator("FINAL_CHECKOUT");
				returnLocator("FINAL_CHECKOUT").click();
				Thread.sleep(30000);
				waitForLocator("SPCSHIPPING_EDIT");
				pageLoadEndTime = System.currentTimeMillis();
				break;
			     
			 case "FRYE":
				System.out.println("--> Navigating to the checkout page");
				APP_LOGS.debug("--> Navigating to the checkout page");
				driver.get("http://frye.qa.onestop.com/Cart");
				waitForLocator("FINAL_CHECKOUT");
				returnLocator("FINAL_CHECKOUT").click();
				Thread.sleep(20000);
				waitForLocator("SPCSHIPPING_EDIT");
			    pageLoadEndTime = System.currentTimeMillis();
			    break;
			     
			 case "DAVI":
			    System.out.println("--> Clicking on the checkout button");
				APP_LOGS.debug("--> Clicking on the checkout button");
				returnLocator("CHECKOUT").click();
				waitForLocator("SPCSHIPPING_EDIT");
			    pageLoadEndTime = System.currentTimeMillis();
			    break;
			     
			 case "ACME":
			 case "EAGL":
			 case "JCTR":
				System.out.println("--> Clicking on the checkout button");
				APP_LOGS.debug("--> Clicking on the checkout button");
				returnLocator("CHECKOUT").click();
				waitForLocator("FINAL_CHECKOUT");
				System.out.println("--> Clicking on the final checkout button");
				APP_LOGS.debug("--> Clicking on the final checkout button");
				returnLocator("FINAL_CHECKOUT").click();
				Thread.sleep(20000);
				waitForLocator("SPCSHIPPING_EDIT");
				pageLoadEndTime = System.currentTimeMillis();
				break;
				

			 case "NYDJ":
			 case "POLK":
			 case "JONY":
				System.out.println("--> Clicking on the checkout button");
				APP_LOGS.debug("--> Clicking on the checkout button");
				returnLocator("CHECKOUT").click();
				Thread.sleep(30000);
				waitForLocator("FINAL_CHECKOUT");
				Thread.sleep(20000);
				System.out.println("--> Clicking on the final checkout button");
				APP_LOGS.debug("--> Clicking on the final checkout button");
				returnLocator("FINAL_CHECKOUT").click();
				waitForLocator("SPCSHIPPING_EDIT");
				pageLoadEndTime = System.currentTimeMillis();
				break;
			}			
				
				if(isElementPresent("SPCSHIPPING_EDIT") == true) {
					System.out.println("--> Final checkout page loaded successfully in: "+(((pageLoadEndTime-pageLoadStartTime)/1000)-10)+" seconds");
					APP_LOGS.debug("--> Final checkout page loaded successfully in: "+(((pageLoadEndTime-pageLoadStartTime)/1000)-10)+" seconds");
					} else {
		    			failure(testcasename);
		    			Assert.fail();
		    		}
		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }  
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ENTER SHIPPING INFO ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void EnterShippingInfo() throws Exception {
    	testcasename = "EnterShippingInfo";
		try {
			System.out.println("--> Continuing with guest checkout");
			APP_LOGS.debug("--> Continuing with guest checkout");
			returnLocator("SPCSHIPPING_EDIT").click();
			waitForLocator("SPCSHIPPING_FIRST_NAME");
				
			System.out.println("--> Entering the first name");
			APP_LOGS.debug("--> Entering the first name");
			returnLocator("SPCSHIPPING_FIRST_NAME").click();
			returnLocator("SPCSHIPPING_FIRST_NAME").clear();
			returnLocator("SPCSHIPPING_FIRST_NAME").sendKeys("QA");
			
			System.out.println("--> Entering the last name");
			APP_LOGS.debug("--> Entering the last name");
			returnLocator("SPCSHIPPING_LAST_NAME").click();
			returnLocator("SPCSHIPPING_LAST_NAME").clear();
			returnLocator("SPCSHIPPING_LAST_NAME").sendKeys("QA");
			
			System.out.println("--> Entering the address");
			APP_LOGS.debug("--> Entering the address");
			returnLocator("SPCSHIPPING_ADDRESS").click();
			returnLocator("SPCSHIPPING_ADDRESS").clear();
			returnLocator("SPCSHIPPING_ADDRESS").sendKeys("3040 E. ANA. ST.");
			
			System.out.println("--> Entering the city");
			APP_LOGS.debug("--> Entering the city");
			returnLocator("SPCSHIPPING_CITY").click();
			returnLocator("SPCSHIPPING_CITY").clear();
			returnLocator("SPCSHIPPING_CITY").sendKeys("COMPTON");
			
			System.out.println("--> Entering the state");
			APP_LOGS.debug("--> Entering the state");
			returnLocator("SPCSHIPPING_STATE").click();
			Thread.sleep(9000);
			returnLocator("SPCSHIPPING_STATE_SELECT").click();
			
			System.out.println("--> Entering the zip code");
			APP_LOGS.debug("--> Entering the zip code");
			returnLocator("SPCSHIPPING_ZIPCODE").click();
			returnLocator("SPCSHIPPING_ZIPCODE").clear();
			returnLocator("SPCSHIPPING_ZIPCODE").sendKeys("90221");
			
			System.out.println("--> Entering the email address");
			APP_LOGS.debug("--> Entering the email address");
			returnLocator("SPCSHIPPING_EMAIL").click();
			returnLocator("SPCSHIPPING_EMAIL").clear();
			returnLocator("SPCSHIPPING_EMAIL").sendKeys("automation1@onestop.com");
			
			System.out.println("--> Entering the phone number");
			APP_LOGS.debug("--> Entering the phone number");
			returnLocator("SPCSHIPPING_PHONE").click();
			returnLocator("SPCSHIPPING_PHONE").clear();
			returnLocator("SPCSHIPPING_PHONE").sendKeys("3108947724");
			
			if(isElementPresent("SPCBILLING_FIRST_NAME") == true){
				System.out.println("--> Selecting the checkbox to enter the same shipping info");
				APP_LOGS.debug("--> Selecting the checkbox to enter the same shipping info");
				returnLocator("SPCBILLING_SAME_AS_SHIPPING").click();
				Thread.sleep(20000);
			}
		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }   
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SELECT SHIPPING CHOICE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void SelectShippingChoice() throws Exception {
    	testcasename = "SelectShippingChoice";
		
		try{
				System.out.println("--> Clicking on the continue button to select shipping choice");
				APP_LOGS.debug("--> Clicking on the continue button to select shipping choice");
				returnLocator("SPCSHIPPING_INFO_CONTINUE").click();
				waitForLocator("SPCSHIPPINGCHOICECONTINUE");
				Thread.sleep(20000);
				
				
				if(isElementPresent("ADDRESS_VERIFICATION") == true){
					System.out.println("--> Selecting Address on the Verification Pop-Up");
					APP_LOGS.debug("--> Selecting the address on the Verification Pop-Up");
					returnLocator("ADDRESS_VERIFICATION").click();
					waitForLocator("SELECT_ADDRESS");
					returnLocator("SELECT_ADDRESS").click();
					waitForLocator("SPCSHIPPINGCHOICECONTINUE");
					Thread.sleep(20000);
					((JavascriptExecutor) driver).executeScript("scroll(250,0);");
				}
				
				System.out.println("--> Clicking on the continue to payment info button");
				APP_LOGS.debug("--> Clicking on the continue to payment info button");
				returnLocator("SPCSHIPPINGCHOICECONTINUE").click();
				Thread.sleep(20000);
		}catch(Exception e){
			failure(testcasename);
			Assert.fail();
		}	
		}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PLACE ORDER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void placeOrder() throws Exception {
    	testcasename = "placeOrder";
		
		try{
				System.out.println("--> Entering the credit card number");
				APP_LOGS.debug("--> Clicking on the payment info tab");
				returnLocator("CREDIT_CARD_NO").sendKeys("4111111111111111");
				System.out.println("--> Entering the CVV code");
				APP_LOGS.debug("--> Clicking on the CVV code");
				returnLocator("CVV").sendKeys("111");
				System.out.println("--> Clicking on the expiration year button");
				APP_LOGS.debug("--> Clicking on the expiration year button");
				returnLocator("EXPIRATION_YEAR").click();
				System.out.println("--> Selecting the expiration year");
				APP_LOGS.debug("--> Selecting the expiration year");
				returnLocator("EXPIRATION_YEAR_SELECT").click();
				Thread.sleep(15000);
				
				if("HUDS".equals(sitename)){
					System.out.println("--> Selecting the expiration month");
					APP_LOGS.debug("--> Selecting the expiration month");
					Select state = new Select(returnLocator("EXPIRATION_MONTH"));
					state.selectByVisibleText("12 - December");
					Thread.sleep(15000);
					System.out.println("--> Clicking on the review order button");
					APP_LOGS.debug("--> Clicking on the review order button");
					returnLocator("REVIEW_ORDER").click();
					waitForLocator("PLACE_ORDER");
				}
				
				if("FRYE".equals(sitename)){
					System.out.println("--> Clicking on the review order button");
					APP_LOGS.debug("--> Clicking on the review order button");
					returnLocator("REVIEW_ORDER").click();
					waitForLocator("PLACE_ORDER");
				}
				
				System.out.println("--> Clicking on the place order button");
				APP_LOGS.debug("--> Clicking on the place order button");
				returnLocator("PLACE_ORDER").sendKeys(Keys.RETURN);
				waitForLocator("ORDER_ID");
				Thread.sleep(25000);
				String Order_ID = returnLocator("ORDER_ID").getText();
				if(isElementPresent("ORDER_ID") == true) {
					System.out.println("--> Order placed successfully and the Order ID is: " + Order_ID);
					APP_LOGS.debug("--> Order placed successfully and the Order ID is: " + Order_ID);
				} else {
					System.out.println("--> Unable to place order");
					APP_LOGS.debug("--> Unable to place order");
					Assert.fail();
				}
		} catch(Exception e){
					failure(testcasename);
					Assert.fail();
				}
    }	
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ METHOD CAPTURE ON SCRIPT FAILURE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void failure(String testcasename) throws Exception{
		String currentURL = driver.getCurrentUrl();
		ErrorList.add("Failed while performing " + testcasename +" ("+currentURL + ")");
		//verifyServerResponse();
		System.out.println("Capturing the screenshot");
		APP_LOGS.debug("Capturing the screenshot");
		captureScreenShot(sitename + "_" + envname + "_");
		System.out.println("Capturing the response headers on the page");
		APP_LOGS.debug("Capturing the response headers on the page");
		captureHeaders(sitename + "_" + envname + "_");
		System.out.println(":=> Failed while performing "+ testcasename +" ("+currentURL + ")");
		APP_LOGS.debug(":=> Failed while performing "+ testcasename +" ("+currentURL + ")");
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
}
