package TestCases;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import ConfigFiles.Config;

/**
 * @author Neha Chowdhry
 */

public class STAGE_Test_Suite extends Config {
	
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ HOME PAGE LOAD VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
		public static void HomePageLoad() throws Exception {
	    	testcasename = "HomePageLoadVerification";
	    	try{
	    		pageLoadStartTime = System.currentTimeMillis();
	    		SiteURLs.clear();
	    		SiteURLs=readTestDataFromExcel(siteURLsExcel, 4, sitename);
	    		System.out.println("--> Navigating to the home page: "+SiteURLs.get(1));
	    		APP_LOGS.debug("--> Navigating to the home page: "+SiteURLs.get(1));
	    		driver.get(SiteURLs.get(1));
	        
	    	  	
	        if(isElementPresent("FANCY_BOX") ){
				System.out.println("--> Closing the Fancy Box Pop-Up");
				APP_LOGS.debug("--> Closing the Fancy Box Pop-Up");
				returnLocator("FANCY_BOX").click();
				waitForLocator("PLP1");
			}
	        
	        if("DAVI".equals(sitename)){
	        	System.out.println("--> Refreshing the page");
				APP_LOGS.debug("--> Refreshing the page");
	        	driver.navigate().refresh();
	        	waitForLocator("MY_ACCOUNT");
	        }
			
	        if(isElementPresent("MY_ACCOUNT") ) {
	        pageLoadEndTime = System.currentTimeMillis();
	        System.out.println(s9Space+"Home page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
	        APP_LOGS.debug(s9Space+"Home page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
	        }
	        
	        else {
	        	System.out.println("***** AUTOMATION TEST FAILED TO RUN");
	            APP_LOGS.debug("***** AUTOMATION TEST FAILED TO RUN");
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
	            Assert.fail();
	        }	        
			}catch(Exception e){
				pageloadErrorOrException=e.getMessage();
				System.out.println("***** EXCEPTION IN HomePageLoad METHOD --> "+pageloadErrorOrException);
				APP_LOGS.debug("***** EXCEPTION IN HomePageLoad METHOD --> "+pageloadErrorOrException);
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
				Assert.fail();
			}
	    } 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PLP VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void GoToPLP() throws Exception {
    	testcasename = "GoToPLP";
    	pageLoadStartTime = System.currentTimeMillis();
		
    	Actions builder = new Actions(driver);
    	
		try{	
			switch (sitename) {		
			 case "JOES":
				  System.out.println("--> Navigating to the PLP");
					APP_LOGS.debug("--> Navigating to the PLP");
					returnLocator("PLP1").click();
					waitForLocator("PLP2");
					returnLocator("PLP2").click();
					waitForLocator("PLP3");
					returnLocator("PLP3").click();
					if(isElementPresent("FANCY_BOX")){
						returnLocator("FANCY_BOX").click();
					}
					waitForLocator("PDP");
					pageLoadEndTime = System.currentTimeMillis();
				 break;
			
			 case "EAGL":
			 case "LNTS":
			 case "NYDJ":			 
			 case "CAKE":
			    System.out.println("--> Navigating to the PLP");
				APP_LOGS.debug("--> Navigating to the PLP");
				returnLocator("PLP1").click();
				waitForLocator("PDP");
				pageLoadEndTime = System.currentTimeMillis();
				break;
				
			 case "JONY": 
				System.out.println("--> Navigating to the PLP");
				APP_LOGS.debug("--> Navigating to the PLP");
				builder.moveToElement(returnLocator("PLP1")).perform();
				waitForLocator("PLP2");
				returnLocator("PLP2").click(); 
				Thread.sleep(10000);
				if(isElementPresent("FANCY_BOX2")){  
					returnLocator("FANCY_BOX2").click();
				} 
				waitForLocator("PDP");
				pageLoadEndTime = System.currentTimeMillis();
				break;
				
			 case "AQUA":	 
			 case "SPLD":
			 case "DAVI": 
			 case "DLAM":
			 case "FRYE":
			 case "HUDS":
			 case "JCTR":
			 case "SPDR":
			 case "SEVN":
			 case "KATY":
			 case "PAIG":
			 case "PEBA":
				System.out.println("--> Navigating to the PLP");
				APP_LOGS.debug("--> Navigating to the PLP");
			    builder.moveToElement(returnLocator("PLP1")).perform();
			    waitForLocator("PLP2");
				returnLocator("PLP2").click(); 
			    waitForLocator("PDP");
			    pageLoadEndTime = System.currentTimeMillis();
				break;
			}
			
			if(isElementPresent("PDP") == true) {
				System.out.println("--> PLP loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
				APP_LOGS.debug("--> PLP loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
				} else {
	    			failure(testcasename);
	    			Assert.fail();
	    		}
		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PDP VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void GoToPDP() throws Exception {
		testcasename = "GoToPDP";
    	pageLoadStartTime = System.currentTimeMillis();
		
		System.out.println("--> Navigating to the PDP");
		APP_LOGS.debug("--> Navigating to the PDP");
		try{
			
			if("JONY".equals(sitename)){	
				returnLocator("PDP").click();
				Thread.sleep(30000);
			    pageLoadEndTime = System.currentTimeMillis();
			}			
			else if("SEVN".equals(sitename)){
					driver.get("http://sevn.stage.onestop.com/large-cosmetic-pouch-in-black-/d/13559C20535?CategoryId=5008");									
			}
			else if("KATY".equals(sitename)){	
				returnLocator("PDP").click();
				waitForLocator("SIZE");
			    pageLoadEndTime = System.currentTimeMillis();
			}
			else if("CAKE".equals(sitename)){	
				returnLocator("PDP").click();
				waitForLocator("SIZE");
			    pageLoadEndTime = System.currentTimeMillis();
			}
			else{
				returnLocator("PDP").click();
				if(! "NYDJ".equals(sitename))
				  { waitForLocator("ADD_TO_CART"); }
				waitForLocator("SIZE");
			 	pageLoadEndTime = System.currentTimeMillis();
			    }
		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SIZE VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void SelectProductSize() throws Exception {
    	testcasename = "SelectProductSize";
		try{
			switch (sitename) {
			 case "SEVN": 
			 case "EAGL":
			 case "FRYE":
			 case "LNTS":			 
			 case "SPLD":
			 case "AQUA":
				 break;
				 
			 case "JOES":
			 case "JONY":	 			
			 case "DAVI":
			 case "PAIG":
			 case "SPDR":
			 case "HUDS":
			 case "KATY":
			 case "CAKE":
			 	 if (isElementPresent("SIZE") == true) 
					{
					System.out.println("--> Selecting the size of the product");
					APP_LOGS.debug("--> Selecting the size of the product");
					returnLocator("SIZE").click();
					Thread.sleep(20000);
					waitForLocator("SIZE_VALUE");	
					returnLocator("SIZE_VALUE").click();
					Thread.sleep(20000);
					waitForLocator("ADD_TO_CART");	
					}else {
						System.out.println("--> No size is available for selection");
						APP_LOGS.debug("--> No size is available for selection");
					}
				 break;
				 
			 case "DLAM":
			 case "JCTR":
			 case "NYDJ":	
			 case "PEBA":
				  if (isElementPresent("SIZE") == true) 
				  	 {
					  	System.out.println("--> Selecting the size of the product");
					  	APP_LOGS.debug("--> Selecting the size of the product");
					  	returnLocator("SIZE").click();
					  	waitForLocator("ADD_TO_CART");	
				  }else {
					  	System.out.println("--> No size is available for selection");
					  	APP_LOGS.debug("--> No size is available for selection");
			     }
			break;
			/*case "CAKE":
				System.out.println("--> Selecting the size of the product");
				APP_LOGS.debug("--> Selecting the size of the product");
				Thread.sleep(3000);
				returnLocator("SIZE").click();
				Thread.sleep(3000);
				waitForLocator("SIZE_VALUE");	
				returnLocator("SIZE_VALUE").click();
				Thread.sleep(3000);
				waitForLocator("ADD_TO_CART");	
			break;*/
		 }//switch
		
		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }    
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ADD TO CART / CHECKOUT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void AddToCartAndCheckout() throws Exception {
    	testcasename = "AddToCartAndCheckout";
    	
    	Actions builder = new Actions(driver);
    	
		try{
			System.out.println("--> Clicking on the add to cart button");
			APP_LOGS.debug("--> Clicking on the add to cart button");
			
			if(sitename.equals("PEBA")){
				((JavascriptExecutor) driver).executeScript("scroll(0,300);");
			}
			
			returnLocator("ADD_TO_CART").click();
			waitForLocator("CHECKOUT");
			
			pageLoadStartTime = System.currentTimeMillis();
			System.out.println("--> Navigating to the checkout page");
			APP_LOGS.debug("--> Navigating to the checkout page");
			
			switch (sitename) {
			case "PAIG":
				 Thread.sleep(10000);
				 //Actions builder = new Actions(driver);
				 builder.moveToElement(returnLocator("CART")).perform();
				 returnLocator("CHECKOUT").click();			
				 Thread.sleep(20000);			     
			     pageLoadEndTime = System.currentTimeMillis();
				break;
				
			case "SPLD":
			case "LNTS":
			case "CAKE":
				 Thread.sleep(9000);				
				 waitForLocator("CART");
				 returnLocator("CART").sendKeys(Keys.ENTER);
				 waitForLocator("FINAL_CHECKOUT");
				 returnLocator("FINAL_CHECKOUT").click();
			     Thread.sleep(20000);
			     waitForLocator("SPCSHIPPING_EDIT");
			     pageLoadEndTime = System.currentTimeMillis();
				break;
			 
		
			 case "SEVN":
				 waitForLocator("CART");
				 returnLocator("CART").sendKeys(Keys.ENTER);
				 returnLocator("CHECKOUT").click();
			     Thread.sleep(20000);
			     waitForLocator("SPCSHIPPING_EDIT");
			     pageLoadEndTime = System.currentTimeMillis();			     
				 break;
			 case "JOES": 
			 case "DLAM":			 
			 case "SPDR":
				 Thread.sleep(9000);				
				 waitForLocator("CART");
				 returnLocator("CART").sendKeys(Keys.ENTER);
				 //waitForLocator("CHECKOUT");
				 returnLocator("CHECKOUT").click();
			     Thread.sleep(20000);
			     waitForLocator("SPCSHIPPING_EDIT");
			     pageLoadEndTime = System.currentTimeMillis();
			     break;
			     
			 case "AQUA":
					System.out.println("--> Navigating to the checkout page");
					APP_LOGS.debug("--> Navigating to the checkout page");
					driver.get("https://aqua.stage.onestop.com/Checkout");					
					waitForLocator("SPCSHIPPING_EDIT");
				    pageLoadEndTime = System.currentTimeMillis();
				    break;
			     
			 case "FRYE":
				System.out.println("--> Navigating to the checkout page");
				APP_LOGS.debug("--> Navigating to the checkout page");
				driver.get("http://frye.stage.onestop.com/Cart");
				waitForLocator("FINAL_CHECKOUT");
				returnLocator("FINAL_CHECKOUT").click();
				Thread.sleep(20000);
				waitForLocator("SPCSHIPPING_EDIT");
			    pageLoadEndTime = System.currentTimeMillis();
			    break;
			
			 case "DAVI":
				    System.out.println("--> Clicking on the checkout button");
					APP_LOGS.debug("--> Clicking on the checkout button");
					driver.get("http://davi.stage.onestop.com/Cart");
					waitForLocator("FINAL_CHECKOUT");
					returnLocator("FINAL_CHECKOUT").click();
				    pageLoadEndTime = System.currentTimeMillis();
				    break;
			 case "EAGL":  
			 case "JONY":
				 Thread.sleep(9000);				
					 waitForLocator("CART");
					 returnLocator("CART").sendKeys(Keys.ENTER);										 
					 waitForLocator("FINAL_CHECKOUT");					 
					 Thread.sleep(20000);
					 System.out.println("--> Clicking on the final checkout button");
					 APP_LOGS.debug("--> Clicking on the final checkout button");
					 returnLocator("FINAL_CHECKOUT").click();
					 waitForLocator("SPCSHIPPING_EDIT");
					 pageLoadEndTime = System.currentTimeMillis();
					 break;
			 case "HUDS":
			 case "JCTR":
				System.out.println("--> Clicking on the checkout button");
				APP_LOGS.debug("--> Clicking on the checkout button");
				((JavascriptExecutor) driver).executeScript("scroll(250,0);");
				Thread.sleep(9000);
				returnLocator("CHECKOUT").click();
				waitForLocator("FINAL_CHECKOUT");
				System.out.println("--> Clicking on the final checkout button");
				APP_LOGS.debug("--> Clicking on the final checkout button");
				returnLocator("FINAL_CHECKOUT").click();
				Thread.sleep(20000);
				waitForLocator("SPCSHIPPING_EDIT");
				pageLoadEndTime = System.currentTimeMillis();
				break;
			
			case "NYDJ":
				 	System.out.println("--> Clicking on the checkout button");
					APP_LOGS.debug("--> Clicking on the checkout button");
					driver.get("http://nydj.stage.onestop.com/Cart");
					waitForLocator("FINAL_CHECKOUT");
					returnLocator("FINAL_CHECKOUT").click();
				    pageLoadEndTime = System.currentTimeMillis();
				    break;
				
			
			case "KATY":
			case "PEBA":
				System.out.println("--> Clicking on the checkout button");
				APP_LOGS.debug("--> Clicking on the checkout button");
				((JavascriptExecutor) driver).executeScript("scroll(0,500);");
				builder.moveToElement(returnLocator("CHECKOUT")).perform();
				returnLocator("CHECKOUT").click();
				Thread.sleep(20000);
				waitForLocator("FINAL_CHECKOUT");
				Thread.sleep(20000);
				System.out.println("--> Clicking on the final checkout button");
				APP_LOGS.debug("--> Clicking on the final checkout button");
				returnLocator("FINAL_CHECKOUT").click();
				waitForLocator("SPCSHIPPING_EDIT");
				pageLoadEndTime = System.currentTimeMillis();
				break;
				
			default:				
			    Thread.sleep(10000);
   			    returnLocator("CHECKOUT").click();			
				Thread.sleep(20000);			     
				pageLoadEndTime = System.currentTimeMillis();
				break;
			}				
				
				if(isElementPresent("SPCSHIPPING_EDIT") == true) {
					System.out.println("--> Final checkout page loaded successfully in: "+(((pageLoadEndTime-pageLoadStartTime)/1000)-10)+" seconds");
					APP_LOGS.debug("--> Final checkout page loaded successfully in: "+(((pageLoadEndTime-pageLoadStartTime)/1000)-10)+" seconds");
					} else {
		    			failure(testcasename);
		    			Assert.fail();
		    		}
		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }  
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ENTER SHIPPING INFO ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void EnterShippingInfo() throws Exception {
    	testcasename = "EnterShippingInfo";
		try {
			
		switch (sitename) {
	   	 case "JOES":
		 case "AQUA":
		 case "DAVI":
		 case "DLAM":
		 case "EAGL":
		 case "FRYE":
		 case "JCTR":
		 case "JONY":
		 case "LNTS":
		 case "NYDJ":
		 case "PAIG":		 
		 case "SEVN":
		 case "SPDR":
		 case "SPLD":
		 case "CAKE":
		 case "KATY":
		 case "PEBA":
			
			if( ! isElementPresent("SPCSHIPPING_FIRST_NAME") ){
				System.out.println("--> Clicking on Shipping Info button");
				APP_LOGS.debug("--> Clicking on Shipping Info button");				
				waitForLocator("SPCSHIPPING_EDIT");
				returnLocator("SPCSHIPPING_EDIT").click();
				waitForLocator("SPCSHIPPING_FIRST_NAME");
			 }
			
			System.out.println("--> Continuing with guest checkout, entering the Shipping Info details");
			APP_LOGS.debug("--> Continuing with guest checkout, entering the Shipping Info details");
			break;			
		 
		}
		    
		    if("EAGL".equals(sitename))
		    { 
		    	if(isElementPresent("FANCY_BOX") == true)
		          {
				    System.out.println("--> Closing the Fancy Box Pop-Up");
				    APP_LOGS.debug("--> Closing the Fancy Box Pop-Up");
				    returnLocator("FANCY_BOX").click();
				  }
		    }
			System.out.println("--> Entering the first name");
			APP_LOGS.debug("--> Entering the first name");
			returnLocator("SPCSHIPPING_FIRST_NAME").click();
			returnLocator("SPCSHIPPING_FIRST_NAME").clear();
			returnLocator("SPCSHIPPING_FIRST_NAME").sendKeys("QA");
			
			System.out.println("--> Entering the last name");
			APP_LOGS.debug("--> Entering the last name");
			returnLocator("SPCSHIPPING_LAST_NAME").click();
			returnLocator("SPCSHIPPING_LAST_NAME").clear();
			returnLocator("SPCSHIPPING_LAST_NAME").sendKeys("QA");
			
			System.out.println("--> Entering the address");
			APP_LOGS.debug("--> Entering the address");
			returnLocator("SPCSHIPPING_ADDRESS").click();
			returnLocator("SPCSHIPPING_ADDRESS").clear();
			returnLocator("SPCSHIPPING_ADDRESS").sendKeys("3040 E. ANA. ST.");
			
			System.out.println("--> Entering the city");
			APP_LOGS.debug("--> Entering the city");
			returnLocator("SPCSHIPPING_CITY").click();
			returnLocator("SPCSHIPPING_CITY").clear();
			returnLocator("SPCSHIPPING_CITY").sendKeys("COMPTON");
			
			System.out.println("--> Entering the state");
			APP_LOGS.debug("--> Entering the state");
			
			if(sitename.equals("PEBA")){
				((JavascriptExecutor) driver).executeScript("scroll(0,300);");
			}
			
			returnLocator("SPCSHIPPING_STATE").click();
			Thread.sleep(9000);
			
			if(sitename.equals("SPDR")){
				((JavascriptExecutor) driver).executeScript("scroll(0,300);");
				Thread.sleep(9000);
				Actions builder = new Actions(driver);
				builder.moveToElement(returnLocator("SPCSHIPPING_STATE_SELECT")).doubleClick().build().perform();
				Thread.sleep(9000);
			}
			
			else {
				returnLocator("SPCSHIPPING_STATE_SELECT").click();
				Thread.sleep(9000);
			}
			
			System.out.println("--> Entering the email address");
			APP_LOGS.debug("--> Entering the email address");
			returnLocator("SPCSHIPPING_EMAIL").click();
			returnLocator("SPCSHIPPING_EMAIL").clear();
			returnLocator("SPCSHIPPING_EMAIL").sendKeys("automation1@onestop.com");
			
			System.out.println("--> Entering the zip code");
			APP_LOGS.debug("--> Entering the zip code");
			returnLocator("SPCSHIPPING_ZIPCODE").click();
			returnLocator("SPCSHIPPING_ZIPCODE").clear();
			returnLocator("SPCSHIPPING_ZIPCODE").sendKeys("90221");
			
			System.out.println("--> Entering the phone number");
			APP_LOGS.debug("--> Entering the phone number");
			returnLocator("SPCSHIPPING_PHONE").click();
			returnLocator("SPCSHIPPING_PHONE").clear();
			returnLocator("SPCSHIPPING_PHONE").sendKeys("3108947724");			
		 
		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }   
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SELECT SHIPPING CHOICE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void SelectShippingChoice() throws Exception {
    	testcasename = "SelectShippingChoice";
		
		try{
				System.out.println("--> Clicking on the continue button to select shipping choice");
				APP_LOGS.debug("--> Clicking on the continue button to select shipping choice");
				
				if(sitename.equals("PEBA")){
					((JavascriptExecutor) driver).executeScript("scroll(0,500);");
				}
				
				returnLocator("SPCSHIPPING_INFO_CONTINUE").click();
				Thread.sleep(20000);
				waitForLocator("SPCSHIPPINGCHOICECONTINUE");
				
				if(isElementPresent("ADDRESS_VERIFICATION") == true){
					System.out.println("--> Selecting Address on the Verification Pop-Up");
					APP_LOGS.debug("--> Selecting the address on the Verification Pop-Up");
					returnLocator("ADDRESS_VERIFICATION").click();
					waitForLocator("SELECT_ADDRESS");
					returnLocator("SELECT_ADDRESS").click();
					waitForLocator("SPCSHIPPINGCHOICECONTINUE");
					Thread.sleep(20000);
					((JavascriptExecutor) driver).executeScript("scroll(250,0);");
				}
				
				System.out.println("--> Clicking on the continue to payment info button");
				APP_LOGS.debug("--> Clicking on the continue to payment info button");
				returnLocator("SPCSHIPPINGCHOICECONTINUE").click();
				Thread.sleep(20000);
		}catch(Exception e){
			failure(testcasename);
			Assert.fail();
		}	
		}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PLACE ORDER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void placeOrder() throws Exception {
    	testcasename = "placeOrder";
		
		try{
								
				System.out.println("--> Entering the credit card number");
				APP_LOGS.debug("--> Clicking on the payment info tab");
				returnLocator("CREDIT_CARD_NO").sendKeys("4111111111111111");
				System.out.println("--> Entering the CVV code");
				APP_LOGS.debug("--> Clicking on the CVV code");
				returnLocator("CVV").sendKeys("111");
				System.out.println("--> Clicking on the expiration year button");
				APP_LOGS.debug("--> Clicking on the expiration year button");
				returnLocator("EXPIRATION_YEAR").click();
				System.out.println("--> Selecting the expiration year");
				APP_LOGS.debug("--> Selecting the expiration year");
				returnLocator("EXPIRATION_YEAR_SELECT").click();
				Thread.sleep(15000);
			
				
				if("FRYE".equals(sitename)){
					System.out.println("--> Clicking on the review order button");
					APP_LOGS.debug("--> Clicking on the review order button");
					returnLocator("REVIEW_ORDER").click();
					waitForLocator("PLACE_ORDER");
				}
							
				System.out.println("--> Clicking on the place order button");
				APP_LOGS.debug("--> Clicking on the place order button");
				returnLocator("PLACE_ORDER").sendKeys(Keys.RETURN);
				
				Thread.sleep(30000);
				waitForLocator("ORDER_ID");
				String Order_ID = returnLocator("ORDER_ID").getText();
				if(isElementPresent("ORDER_ID") == true) {
					System.out.println("--> Order placed successfully and the Order ID is: " + Order_ID);
					APP_LOGS.debug("--> Order placed successfully and the Order ID is: " + Order_ID);
				} else {
					System.out.println("--> Unable to place order");
					APP_LOGS.debug("--> Unable to place order");
					Assert.fail();
				}
		} catch(Exception e){
					failure(testcasename);
					Assert.fail();
				}
    }	
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ METHOD CAPTURE ON SCRIPT FAILURE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void failure(String testcasename) throws Exception{
		String currentURL = driver.getCurrentUrl();
		ErrorList.add("Failed while performing " + testcasename +" ("+currentURL + ")");
		//verifyServerResponse();
		System.out.println("Capturing the screenshot");
		APP_LOGS.debug("Capturing the screenshot");
		captureScreenShot(sitename + "_" + envname + "_");
		System.out.println("Capturing the response headers on the page");
		APP_LOGS.debug("Capturing the response headers on the page");
		captureHeaders(sitename + "_" + envname + "_");
		System.out.println(":=> Failed while performing "+ testcasename +" ("+currentURL + ")");
		APP_LOGS.debug(":=> Failed while performing "+ testcasename +" ("+currentURL + ")");
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
}  
