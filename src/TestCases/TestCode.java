package TestCases;


import org.openqa.selenium.Keys;

import ConfigFiles.Config;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class TestCode extends Config{
	
	
	
	@Given("^Open Chrome and go to product detail page on Turtle Beach US site$")
	public static void open_Chrome_and_go_to_product_detail_page_on_Turtle_Beach_US_site() throws Throwable {

		clearCookies();
        pageLoadStartTime = System.currentTimeMillis();
		SiteURLs.clear();
		SiteURLs=readTestDataFromExcel(siteURLsExcel, 0, sitename);
		System.out.println("--> Navigating to the home page: "+SiteURLs.get(1));
		APP_LOGS.debug("--> Navigating to the home page: "+SiteURLs.get(1));
		driver.get(SiteURLs.get(1));
		pageLoadEndTime = System.currentTimeMillis();
		waitForLocator("FANCYBOX"); 
		returnLocator("FANCYBOX").click();  
		waitForLocator("SEARCH_BOX"); 
		returnLocator("SEARCH_BOX").click();
		returnLocator("SEARCH_TEXTBOX").click();
		returnLocator("SEARCH_TEXTBOX").clear();
		returnLocator("SEARCH_TEXTBOX").sendKeys("bundle");
		returnLocator("SEARCH_TEXTBOX").sendKeys(Keys.ENTER);		
		waitForLocator("PDP_BUNDLE"); 
		System.out.println("--> Product Bundle Found!");
		APP_LOGS.debug("--> Product Bundle Found!");
	}
	
	@When("^Product bundle loads$")
	public static void product_bundle_loads() throws Throwable {
		returnLocator("PDP_BUNDLE").click();
		waitForLocator("SHOPPING_CART");
		System.out.println("--> Product Bundle Page Loaded!");
		APP_LOGS.debug("--> Product Bundle Page Loaded!");
//	    throw new PendingException();
	}
//	
	@Then("^User should see two products as one bundle$")
	public static void user_should_see_two_products_as_one_bundle() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		waitForLocator("BUNDLE_ONE");
		LogInfo("--> First Product Loaded!");
		waitForLocator("BUNDLE_TWO");
		System.out.println("--> Second Product Loaded!");
		APP_LOGS.debug("--> Second Product Loaded!");
//	    throw new PendingException();
	}
	@Then("^User checks out$")
	public static void user_checks_out() throws Throwable{
		waitForLocator("ADD_TO_CART"); 
		returnLocator("ADD_TO_CART").submit();
		waitForLocator("CHECKOUT"); 
		returnLocator("CHECKOUT").submit();
		waitForLocator("PAYPAL");
	}
	
}
