package TestCases;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.List;


import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.apache.poi.ss.usermodel.Cell;

import ConfigFiles.Config;

public class CMS_Links_Keywords extends Config{


			
	


	public static String GoToHomePageAndVerifyLoaded() throws Exception {
		/* @HELP
		@class:			CMS_Links_Keywords
		@method:		GoToHomePageAndVerifyLoaded()
		@parameter:	None
		@notes:			Navigates to specific client URL & verifies home page is loaded
		@returns:		"PASS" or "FAIL" with Exception in case if method not got executed because of some runtime exception 
		@END*/
	
		testcasename="GoToHomePageAndVerifyLoaded";
	
		try{
				
			System.out.println("INFO  :Entering URL-->http://acme.qa.onestop.com/admin");
			APP_LOGS.debug("INFO  :Entering URL-->http://acme.qa.onestop.com/admin");
			driver.get("http://acme.qa.onestop.com/admin");
			loginCredsAdmin.clear();
			loginCredsAdmin=readTestDataFromExcel(siteURLsExcel,7,"AllClientLogin");
			pageLoadStartTime = System.currentTimeMillis();
			
			System.out.println(customLogMessage="INFO  : Redirecting to Admin page");
			APP_LOGS.debug(customLogMessage);
		
			System.out.println(customLogMessage="INFO  : Enetring Username");
			APP_LOGS.debug(customLogMessage);
			driver.findElement(By.id("username-email")).sendKeys(loginCredsAdmin.get(1));
			Thread.sleep(3000);
			
		
			System.out.println(customLogMessage="INFO  : Enetring Password");
			APP_LOGS.debug(customLogMessage);
			Thread.sleep(2000);
			driver.findElement(By.id("password")).sendKeys(loginCredsAdmin.get(2));
			Thread.sleep(3000);
			
			System.out.println(customLogMessage="Action: Clicking on SignIn button");
			APP_LOGS.debug(customLogMessage);
			driver.findElement(By.xpath("//*[@id='page-container']/div[2]/form/fieldset/button")).click();
			Thread.sleep(10000);
			
			pageLoadEndTime = System.currentTimeMillis();
			System.out.println(customLogMessage="INFO  : My Account page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
			APP_LOGS.debug(customLogMessage);
			
								
		}catch(Exception e){
			failure(testcasename);
			Assert.fail();
			return "FAIL";
		}
		System.out.println(customLogMessage="RESULT: PASS");
		APP_LOGS.debug(customLogMessage);
		return "PASS";
	}


	//==================================================================
				
			public static String verifyHardErrorandURLsOnCMS() throws Exception {	
	
				String excelData;
				
				
				
					Actions act = new Actions(driver);
					FileInputStream file = new FileInputStream(new File("C:\\Saucelabs_WD\\testdata\\CMS_Links.xlsx"));
					XSSFWorkbook workbook = new XSSFWorkbook(file);
					XSSFSheet sheet = workbook.getSheet("CMS");
					Iterator<Row> rowIterator = null;
					try{
						rowIterator = sheet.iterator();
					/*}catch(Exception e){
						System.out.println("Invalid Sheet Name");
						//failure(testcasename);
						Assert.fail();
						System.exit(1);
						//return "FAIL";*/
					
						while (rowIterator.hasNext()) {
							Row row = rowIterator.next();
							Iterator<Cell> cellIterator = row.cellIterator();
							while (cellIterator.hasNext()) {
								Cell cell = cellIterator.next();
								excelData=cell.getStringCellValue().trim();
							
					
							System.out.println("\n");
							APP_LOGS.debug("\n");
						Thread.sleep(10000);
						System.out.println("Action : Clicking on link-->"+excelData);
						APP_LOGS.debug("Action : Clicking on link-->"+excelData);
						testcasename=excelData;
						try{
							
							driver.findElement(By.linkText(excelData)).click();
							verifyServerResponseOnCMS();
					 		driver.navigate().back();
								if(excelData.equals("Settings"))
									{
									try
										{
										driver.findElement(By.linkText("General")).isDisplayed();					
										System.out.println("Action: Clicking on Settings arrow button");
										APP_LOGS.debug("Action: Clicking on Settings arrow button");
										}catch (Exception e)
										{
								
										driver.findElement(By.xpath("//*[@id='menu']/div/ul/li[36]/h3/span/span")).click();
										}	
									
									 }
						    }catch(Exception e)
						    	{
						    		String message = excelData+"-- Link not found on page";
						    		failure(testcasename);
						    	}
					
								}
					
						}
		
			
					
				}catch(Exception e){
					failure(testcasename);
					//Assert.fail();
					return "FAIL";
				}
					System.out.println(customLogMessage="RESULT: PASS");
					APP_LOGS.debug(customLogMessage);
					
				return "PASS";
			}
				
				
				
			
				
			//==================================================================
			
			public static void failure(String testcasename) throws Exception{
				String currentURL = driver.getCurrentUrl();
				ErrorList.add("Failed while performing " + testcasename +" ("+currentURL + ")");
				verifyServerResponseOnCMS();
				System.out.println("Capturing the screenshot");
				APP_LOGS.debug("Capturing the screenshot");
				captureScreenShot(sitename + "_" + envname + "_");
				System.out.println("Capturing the response headers on the page");
				APP_LOGS.debug("Capturing the response headers on the page");
				captureHeaders(sitename + "_" + envname + "_");
				System.out.println(":=> Failed while performing "+ testcasename +" ("+currentURL + ")");
				APP_LOGS.debug(":=> Failed while performing "+ testcasename +" ("+currentURL + ")");
				//sendTextAlert();
		    }
				
		}
		
				
		



