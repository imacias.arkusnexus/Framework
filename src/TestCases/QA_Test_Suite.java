package TestCases;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import ConfigFiles.Config;

/**
 * @author Neha Chowdhry
 */

public class QA_Test_Suite extends Config {
	
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ HOME PAGE LOAD VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
		public static void HomePageLoad() throws Exception {
	    	testcasename = "HomePageLoadVerification";
	    	try{
	    		pageLoadStartTime = System.currentTimeMillis();
	    		SiteURLs.clear();
	    		SiteURLs=readTestDataFromExcel(siteURLsExcel, 2, sitename);
	    		printConsoleAndLog("--> Navigating to the home page: "+SiteURLs.get(1));
	    		driver.get(SiteURLs.get(1));
		        pageLoadEndTime = System.currentTimeMillis();
		     if(sitename.equals("TRAU") || sitename.equals("TRNZ"))
		     {
		    	 	printConsoleAndLog("--> Login in Aus");
					returnLocator("PASSWORD").sendKeys("onestop1");
					waitForLocator("ENTERB");
					returnLocator("ENTERB").click();
		     }
		    if(sitename.equals("FRYE"))
		    {
		    	printConsoleAndLog("Keys.ESCAPE");
/*		    	driver.getKeyboard().sendKeys(Keys.ESCAPE);*/
		    }
		    else{
	        if(isElementPresent("FANCY_BOX")){
	        	printConsoleAndLog("--> Closing the Fancy Box Pop-Up");
				returnLocator("FANCY_BOX").click();
				waitForLocator("PLP1");
			}
		    }
	        if(isElementPresent("MY_ACCOUNT")) {
	        pageLoadEndTime = System.currentTimeMillis();
	        printConsoleAndLog(s9Space+"Home page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
	        }
	        
	        else {
	        	printConsoleAndLog("***** AUTOMATION TEST FAILED TO RUN");
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
	            Assert.fail();
	        }	 
			}catch(Exception e){
				pageloadErrorOrException=e.getMessage();
				printConsoleAndLog("***** EXCEPTION IN HomePageLoad METHOD --> "+pageloadErrorOrException);
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
				Assert.fail();
			}
	    } 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SEARCH VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	    public static void SearchForAProduct() throws Exception {
	    	testcasename = "SearchForAProduct";
	    	pageLoadStartTime = System.currentTimeMillis();
	    	String objectArray[]=null;
	    	String searchCountIdentifierValue;
	    	String sSearch=LocatorProps.getProperty("SEARCH_KEYWORD");
			
	    	try{
	    		printConsoleAndLog("--> Searching for a Product");
				switch (sitename) {
				 case "FRYE":
				 case "JCTR":
				 case "LNTS":
				 case "SEVN":
				 case "SPLD":
				 case "TRTL":
				 case "TRAU":
				 case "TRNZ":
					break;
					 
				 case "AQUA":
				 case "CAKE":				 
				 case "DLAM":
				 case "EAGL":
				 case "JOES":
				 case "KATY":
				 case "NYDJ":
				 case "PAIG":
				 case "SPDR":
				 case "TRYN":
					 			printConsoleAndLog("--> Clicking on search link");
					 			waitForLocator("SEARCH_LINK");
					 			returnLocator("SEARCH_LINK").click();
					 			waitForLocator("SEARCH_BOX");
					 			printConsoleAndLog("--> Entering search keyword: "+ sSearch);
					 			returnLocator("SEARCH_BOX").sendKeys(sSearch);
					 			printConsoleAndLog("--> Performing search by Hitting Enter");
					 			returnLocator("SEARCH_BOX").sendKeys(Keys.RETURN);
					 			waitForLocator("PLP1");
					 			pageLoadEndTime = System.currentTimeMillis();
					 			printConsoleAndLog(s9Space+"Search Results page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
					 			printConsoleAndLog(s9Space+"Current URL: " + driver.getCurrentUrl());
					 			break;				
				 
				 default:	
					 		printConsoleAndLog("--> Clicking on search box");
							waitForLocator("SEARCH_BOX");
					 		returnLocator("SEARCH_BOX").click();
					 		printConsoleAndLog("--> Clearing the search box field");
							returnLocator("SEARCH_BOX").clear();
							printConsoleAndLog("--> Entering search keyword: "+ sSearch);
					 		returnLocator("SEARCH_BOX").sendKeys(sSearch);
					 		printConsoleAndLog("--> Performing search by Hitting Enter");
				 			returnLocator("SEARCH_BOX").sendKeys(Keys.RETURN);
					 		waitForLocator("PLP1");
					 		Thread.sleep(20000);
					 		pageLoadEndTime = System.currentTimeMillis();
					 		printConsoleAndLog(s9Space+"Search Results page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
					 		printConsoleAndLog(s9Space+"Current URL: " + driver.getCurrentUrl());
				 break;
				}
				
				if((sitename.equals("SEVN"))||(sitename.equals("JCTR"))||(sitename.equals("LNTS"))||(sitename.equals("FRYE"))||(sitename.equals("SPLD"))){
					printConsoleAndLog("--> This client uses SLI for product search");			
				}else{
					waitForLocator("SEARCH_COUNT");
					objectArray = LocatorProps.getProperty("SEARCH_COUNT").split("__");
					searchCountIdentifierValue=objectArray[1].trim();
					
					List<WebElement> Count = driver.findElements(By.xpath(searchCountIdentifierValue));
					if (Count.size()>0)
					{
						printConsoleAndLog("--> Number of Search Results displayed on the page: "+ Count.size());
					} else {
						failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
						Assert.fail();
						}
				}
			}
			catch(Exception e){
				pageloadErrorOrException=e.getMessage();
				printConsoleAndLog("***** GOT EXCEPTION IN SearchForAProduct METHOD --> "+pageloadErrorOrException);
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
				Assert.fail();
				}
	    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PLP VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void GoToPLP() throws Exception {
    	testcasename = "GoToPLP";
    	pageLoadStartTime = System.currentTimeMillis();
		
		try{	
			switch (sitename) {
			case "AQUA":
			case "CAKE":
			case "DAVI": 
			case "EAGL":			 
			case "LNTS":
			case "NYDJ":
			case "TRYN":
			case "TRTL":
			case "TRAU":
			case "TRNZ":
				printConsoleAndLog("--> Navigating to the PLP");
				returnLocator("PLP1").click();
				waitForLocator("PDP");
				pageLoadEndTime = System.currentTimeMillis();
				break;
				
			 case "JOES":
				 	printConsoleAndLog("--> Navigating to the PLP");
					if(isElementPresent("FANCY_BOX")){
						printConsoleAndLog("--> Closing the Fancy Box Pop-Up");
						returnLocator("FANCY_BOX").click();						
					}
					returnLocator("PLP1").click();
					waitForLocator("PLP2");
					returnLocator("PLP2").click();
					
					if(isElementPresent("FANCY_BOX")){
						printConsoleAndLog("--> Closing the Fancy Box Pop-Up");
						returnLocator("FANCY_BOX").click();						
					}
					
					waitForLocator("PLP3");
					returnLocator("PLP3").click();
					waitForLocator("PDP");
					pageLoadEndTime = System.currentTimeMillis();
					break;
			
			 case "LYCA":
				    returnLocator("PLP1").click();
					waitForLocator("PLP2");
					returnLocator("PLP2").click();
					pageLoadEndTime = System.currentTimeMillis();
					break;
				 
			 case "JONY":
				    returnLocator("PLP1").click();
					waitForLocator("PLP2");
					returnLocator("PLP2").click();
					waitForLocator("PDP");
				    pageLoadEndTime = System.currentTimeMillis();
					break;
					
			 case "DLAM":
			 case "FRYE":
			 case "JCTR":
			 case "PAIG":
			 case "SPDR":
			 case "SEVN":
			 case "KATY":
			 case "SPLD":
			 case "PEBA":
				printConsoleAndLog("--> Navigating to the PLP");
				Actions builder = new Actions(driver);
			    builder.moveToElement(returnLocator("PLP1")).perform();
			    returnLocator("PLP2").click();
			    waitForLocator("PDP");
			    pageLoadEndTime = System.currentTimeMillis();
				break;
			}
			
			if(!(sitename.equals("LYCA")) && !(sitename.equals("JONY")))
			   {
					if( isElementPresent("PDP")) {
						printConsoleAndLog("--> PLP loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
						} else {
			    			failure(testcasename);
			    			Assert.fail();
			    		}
			   }	
			
		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PDP VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void GoToPDP() throws Exception {
		testcasename = "GoToPDP";
    	pageLoadStartTime = System.currentTimeMillis();
		
    	printConsoleAndLog("--> Navigating to the PDP");
		try{
			
			if(sitename.equals("JONY")){
				driver.get("http://jony.qa.onestop.com/wristlet/d/1026C9163?CategoryId=1050");
			    waitForLocator("ADD_TO_CART");
			    pageLoadEndTime = System.currentTimeMillis();
			}
			else if(sitename.equals("LYCA")){               
		                    if(isElementPresent("FANCY_BOX2")){
		                    	                          Thread.sleep(10000); // as pop up displays after sometime. 
		                    	                          	printConsoleAndLog("--> Closing the Fancy Box Pop-Up");
		                                                    returnLocator("FANCY_BOX2").click();                                                                                      
		                    }
		                  
		                  ((JavascriptExecutor) driver).executeScript("scroll(0,250);"); // as user has to scroll down to click on Plan. 
		     		     Actions builder = new Actions(driver);
					     builder.moveToElement(returnLocator("PDP_MOUSEOVER")).perform();
					     returnLocator("PDP").click();
					     waitForLocator("ADD_TO_CART");
					     pageLoadEndTime = System.currentTimeMillis();
			}
			else if(sitename.equals("TRNZ"))
			{
				printConsoleAndLog("--> Navigating to PDP");
				driver.get("https://os-trnz.myshopify.com/collections/playstation-products/products/elite-800-headset");
				waitForLocator("ADD_TO_CART");
				pageLoadEndTime = System.currentTimeMillis();
			}
			else if(sitename.equals("TRAU"))
			{
				printConsoleAndLog("--> Navigating to PDP");
				driver.get("https://os-trau.myshopify.com/collections/xbox-products/products/stealth-700-headset-xbox-one");
				waitForLocator("ADD_TO_CART");
				pageLoadEndTime = System.currentTimeMillis();
			}
			else if(sitename.equals("TRTL"))
			{
				returnLocator("PDP").sendKeys(Keys.ENTER);
				waitForLocator("ADD_TO_CART");
				pageLoadEndTime = System.currentTimeMillis();
			}
			else{
				printConsoleAndLog("--> Scrolling down on the page");
				((JavascriptExecutor) driver).executeScript("scroll(0,250);");
				Thread.sleep(5000);	
			
			returnLocator("PDP").click();
			waitForLocator("ADD_TO_CART");
			pageLoadEndTime = System.currentTimeMillis();
			}
		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SIZE VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void SelectProductSize() throws Exception {
    	testcasename = "SelectProductSize";
    	
		try{
			switch (sitename) {
			 case "EAGL":
			 case "LNTS":
			 case "JONY":
			 case "TRTL":
			 case "TRAU":
			 case "TRNZ":
			 case "CAKE":
			 case "SEVN":
				 break;
				 
			 case "AQUA":
			 case "DAVI":
			 case "KATY":
			 case "PAIG":
			 case "SPDR":
			 case "SPLD":	 
			 case "JOES":	 
				 if (isElementPresent("SIZE")) {
					printConsoleAndLog("--> Selecting the size of the product");
					returnLocator("SIZE").click();
					waitForLocator("SIZE_VALUE");	
					returnLocator("SIZE_VALUE").click();
					waitForLocator("ADD_TO_CART");	
					}
				 else {
					 printConsoleAndLog("--> No size is available for selection");
					}
				 break;
							 
			 case "DLAM":
			 case "JCTR":
			 case "NYDJ":
			 case "PEBA":
			 case "TRYN":
			 case "FRYE":
				 if (isElementPresent("SIZE")){
					 printConsoleAndLog("--> Selecting the size of the product");
					 returnLocator("SIZE").click();
					 waitForLocator("ADD_TO_CART");	
				 }else {
					 printConsoleAndLog("--> No size is available for selection");
			      }
				 break;
				 
			 case "LYCA":
				 	printConsoleAndLog("--> Checking availability for the selected plan");
		 			returnLocator("ZIPCODE").sendKeys("90245");
		 			printConsoleAndLog("--> Clicking on the 'Checking availability' button");
		 			returnLocator("CHECK_AVAILABILITY").click();
					Thread.sleep(20000);
		 			waitForLocator("ADD_TO_CART");
		 			break;
				 
			}//switch
		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }    
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ADD TO CART / CHECKOUT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void AddToCartAndCheckout() throws Exception {
    	testcasename = "AddToCartAndCheckout";
		try{
			printConsoleAndLog("--> Scrolling down on the page");
			((JavascriptExecutor) driver).executeScript("scroll(0,250);");
			Thread.sleep(5000);
			
			
			//agregar click function addtocart
			if(sitename.equals("JONY")){
				printConsoleAndLog("--> Clicking on the add to cart button");			
			    returnLocator("ADD_TO_CART").click();
				waitForLocator("CHECKOUT");	
			}
			else{	
			printConsoleAndLog("--> Clicking on the add to cart button");
			Actions builder = new Actions(driver);
		    builder.moveToElement(returnLocator("ADD_TO_CART")).build().perform();
		    returnLocator("ADD_TO_CART").click();
			
			}
			
			pageLoadStartTime = System.currentTimeMillis();
			printConsoleAndLog("--> Navigating to the checkout page");
			
			switch (sitename) {
			
			case "TRTL":
			case "TRAU":
			case "TRNZ":
				waitForLocator("CHECKOUT");
				returnLocator("CHECKOUT").click();
				waitForLocator("PAYPAL");
				
				break;
			case "TRYN":
			case "FRYE":
				waitForLocator("SHOPPING_CART");
				returnLocator("SHOPPING_CART").click();
				waitForLocator("CHECKOUT");
				returnLocator("CHECKOUT").click();
				Thread.sleep(20000);
				waitForLocator("SPCSHIPPING_EDIT");
			    pageLoadEndTime = System.currentTimeMillis();
			    break;
			 			
			 case "JOES":	
				 printConsoleAndLog("--> Redirect to cart");
				 driver.get("https://joes.qa.onestop.com/Cart");
				 waitForLocator("FINAL_CHECKOUT");
				 returnLocator("FINAL_CHECKOUT").click();
			     Thread.sleep(10000);			     
			     pageLoadEndTime = System.currentTimeMillis();
			     break;			     

			 case "SPDR":
				 driver.get("https://spdr.qa.onestop.com/Checkout");
			     waitForLocator("SPCSHIPPING_EDIT");
			     pageLoadEndTime = System.currentTimeMillis();
			     break;
			     
			     
			
			 case "PAIG":
				printConsoleAndLog("--> Navigating to the checkout page");
				driver.get("http://paig.qa.onestop.com/Cart");
				waitForLocator("FINAL_CHECKOUT");
				returnLocator("FINAL_CHECKOUT").click();
				Thread.sleep(30000);
				waitForLocator("SPCSHIPPING_EDIT");
				pageLoadEndTime = System.currentTimeMillis();
				break;

			 case "DAVI":
				 printConsoleAndLog("--> Redirect to cart");
				 driver.get("http://davi.qa.onestop.com/Cart");
				 waitForLocator("FINAL_CHECKOUT");
				 returnLocator("FINAL_CHECKOUT").click();
				 break;
				 
			 case "LNTS":
				 printConsoleAndLog("--> Navigating to the checkout page");
				 waitForLocator("CHECKOUT");
				 returnLocator("CHECKOUT").click();
				 Thread.sleep(30000);
				 waitForLocator("FINAL_CHECKOUT");
				 returnLocator("FINAL_CHECKOUT").click();
				 Thread.sleep(30000);
			    break;
			     
			 case "EAGL":
			 case "JCTR":
			 case "SPLD":
			 case "KATY":
			 case "CAKE":
				printConsoleAndLog("--> Clicking on the checkout button");
				waitForLocator("CHECKOUT");
				returnLocator("CHECKOUT").click();
				waitForLocator("FINAL_CHECKOUT");
				printConsoleAndLog("--> Clicking on the final checkout button");
				returnLocator("FINAL_CHECKOUT").click();
				Thread.sleep(20000);
				waitForLocator("SPCSHIPPING_EDIT");
				pageLoadEndTime = System.currentTimeMillis();
				break;
				

			 case "NYDJ":
			 case "JONY":
			 case "PEBA":
			 case "AQUA":
				printConsoleAndLog("--> Clicking on the checkout button");
				waitForLocator("CHECKOUT");
				returnLocator("CHECKOUT").click();
				Thread.sleep(30000);
				waitForLocator("FINAL_CHECKOUT");
				Thread.sleep(20000);
				printConsoleAndLog("--> Clicking on the final checkout button");
				returnLocator("FINAL_CHECKOUT").click();
				waitForLocator("SPCSHIPPING_EDIT");
				pageLoadEndTime = System.currentTimeMillis();
				break;
				
			 case "LYCA":
				 	printConsoleAndLog("--> Clicking on the checkout button");
					returnLocator("SHOPPING_CART").click();
			    	waitForLocator("CHECKOUT");
					returnLocator("CHECKOUT").click();
					Thread.sleep(30000);										
					waitForLocator("SPCSHIPPING_EDIT");
					pageLoadEndTime = System.currentTimeMillis();
					break;
				
			 case "SEVN":
				 	printConsoleAndLog("--> Clicking on the checkout button");
					//returnLocator("CHECKOUT").click();
					driver.get("https://sevn.qa.onestop.com/checkout");
					Thread.sleep(30000);										
					waitForLocator("SPCSHIPPING_EDIT");
					pageLoadEndTime = System.currentTimeMillis();
					break;
				
			}			
				
				if(isElementPresent("SPCSHIPPING_EDIT")) {
					printConsoleAndLog("--> Final checkout page loaded successfully in: "+(((pageLoadEndTime-pageLoadStartTime)/1000)-10)+" seconds");
					} else {
		    			failure(testcasename);
		    			Assert.fail();
		    		}
		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }  
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ENTER SHIPPING INFO ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void EnterShippingInfo() throws Exception {
    	testcasename = "EnterShippingInfo";
    	Actions builder = new Actions(driver);
    	
		try {
			printConsoleAndLog("--> Continuing with guest checkout");
			
			if (sitename.equals("EAGL") && isElementPresent("FANCY_BOX"))
			{ 
				returnLocator("FANCY_BOX").click(); 
			}	
								
			if( ! isElementPresent("SPCSHIPPING_FIRST_NAME") ){
				printConsoleAndLog("--> Clicking on Shipping Info button");		
				waitForLocator("SPCSHIPPING_EDIT");
				returnLocator("SPCSHIPPING_EDIT").click();
				waitForLocator("SPCSHIPPING_FIRST_NAME");
			 }
			
			switch(sitename){
			
				case "TRYN":
				case "FRYE":
					printConsoleAndLog("--> Entering the Email Address");
					waitForLocator("SPCSHIPPING_EMAIL");
					returnLocator("SPCSHIPPING_EMAIL").click();
					returnLocator("SPCSHIPPING_EMAIL").clear();
					returnLocator("SPCSHIPPING_EMAIL").sendKeys("automation1@onestop.com");
				
					printConsoleAndLog("--> Entering the First Name");
					returnLocator("SPCSHIPPING_FIRST_NAME").click();
					returnLocator("SPCSHIPPING_FIRST_NAME").clear();
					returnLocator("SPCSHIPPING_FIRST_NAME").sendKeys("AUTOMATION1");
				
					printConsoleAndLog("--> Entering the Last Name");
					waitForLocator("SPCSHIPPING_LAST_NAME");
					returnLocator("SPCSHIPPING_LAST_NAME").click();
					returnLocator("SPCSHIPPING_LAST_NAME").clear();
					returnLocator("SPCSHIPPING_LAST_NAME").sendKeys("QA");
				
					printConsoleAndLog("--> Entering Address");
					waitForLocator("SPCSHIPPING_ADDRESS");
					returnLocator("SPCSHIPPING_ADDRESS").click();
					returnLocator("SPCSHIPPING_ADDRESS").clear();				
					returnLocator("SPCSHIPPING_ADDRESS").sendKeys("550 CONTINENTAL BLVD.");
					
					printConsoleAndLog("--> Scrolling down on the page");
					((JavascriptExecutor) driver).executeScript("scroll(0,550);");
					Thread.sleep(5000);
				
					printConsoleAndLog("--> Entering City Name");
					((JavascriptExecutor) driver).executeScript("scroll(0,650);");
					waitForLocator("SPCSHIPPING_CITY");
					returnLocator("SPCSHIPPING_CITY").click();
					returnLocator("SPCSHIPPING_CITY").clear();
					returnLocator("SPCSHIPPING_CITY").sendKeys("EL SEGUNDO");
					
					printConsoleAndLog("--> Selecting State");
					waitForLocator("SPCSHIPPING_STATE");
					returnLocator("SPCSHIPPING_STATE").click();
					returnLocator("SPCSHIPPING_STATE_SELECT").click();
					builder.moveToElement(returnLocator("SPCSHIPPING_STATE_SELECT")).doubleClick().build().perform();	
				
					printConsoleAndLog("--> Entering Zip Code");
					waitForLocator("SPCSHIPPING_ZIPCODE");
					returnLocator("SPCSHIPPING_ZIPCODE").click();
					returnLocator("SPCSHIPPING_ZIPCODE").clear();
					returnLocator("SPCSHIPPING_ZIPCODE").sendKeys("90245");
					
					printConsoleAndLog("--> Entering the Phone Number");
					waitForLocator("SPCSHIPPING_PHONE");
					returnLocator("SPCSHIPPING_PHONE").click();
					returnLocator("SPCSHIPPING_PHONE").clear();
					returnLocator("SPCSHIPPING_PHONE").sendKeys("3108947724");
				break;
			
				case "AQUA":
				case "CAKE":
				case "DAVI":
				case "EAGL":
				case "JCTR":
				case "JOES":
				case "JONY":
				case "KATY":
				case "LNTS":
				case "LYCA":
				case "NYDJ":
				case "PAIG":
				case "PEBA":
				case "SPDR":
					printConsoleAndLog("--> Continuing with guest checkout, entering the Shipping Info details");
					printConsoleAndLog("--> Entering the first name");
					returnLocator("SPCSHIPPING_FIRST_NAME").click();
					returnLocator("SPCSHIPPING_FIRST_NAME").clear();
					returnLocator("SPCSHIPPING_FIRST_NAME").sendKeys("QA");
			
					printConsoleAndLog("--> Entering the last name");
					returnLocator("SPCSHIPPING_LAST_NAME").click();
					returnLocator("SPCSHIPPING_LAST_NAME").clear();
					returnLocator("SPCSHIPPING_LAST_NAME").sendKeys("QA");
					
					printConsoleAndLog("--> Entering the address");
					returnLocator("SPCSHIPPING_ADDRESS").click();
					returnLocator("SPCSHIPPING_ADDRESS").clear();
					returnLocator("SPCSHIPPING_ADDRESS").sendKeys("3040 E. ANA. ST.");
					
					printConsoleAndLog("--> Entering the city");
					returnLocator("SPCSHIPPING_CITY").click();
					returnLocator("SPCSHIPPING_CITY").clear();
					returnLocator("SPCSHIPPING_CITY").sendKeys("COMPTON");
				break;
			}
			
			if("PAIG".equals("PAIG")){	
				printConsoleAndLog("--> Scrolling down on the page");
				((JavascriptExecutor) driver).executeScript("scroll(0,250);");
			}
			
			if(sitename.equals("SPDR")){
			printConsoleAndLog("--> Entering the state");
			((JavascriptExecutor) driver).executeScript("scroll(0,250);");
			returnLocator("SPCSHIPPING_STATE").click();
			Thread.sleep(9000);
			waitForLocator("SPCSHIPPING_STATE_SELECT");
			returnLocator("SPCSHIPPING_STATE_SELECT").click();			
			}
			
			else{
				printConsoleAndLog("--> Entering the state");
				returnLocator("SPCSHIPPING_STATE").click();
				Thread.sleep(9000);
				waitForLocator("SPCSHIPPING_STATE_SELECT");
				returnLocator("SPCSHIPPING_STATE_SELECT").click();
			}
			
			printConsoleAndLog("--> Entering the zip code");
			returnLocator("SPCSHIPPING_ZIPCODE").click();
			returnLocator("SPCSHIPPING_ZIPCODE").clear();
			returnLocator("SPCSHIPPING_ZIPCODE").sendKeys("90221");
			
			printConsoleAndLog("--> Scrolling down on the page");
			((JavascriptExecutor) driver).executeScript("scroll(0,250);");
			Thread.sleep(5000);
			
			printConsoleAndLog("--> Entering the email address");
			returnLocator("SPCSHIPPING_EMAIL").click();
			returnLocator("SPCSHIPPING_EMAIL").clear();
			returnLocator("SPCSHIPPING_EMAIL").sendKeys("automation1@onestop.com");
			
			printConsoleAndLog("--> Entering the phone number");
			returnLocator("SPCSHIPPING_PHONE").click();
			returnLocator("SPCSHIPPING_PHONE").clear();
			returnLocator("SPCSHIPPING_PHONE").sendKeys("3108947724");
			
			if(isElementPresent("SPCBILLING_FIRST_NAME")){
				printConsoleAndLog("--> Selecting the checkbox to enter the same shipping info");
				returnLocator("SPCBILLING_SAME_AS_SHIPPING").click();
				Thread.sleep(20000);
			}
		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }   
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SELECT SHIPPING CHOICE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void SelectShippingChoice() throws Exception {
    	testcasename = "SelectShippingChoice";
		
		try{
			if("TRYN".equals(sitename) || "FRYE".equals(sitename)){
				printConsoleAndLog("--> Clicking on the continue button to select shipping choice");
				waitForLocator("SPCSHIPPING_CHOICE");
				returnLocator("SPCSHIPPING_CHOICE").click();
				printConsoleAndLog("--> Clicking on the continue to payment info button");
				((JavascriptExecutor) driver).executeScript("scroll(250,0);");
				waitForLocator("SPCSHIPPINGCHOICECONTINUE");
				returnLocator("SPCSHIPPINGCHOICECONTINUE").sendKeys(Keys.ENTER);
				Thread.sleep(20000);
		
			}
			else{
				printConsoleAndLog("--> Clicking on the continue button to select shipping choice");
				returnLocator("SPCSHIPPING_INFO_CONTINUE").click();
				waitForLocator("SPCSHIPPINGCHOICECONTINUE");
				Thread.sleep(20000);
				
				
				if(isElementPresent("ADDRESS_VERIFICATION")){
					printConsoleAndLog("--> Selecting Address on the Verification Pop-Up");
					returnLocator("ADDRESS_VERIFICATION").click();
					waitForLocator("SELECT_ADDRESS");
					returnLocator("SELECT_ADDRESS").click();
					waitForLocator("SPCSHIPPINGCHOICECONTINUE");
					Thread.sleep(20000);
					if(!(sitename.equals("LYCA"))){ 
					  ((JavascriptExecutor) driver).executeScript("scroll(250,0);");  }
				}
				
				printConsoleAndLog("--> Clicking on the continue to payment info button");
				returnLocator("SPCSHIPPINGCHOICECONTINUE").click();
				Thread.sleep(20000);
			}
		}catch(Exception e){
			failure(testcasename);
			Assert.fail();
		}	
		}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PLACE ORDER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void placeOrder() throws Exception {
    	testcasename = "placeOrder";
		
		try{
			if(sitename.equals("TRYN") || sitename.equals("FRYE")){
				printConsoleAndLog("--> Verifying Sales Tax on SPC");	
				Thread.sleep(15000);
				String salesTax=returnLocator("SALES_TAX").getText().replace("\n", "").replace("Sales Tax:", "").trim();
				printConsoleAndLog(s9Space+"SPC Sales tax value is displayed as: "+salesTax);
			}
			else{
				printConsoleAndLog("--> Entering the credit card number");
				waitForLocator("CREDIT_CARD_NO");
				returnLocator("CREDIT_CARD_NO").sendKeys("4111111111111111");
				printConsoleAndLog("--> Entering the CVV code");
				returnLocator("CVV").sendKeys("111");
				
				printConsoleAndLog("--> Scrolling down on the page");
				((JavascriptExecutor) driver).executeScript("scroll(0,250);");
				Thread.sleep(5000);
				
				printConsoleAndLog("--> Clicking on the expiration year button");
				returnLocator("EXPIRATION_YEAR").click();
				printConsoleAndLog("--> Selecting the expiration year");
				returnLocator("EXPIRATION_YEAR_SELECT").click();
				Thread.sleep(15000);
				
				
				printConsoleAndLog("--> Clicking on the place order button");
				
				if(sitename.equals("LYCA")){
				  	returnLocator("ACCEPT_TERMS_CONDITIONS").click(); }
				
				returnLocator("PLACE_ORDER").sendKeys(Keys.RETURN);							
				
				if(sitename.equals("NYDJ")){ 
					printConsoleAndLog("--> Unable to place order for this customer " );
				  }
				
				else{
					  
					  Thread.sleep(15000);
					  waitForLocator("ORDER_ID");
				  	  Thread.sleep(25000);
				  	  String Order_ID = returnLocator("ORDER_ID").getText();
				  	  if(isElementPresent("ORDER_ID")) {
				  		printConsoleAndLog("--> Order placed successfully and the Order ID is: " + Order_ID);
				  	   }					  
				     else {
				    	 printConsoleAndLog("--> Unable to place order");
					     Assert.fail(); }
				      }}
				
		} catch(Exception e){
					failure(testcasename);
					Assert.fail();
				}
    }	
    
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ METHOD CAPTURE ON SCRIPT FAILURE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void failure(String testcasename) throws Exception{
		String currentURL = driver.getCurrentUrl();
		ErrorList.add("Failed while performing " + testcasename +" ("+currentURL + ")");
		//verifyServerResponse();
		printConsoleAndLog("Capturing the screenshot");
		captureScreenShot(sitename + "_" + envname + "_");
		printConsoleAndLog("Capturing the response headers on the page");
		captureHeaders(sitename + "_" + envname + "_");
		printConsoleAndLog(":=> Failed while performing "+ testcasename +" ("+currentURL + ")");
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
}  
