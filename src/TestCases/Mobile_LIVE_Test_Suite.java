package TestCases;

import java.util.List;


/**
 * @author Neha Chowdhry
 */
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import ConfigFiles.Config;

public class Mobile_LIVE_Test_Suite extends Config  {
	
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ HOME PAGE LOAD VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	public static void HomePageLoad() throws Exception {
    	testcasename = "HomePageLoadVerification";
    	
		System.out.println("--> Navigating to the home page");
		APP_LOGS.debug("--> Navigating to the home page");
		try{	
		pageLoadStartTime = System.currentTimeMillis();
		SiteURLs.clear();
		SiteURLs=readTestDataFromExcel(siteURLsExcel, 0, sitename);
		System.out.println(SiteURLs.get(1));
		APP_LOGS.debug(SiteURLs.get(1));
		driver.get(SiteURLs.get(1));
		Thread.sleep(10000);
		
		if("EAGL".equals(sitename)){
			driver.get("http://shop.eaglecreek.com/small-backpacks/l/343");
			waitForLocator("FANCY_BOX");
		}
		
		if(isElementPresent("FANCY_BOX") == true){
			  System.out.println("--> Closing the Fancy Box Pop-Up");
			  APP_LOGS.debug("--> Closing the Fancy Box Pop-Up");
			  returnLocator("FANCY_BOX").click();
			  waitForLocator("SEARCH_LINK"); 			  
		}		 
		
		if( isElementPresent("SEARCH_LINK") == true) {
		    pageLoadEndTime = System.currentTimeMillis();
		    System.out.println("--> Home page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
		    APP_LOGS.debug("--> Home page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
		 }else {
		    	System.out.println("--> Automation Test failed to run");
		        APP_LOGS.debug("--> Automation Test failed to run");
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError, dropDownElementSelectionError);
		        Assert.fail();
		    }    
				
		}//TRY
		catch(Exception e){
			pageloadErrorOrException=e.getMessage();
			System.out.println("***** EXCEPTION IN HomePageLoad METHOD --> "+pageloadErrorOrException);
			APP_LOGS.debug("***** EXCEPTION IN HomePageLoad METHOD --> "+pageloadErrorOrException);
			failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError, dropDownElementSelectionError);
			Assert.fail();
		}
	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SEARCH VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	    public static void SearchForAProduct() throws Exception {
	    	testcasename = "SearchForAProduct";
	    	pageLoadStartTime = System.currentTimeMillis();
	    	String objectArray[]=null;
	    	String searchCountIdentifierValue;
	    	String sSearch=LocatorProps.getProperty("SEARCH_KEYWORD");
			System.out.println("--> Searching for a product");
			APP_LOGS.debug("--> Searching for a product");
			try{
				System.out.println("--> Clicking on search link");
			 	APP_LOGS.debug("--> Clicking on search link");
			 	returnLocator("SEARCH_LINK").click();
				Thread.sleep(5000);
				waitForLocator("SEARCH_BOX");
								
				System.out.println("--> Entering search keyword: "+ sSearch);
			 	APP_LOGS.debug("--> Entering search keyword: "+ sSearch);
				returnLocator("SEARCH_BOX").sendKeys(sSearch);
		
				System.out.println("--> Performing search by Hitting Enter");
				APP_LOGS.debug("--> Performing search by Hitting Enter");
				returnLocator("SEARCH_BOX").sendKeys(Keys.RETURN);
								
				if ("SPDR".equals(sitename)){ 
					System.out.println("--> Re-direct to search page");
					APP_LOGS.debug("--> Re-direct to search page");
					driver.get("http://www.spyder.com/products/search?q=JACKETS"); 
				    waitForLocator("SEARCH_COUNT");
				}
				else if("JCTR".equals(sitename)){
					Thread.sleep(5000);
					if(isElementPresent("FANCY_BOX") == true){
						  System.out.println("--> Closing the Fancy Box Pop-Up");
						  APP_LOGS.debug("--> Closing the Fancy Box Pop-Up");
						  returnLocator("FANCY_BOX").click();
						 }
				}				
				else if("MDNA".equals(sitename)){
					System.out.println("--> No Search Count");
					APP_LOGS.debug("--> No Search Count");
				}
				else{
					waitForLocator("SEARCH_COUNT");
					pageLoadEndTime = System.currentTimeMillis();
				}
							
				
			    while ( "MDNA".equals(sitename) && !isElementPresent("PLP1")  )
				{
					System.out.println("--> Refreshing Page");
					APP_LOGS.debug("--> Refreshing Page");
					driver.navigate().refresh();
					Thread.sleep(5000);
				}
				
				if(isElementPresent("PLP1") == true) {
				    System.out.println("--> Search Results page loaded successfully in: "+(((pageLoadEndTime-pageLoadStartTime)/1000)-30) + " seconds");
					APP_LOGS.debug("--> Search Results page loaded successfully in: "+(((pageLoadEndTime-pageLoadStartTime)/1000) - 30) + " seconds");
				}else{
					failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError, dropDownElementSelectionError);
					Assert.fail();
				}
				
				if(! "MDNA".equals(sitename) ){
				    System.out.println("--> Counting the items from search page");
					APP_LOGS.debug("--> Counting the items from search page");
					waitForLocator("SEARCH_COUNT");
					objectArray = LocatorProps.getProperty("SEARCH_COUNT").split("__");
					searchCountIdentifierValue=objectArray[1].trim();
					List<WebElement> Count = driver.findElements(By.xpath(searchCountIdentifierValue));
				    {
			    	 if (Count.size()>0)
			    	    {
			    		  System.out.println("--> Number of Search Results displayed on the page: "+ Count.size());		
					      APP_LOGS.debug("--> Number of Search Results displayed on the page: "+ Count.size());		
				        }
			         else{ 
					     failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError, dropDownElementSelectionError);
					     Assert.fail();
				        }
				    }	
				}//if
				
			}catch(Exception e){
				pageloadErrorOrException=e.getMessage();
				System.out.println("***** EXCEPTION IN SearchForAProduct METHOD --> "+pageloadErrorOrException);
				APP_LOGS.debug("***** EXCEPTION IN SearchForAProduct METHOD --> "+pageloadErrorOrException);
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError, dropDownElementSelectionError);
				Assert.fail();
				}
	    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PLP VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	    public static void GoToPLP() throws Exception {
	    	testcasename = "GoToPLP";
	    	pageLoadStartTime = System.currentTimeMillis();
			
			System.out.println("--> Navigating to the PLP");
			APP_LOGS.debug("--> Navigating to the PLP");
			try{	 			
	 			if(("EAGL".equals(sitename))){
	 				driver.get("http://shop.eaglecreek.com/money-belts-neck-wallets/l/312");
	 			}
	 			else{					
					if("SEVN".equals(sitename)){
						if(isElementPresent("FANCY_BOX") == true){
							System.out.println("--> Closing the Fancy Box Pop-Up");
							APP_LOGS.debug("--> Closing the Fancy Box Pop-Up");
							returnLocator("FANCY_BOX").click();
							waitForLocator("PLP1");
						   }
					  }
						 			
				if("FRYE".equals(sitename) )
				  { Thread.sleep(30000); }
					
				System.out.println("--> Navigating to the PLP1");
			    APP_LOGS.debug("--> Navigating to the PLP1");
			    waitForLocator("PLP1");
				returnLocator("PLP1").click();
				
				if("FRYE".equals(sitename) )
				   { Thread.sleep(10000); }
				
				System.out.println("--> Navigating to the PLP2");
				APP_LOGS.debug("--> Navigating to the PLP2");
				waitForLocator("PLP2");
	 			returnLocator("PLP2").click();
	 			
	 			if(isElementPresent("PLP3") == true && ( ! "MDNA".equals(sitename) ) ){
	 				waitForLocator("PLP3");
	 				returnLocator("PLP3").click();
	 			}
	 			
	 			if("NYDJ".equals(sitename)){
					driver.get("http://www.nydj.com/tops/l/713");
	 			}
		
	 			Thread.sleep(8000);
				if("FRYE".equals(sitename) && isElementPresent("PLP4") == true){
					returnLocator("PLP4").click();
					((JavascriptExecutor) driver).executeScript("window.scrollBy(0,550)", "");
	 			}			
	 			
	 			if("JONY".equals(sitename)){
	 				waitForLocator("SEARCH_LINK");
	 				pageLoadEndTime = System.currentTimeMillis();	
					
					if(isElementPresent("SEARCH_LINK") == true) {
						System.out.println("--> PLP loaded successfully in: "+(((pageLoadEndTime-pageLoadStartTime)/1000) - 10) + " seconds");
						APP_LOGS.debug("--> PLP loaded successfully in: "+(((pageLoadEndTime-pageLoadStartTime)/1000) - 10) + " seconds");
						} else {
							failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError, dropDownElementSelectionError);
							Assert.fail();						
						}
	 			}else{
	 				if("MDNA".equals(sitename)){
	 					System.out.println("--> No fancybox");
						APP_LOGS.debug("--> No fancybox");
	 				}
	 				else if(isElementPresent("FANCY_BOX") == true){
	 			     System.out.println("--> Closing the Fancy Box Pop-Up");
	 				 APP_LOGS.debug("--> Closing the Fancy Box Pop-Up");
	 				 returnLocator("FANCY_BOX").click();	 				 
	 			}
	 			
 				if( "PEBA".equals(sitename) && isElementPresent("FANCY_BOX2"))
				{ returnLocator("FANCY_BOX2").click(); }
 				
 				if( "SPDR".equals(sitename) )
				{  ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,550)", ""); }
 				 				
 				waitForLocator("PDP");
				pageLoadEndTime = System.currentTimeMillis();	
				
				if(isElementPresent("PDP") == true) {
					System.out.println("--> PLP loaded successfully in: "+(((pageLoadEndTime-pageLoadStartTime)/1000) - 10) + " seconds");
					APP_LOGS.debug("--> PLP loaded successfully in: "+(((pageLoadEndTime-pageLoadStartTime)/1000) - 10) + " seconds");
					} else {
						failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError, dropDownElementSelectionError);
						Assert.fail();						
					}
	 			}
	 			}
			}catch(Exception e){
				pageloadErrorOrException=e.getMessage();
				System.out.println("***** EXCEPTION IN GoToPLP METHOD --> "+pageloadErrorOrException);
				APP_LOGS.debug("***** EXCEPTION IN GoToPLP METHOD --> "+pageloadErrorOrException);
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError, dropDownElementSelectionError);
				Assert.fail();
				}
	    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PDP VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	    public static void GoToPDP() throws Exception {
			testcasename = "GoToPDP";
	    	pageLoadStartTime = System.currentTimeMillis();
			
			System.out.println("--> Navigating to the PDP");
			APP_LOGS.debug("--> Navigating to the PDP");
			try{
				System.out.println("--> Scrolling the Page");
				APP_LOGS.debug("--> Scrolling the Page");
				((JavascriptExecutor) driver).executeScript("window.scrollBy(0,550)", "");
				Thread.sleep(5000);
				
				System.out.println("--> Clicking on Product");
				APP_LOGS.debug("--> Clicking on Product");
				
				if("JONY".equals(sitename)){
					driver.get("http://www.jny.com/stretch-textured-bracelet/d/1384C11080?CategoryId=1001");
				}else{				
					
					if("MDNA".equals(sitename))
					{ ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,550)", ""); }
					
				  returnLocator("PDP").click();
				}
				
				if("MDNA".equals(sitename))
				{ ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,550)", ""); }
				
				waitForLocator("ADD_TO_CART");
				pageLoadEndTime = System.currentTimeMillis();
				if(isElementPresent("ADD_TO_CART") == true) {
					System.out.println("--> PDP loaded successfully in: "+(((pageLoadEndTime-pageLoadStartTime)/1000)-10) + " seconds");
					APP_LOGS.debug("--> PDP loaded successfully in: "+(((pageLoadEndTime-pageLoadStartTime)/1000)-10) + " seconds");
					} else if(isElementPresent("SIZE") == true) {
						System.out.println("--> PDP loaded successfully in: "+(((pageLoadEndTime-pageLoadStartTime)/1000)-10) + " seconds");
						APP_LOGS.debug("--> PDP loaded successfully in: "+(((pageLoadEndTime-pageLoadStartTime)/1000)-10) + " seconds");
						} 
	
			}catch(Exception e){
				pageloadErrorOrException=e.getMessage();
				System.out.println("***** EXCEPTION IN GoToPDP METHOD --> "+pageloadErrorOrException);
				APP_LOGS.debug("***** EXCEPTION IN GoToPDP METHOD --> "+pageloadErrorOrException);
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError, dropDownElementSelectionError);
				Assert.fail();
				}
	    }  
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SIZE/COLOR VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	    public static void SelectProductSizeOrColor() throws Exception {
	    	testcasename = "SelectProductSizeOrColor";
	    	String strColorName = null;
			
			try{
				switch (sitename) {
				 
				 case "DLAM":
				 case "EAGL":
				 case "JONY":
				 case "LNTS":
				 case "MDNA":
				 break;	 
			 
				 case "FRYE": 
					 /*				 
					 //Refreshing Browser until color is displayed
					 while (! isElementPresent("COLOR_NAME") )
					 {
						 System.out.println("--> Refreshing Browser");
				 		 APP_LOGS.debug("--> Refreshing Browser");
						 driver.navigate().refresh();
						 Thread.sleep(8000);			 		  
					 }
					 				 
					 strColorName = new String(returnLocator("COLOR_NAME").getText());
					 
					 if ( strColorName.equals("") )
						{
						 	System.out.println("--> Selecting Color of the Product");
				 			APP_LOGS.debug("--> Selecting Color of the Product");
				 			returnLocator("COLOR").click();
				 			waitForLocator("ADD_TO_CART");	
					    }else {
				 			System.out.println("--> Color selected");
				 			APP_LOGS.debug("--> Color selected");
				 		}				
						     	
				  */
					 if (isElementPresent("SIZE") == true) 
				 		{
				 		   System.out.println("--> Selecting Size of the Product");
				 		   APP_LOGS.debug("--> Selecting Size of the Product");
				 		   returnLocator("SIZE").click();
				 		   waitForLocator("ADD_TO_CART");	
				 		}else {
				 		   System.out.println("--> No Size is available for selection");
				 		   APP_LOGS.debug("--> No Size is available for selection");
				 		}
				 break;
				 
					 
				 case "JCTR":
				 case "PEBA":
				 				 
					 if (isElementPresent("COLOR") == true)
					 {
							System.out.println("--> Selecting the size of the product");
							APP_LOGS.debug("--> Selecting the size of the product");
							((JavascriptExecutor) driver).executeScript("scroll(0,50)");
							waitForLocator("COLOR");
							returnLocator("COLOR").sendKeys(Keys.RETURN);
							waitForLocator("ADD_TO_CART");	
							Thread.sleep(9000);
					 }
					 if (isElementPresent("SIZE") == true) 
						{
						System.out.println("--> Selecting the size of the product");
						APP_LOGS.debug("--> Selecting the size of the product");
						((JavascriptExecutor) driver).executeScript("scroll(0,200)");
						waitForLocator("SIZE");
						returnLocator("SIZE").sendKeys(Keys.RETURN);
						waitForLocator("ADD_TO_CART");	
						Thread.sleep(9000);
						}else {
							System.out.println("--> No size is available for selection");
							APP_LOGS.debug("--> No size is available for selection");
						}
					
					
					 break;
					 
				 case "AQUA":		
				 case "NYDJ":				 
				 case "PAIG":
				 case "SEVN":
				 case "SPDR":
				 case "SPLD":
					  if (isElementPresent("SIZE") == true) 
						 {
						   System.out.println("--> Selecting Size of the Product");
						   APP_LOGS.debug("--> Selecting Size of the Product");
						
						   returnLocator("SIZE").click();
						   selectValueByVisibleText( returnLocator("SIZE"), "SIZE_VALUE");
						   Thread.sleep(9000);	 				 
						   waitForLocator("ADD_TO_CART");	
						   System.out.println("        Current URL: " + driver.getCurrentUrl());
						   APP_LOGS.debug("        Current URL: " + driver.getCurrentUrl());
					
						 }else {
							 System.out.println("--> No Size is available for selection");
							 APP_LOGS.debug("--> No Size is available for selection");
						 }
					  break;			 
				 }				
				}
				catch(Exception e){
					pageloadErrorOrException=e.getMessage();
					System.out.println("***** GOT EXCEPTION IN SelectProductSizeOrColor METHOD --> "+pageloadErrorOrException);
					APP_LOGS.debug("***** GOT EXCEPTION IN SelectProductSizeOrColor METHOD --> "+pageloadErrorOrException);
					failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError, dropDownElementSelectionError);
					Assert.fail();				}
			}
	    
	    
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ADD TO CART / CHECKOUT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	    public static void AddToCartAndCheckout() throws Exception {
	    	testcasename = "AddToCartAndCheckout";
			try{
				((JavascriptExecutor) driver).executeScript("scroll(0,250)");
				Thread.sleep(5000);
				
				System.out.println("--> Clicking on the add to cart button");
				APP_LOGS.debug("--> Clicking on the add to cart button");					  
				returnLocator("ADD_TO_CART").click(); 
				Thread.sleep(10000);
				pageLoadStartTime = System.currentTimeMillis();
				
				System.out.println("--> Navigating to the checkout page");
				APP_LOGS.debug("--> Navigating to the checkout page");
				
				if("SPDR".equals(sitename)){
					driver.get("http://www.spyder.com/Cart");
					waitForLocator("CHECKOUT");
					returnLocator("CHECKOUT").click();	
					waitForLocator("GUEST_CHECKOUT");					
					pageLoadEndTime = System.currentTimeMillis();
				}else{				
					
					returnLocator("SHOPPING_CART").click();	
					Thread.sleep(9000);
					if("FRYE".equals(sitename))
					{ Thread.sleep(20000); }
					waitForLocator("CHECKOUT");	
					returnLocator("CHECKOUT").click();	
					waitForLocator("GUEST_CHECKOUT");					
					pageLoadEndTime = System.currentTimeMillis();
					}
								
				if(isElementPresent("GUEST_CHECKOUT") == true){
					System.out.println("--> Final checkout page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
					APP_LOGS.debug("--> Final checkout page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
				}
				else{
					failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError, dropDownElementSelectionError);
					Assert.fail();
				}				
			}
			catch(Exception e){
				pageloadErrorOrException=e.getMessage();
				System.out.println("***** EXCEPTION IN AddToCartAndCheckout METHOD --> "+pageloadErrorOrException);
				APP_LOGS.debug("***** EXCEPTION IN AddToCartAndCheckout METHOD --> "+pageloadErrorOrException);
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError, dropDownElementSelectionError);
				Assert.fail();
				}
	    }
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

