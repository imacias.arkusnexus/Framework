package TestCases;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import ConfigFiles.Config;

/**
 * @author Neha Chowdhry
 */

public class DLL_QA_Test_Suite extends Config {
	
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ HOME PAGE LOAD VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
		public static void HomePageLoad() throws Exception {
	    	testcasename = "HomePageLoadVerification";
	    	try{
	    		pageLoadStartTime = System.currentTimeMillis();
	    		SiteURLs.clear();
	    		SiteURLs=readTestDataFromExcel(siteURLsExcel, 2, sitename);
	    		System.out.println("--> Navigating to the home page: "+SiteURLs.get(1));
	    		APP_LOGS.debug("--> Navigating to the home page: "+SiteURLs.get(1));
	    		driver.get(SiteURLs.get(1));
	    		System.out.println("--> Navigating to the home page2: "+SiteURLs.get(1)+"admin/os");
	    		driver.get(SiteURLs.get(1)+"admin/os");

	        if(isElementPresent("MY_ACCOUNT") == true) {
	        pageLoadEndTime = System.currentTimeMillis();
	        System.out.println(s9Space+"Login page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
	        APP_LOGS.debug(s9Space+"Login page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
	        }
	        
	        else {
	        	System.out.println("***** AUTOMATION TEST FAILED TO RUN");
	            APP_LOGS.debug("***** AUTOMATION TEST FAILED TO RUN");
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
	            Assert.fail();
	        }	        
			}catch(Exception e){
				pageloadErrorOrException=e.getMessage();
				System.out.println("***** EXCEPTION IN HomePageLoad METHOD --> "+pageloadErrorOrException);
				APP_LOGS.debug("***** EXCEPTION IN HomePageLoad METHOD --> "+pageloadErrorOrException);
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
				Assert.fail();
			}
	    } 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ BLUE MENU VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void GoToBM() throws Exception {
    	testcasename = "GoToBM";
    	pageLoadStartTime = System.currentTimeMillis();
		
		try{	
			switch (sitename) {
			 case "JOES":				 
			 case "AQUA":			
			 case "LNTS":
			 case "NYDJ":
			 case "POLK":
			 case "CAKE":						 
			 case "SPLD":
			 case "DAVI": 
			 case "JONY":
			 case "DLAM":
			 case "FRYE":
			 case "HUDS":
			 case "JCTR":
			 case "SPDR":
			 case "KATY": 
			case "SEVN":
				 	System.out.println("--> Navigating to the BM");
					APP_LOGS.debug("--> Navigating to the BM");
					waitForLocator("BM_USER");
					returnLocator("BM_USER").click();
					returnLocator("BM_USER").sendKeys("daniel.solis@onestop.com");
					waitForLocator("BM_PASS");
					returnLocator("BM_PASS").click();
					returnLocator("BM_PASS").sendKeys("DaSo2017!");
					waitForLocator("BM_SIGNB");
					returnLocator("BM_SIGNB").click();
					pageLoadEndTime = System.currentTimeMillis();
				 break;
				 
			 case "EAGL":
				 System.out.println("--> Navigating to the BM");
					APP_LOGS.debug("--> Navigating to the BM");
					waitForLocator("FANCYBOX");
					returnLocator("FANCYBOX").click();
					waitForLocator("BM_USER");
					returnLocator("BM_USER").click();
					returnLocator("BM_USER").sendKeys("daniel.solis@onestop.com");
					waitForLocator("BM_PASS");
					returnLocator("BM_PASS").click();
					returnLocator("BM_PASS").sendKeys("DaSo2017!");
					waitForLocator("BM_SIGNB");
					returnLocator("BM_SIGNB").click();
					pageLoadEndTime = System.currentTimeMillis();
				 break;
	
			}
			
			if(isElementPresent("LOGOUT") == true) {
				System.out.println("--> BM loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
				APP_LOGS.debug("--> BM loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
				} else {
	    			failure(testcasename);
	    			Assert.fail();
	    		}
		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SEARCH ORDER VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void SearchOrder() throws Exception {
		testcasename = "SearchOrder";
    	pageLoadStartTime = System.currentTimeMillis();
		
		System.out.println("--> Navigating to the Order");
		APP_LOGS.debug("--> Navigating to the Order");
		try{
			driver.get(SiteURLs.get(1)+"admin/os/searchorders.aspx");
			waitForLocator("ORDER_ID");
			returnLocator("ORDER_ID").click();
			returnLocator("ORDER_ID").sendKeys("254608");
			waitForLocator("SEARCH_ID");
			returnLocator("SEARCH_ID").click();
			waitForLocator("ORDER_DETAILS");
		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ORDER IN PROGRESS VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void OrderInProgress() throws Exception {
    	testcasename = "OrderInProgress";
		try{
			waitForLocator("SUMMARY");
			returnLocator("SUMMARY").click();
			waitForLocator("SUM_STAT");
			returnLocator("SUM_STAT").click();
			waitForLocator("SUM_STAT_VALUE");
			returnLocator("SUM_STAT_VALUE").click();
			waitForLocator("PLACED_ORDER");
			returnLocator("PLACED_ORDER").click();
			waitForLocator("EDIT");
			returnLocator("EDIT").click();
			waitForLocator("IN_PROGRESS");
			returnLocator("IN_PROGRESS").click();
			waitForLocator("CONFIRM");
		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }    

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ORDER SHIPPED VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void OrderShipped() throws Exception {
    	testcasename = "OrderShipped";
		try{
			waitForLocator("SUMMARY");
			returnLocator("SUMMARY").click();
			waitForLocator("SUM_STAT");
			returnLocator("SUM_STAT").click();
			waitForLocator("SUM_STAT_VALUE");
			returnLocator("SUM_STAT_VALUE").click();
			waitForLocator("PLACED_ORDER");
			returnLocator("PLACED_ORDER").click();			
			waitForLocator("EDIT");
			returnLocator("EDIT").click();
			waitForLocator("SHIPPED");
			returnLocator("SHIPPED").click();
			waitForLocator("SHIPPED_LAND");
			returnLocator("SHIPPED_LAND").click();
			waitForLocator("TRACKING_NUM");
			returnLocator("TRACKING_NUM").click();
			waitForLocator("NEW_STATUS");
			returnLocator("NEW_STATUS").click();
			waitForLocator("NEW_STATUS_VALUE");
			returnLocator("NEW_STATUS_VALUE").click();					
			waitForLocator("SUBMIT_SHIPPED");
		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }   
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CANCEL ORDER VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void CancelOrder() throws Exception {
    	testcasename = "OrderShipped";
		try{
			waitForLocator("SUMMARY");
			returnLocator("SUMMARY").click();
			waitForLocator("SUM_STAT");
			returnLocator("SUM_STAT").click();
			waitForLocator("SUM_STAT_VALUE");
			returnLocator("SUM_STAT_VALUE").click();
			waitForLocator("PLACED_ORDER");
			returnLocator("PLACED_ORDER").click();			
			waitForLocator("CANCEL_ORDER");
			returnLocator("CANCEL_ORDER").click();
			waitForLocator("CONFIRM_CANCEL");
			returnLocator("CONFIRM_CANCEL").click();
			waitForLocator("SUBMIT_CANCEL");
			//returnLocator("SUBMIT_CANCEL").click();
			//waitForLocator("SUBMIT_SHIPPED");
		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }   
    
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ RETURN ORDER WITH RMA ORDER VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void Return_RMA() throws Exception {
    	testcasename = "Return_RMA";
		try{
			waitForLocator("SUMMARY");
			returnLocator("SUMMARY").click();
			waitForLocator("SUM_STAT");
			returnLocator("SUM_STAT").click();
			waitForLocator("SUM_STAT_VALUE_SHIP");
			returnLocator("SUM_STAT_VALUE_SHIP").click();
			waitForLocator("PLACED_ORDER");
			returnLocator("PLACED_ORDER").click();			
			waitForLocator("CREATE_RMA");
			returnLocator("CREATE_RMA").click();
			waitForLocator("REASON_DROP");
			returnLocator("REASON_DROP").click();
			waitForLocator("REASON_VALUE");
			returnLocator("REASON_VALUE").click();
			waitForLocator("EXCHANGE");
			returnLocator("EXCHANGE").click();
		//	waitForLocator("RMA_SUB");
			//returnLocator("RMA_SUB").click();
		 }catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }   
    
    
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CREATE PROMO CODE VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void PromoCode() throws Exception {
		testcasename = "PromoCode";
    	pageLoadStartTime = System.currentTimeMillis();
		
		System.out.println("--> Navigating to the Order");
		APP_LOGS.debug("--> Navigating to the Order");
		try{
			driver.get(SiteURLs.get(1)+"admin/os/promocodes.aspx");
			waitForLocator("USER_TYPE");
			returnLocator("USER_TYPE").click();
			waitForLocator("USER_TYPE_VALUE");
		//	returnLocator("USER_TYPE_VALUE").click();
			returnLocator("USER_TYPE_VALUE").sendKeys(Keys.ARROW_DOWN);
			waitForLocator("SUBTYPE");
			returnLocator("SUBTYPE").click();
			waitForLocator("SUBTYPE_VALUE");
			//returnLocator("SUBTYPE_VALUE").click();
			returnLocator("SUBTYPE_VALUE").sendKeys(Keys.ARROW_DOWN);
			waitForLocator("PROMO_SUB1");
			returnLocator("PROMO_SUB1").click();
			waitForLocator("DISCOUNT");
			returnLocator("DISCOUNT").click();
			returnLocator("DISCOUNT").sendKeys("50");
			waitForLocator("DISCOUNT");
			returnLocator("DISCOUNT").click();
			waitForLocator("ACKNOWLEDGEMENT");
			returnLocator("ACKNOWLEDGEMENT").click();
			waitForLocator("PROMO_SUB2");
			returnLocator("PROMO_SUB2").click();

		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRODUCT CONFIGURATION CODE VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void ProductConfig() throws Exception {
		testcasename = "ProductConfig";
    	pageLoadStartTime = System.currentTimeMillis();
		
		System.out.println("--> Navigating to the Order");
		APP_LOGS.debug("--> Navigating to the Order");
		try{
			driver.get(SiteURLs.get(1)+"admin/os/productconfiguration.aspx");
			waitForLocator("PRODUCT_ID");
			returnLocator("PRODUCT_ID").click();
			returnLocator("PRODUCT_ID").sendKeys("999999");
			waitForLocator("CATEGORY");
			returnLocator("CATEGORY").click();
			waitForLocator("CATEGORY_VALUE");
			returnLocator("CATEGORY_VALUE").click();
			waitForLocator("TAXABLE");
			returnLocator("TAXABLE").click();
			waitForLocator("FACILITY");
			returnLocator("FACILITY").click();
			waitForLocator("FACILITY_VALUE");
			returnLocator("FACILITY_VALUE").click();
			waitForLocator("FULL_FACILITY");
			returnLocator("FULL_FACILITY").click();
			waitForLocator("FULL_FACILITY_VALUE");
			returnLocator("FULL_FACILITY_VALUE").click();
			waitForLocator("COUNTRY");
			returnLocator("COUNTRY").click();
			waitForLocator("COUNTRY_VALUE");
			returnLocator("COUNTRY_VALUE").click();
			waitForLocator("PRODUCT_SUB");

		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }
    
    
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CATEGORY CONFIGURATION CODE VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void CategoryConfig() throws Exception {
		testcasename = "CategoryConfig";
    	pageLoadStartTime = System.currentTimeMillis();
		
		System.out.println("--> Navigating to the Order");
		APP_LOGS.debug("--> Navigating to the Order");
		try{
			driver.get(SiteURLs.get(1)+"admin/os/categoryconfiguration.aspx");
			waitForLocator("CATEGORY_SEARCH");
			returnLocator("CATEGORY_SEARCH").click();
			returnLocator("CATEGORY_SEARCH").sendKeys("310");
			returnLocator("CATEGORY_SEARCH").sendKeys(Keys.ENTER);
			waitForLocator("CATEGORY_NAME");
			returnLocator("CATEGORY_NAME").click();
			returnLocator("CATEGORY_NAME").sendKeys("TEST");
			waitForLocator("PARENT_CATEGORY");
			returnLocator("PARENT_CATEGORY").click();
			returnLocator("PARENT_CATEGORY").sendKeys(Keys.TAB);
			waitForLocator("PARENT_CATEGORY_VALUE");
			returnLocator("PARENT_CATEGORY_VALUE").click();
			returnLocator("PARENT_CATEGORY_VALUE").sendKeys(Keys.TAB);
			waitForLocator("CATEGORY_CONF");
			returnLocator("CATEGORY_CONF").click();

		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }
    
    
    
    
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SITE CONFIGURATION VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void SiteConfig() throws Exception {
		testcasename = "SiteConfig";
    	pageLoadStartTime = System.currentTimeMillis();
		
		System.out.println("--> Navigating to the Order");
		APP_LOGS.debug("--> Navigating to the Order");
		try{
			driver.get(SiteURLs.get(1)+"admin/os/siteconfiguration.aspx");
			waitForLocator("EDIT_SITE_NAME");
			returnLocator("EDIT_SITE_NAME").click();
			returnLocator("EDIT_SITE_NAME").sendKeys("TEST");			
			waitForLocator("SETTING_VALUE");
			returnLocator("SETTING_VALUE").click();
			returnLocator("SETTING_VALUE").sendKeys("TEST");
			waitForLocator("NOTE_SITE");
			returnLocator("NOTE_SITE").click();
			returnLocator("NOTE_SITE").sendKeys("TEST");
			waitForLocator("CANCEL_EDIT");
			returnLocator("CANCEL_EDIT").click();

		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }
    
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRODUCT CONFIGURATION VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void ProductOrderConfig() throws Exception {
		testcasename = "ProductOrderConfig";
    	pageLoadStartTime = System.currentTimeMillis();
		
		System.out.println("--> Navigating to the Order");
		APP_LOGS.debug("--> Navigating to the Order");
		try{
			driver.get(SiteURLs.get(1)+"admin/os/productorderconfiguration.aspx");
			waitForLocator("PRODUCT_ORDER_CATEGORY");
			returnLocator("PRODUCT_ORDER_CATEGORY").click();	
			returnLocator("PRODUCT_ORDER_CATEGORY").sendKeys(Keys.TAB);
			waitForLocator("PRODUCT_ORDER_CATEGORY_VALUE");
			returnLocator("PRODUCT_ORDER_CATEGORY_VALUE").click();
			returnLocator("PRODUCT_ORDER_CATEGORY_VALUE").sendKeys(Keys.TAB);
			waitForLocator("PRODUCT_ORDER_COLUMN");
			returnLocator("PRODUCT_ORDER_COLUMN").click();
			waitForLocator("VIEW_PLP");
			returnLocator("VIEW_PLP").click();
			//waitForLocator("PLP_TITLE");
			
		}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ METHOD CAPTURE ON SCRIPT FAILURE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void failure(String testcasename) throws Exception{
		String currentURL = driver.getCurrentUrl();
		ErrorList.add("Failed while performing " + testcasename +" ("+currentURL + ")");
		//verifyServerResponse();
		System.out.println("Capturing the screenshot");
		APP_LOGS.debug("Capturing the screenshot");
		captureScreenShot(sitename + "_" + envname + "_");
		System.out.println("Capturing the response headers on the page");
		APP_LOGS.debug("Capturing the response headers on the page");
		captureHeaders(sitename + "_" + envname + "_");
		System.out.println(":=> Failed while performing "+ testcasename +" ("+currentURL + ")");
		APP_LOGS.debug(":=> Failed while performing "+ testcasename +" ("+currentURL + ")");
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
}  
