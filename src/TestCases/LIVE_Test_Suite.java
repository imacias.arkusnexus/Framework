package TestCases;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import java.lang.reflect.Array;
import java.util.ArrayList;
import ConfigFiles.Config;
import org.openqa.selenium.WebDriver;


public class LIVE_Test_Suite extends Config {
	//JOES, AQUA, DAVI
	
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ HOME PAGE LOAD VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	public static void HomePageLoad() throws Exception {
    	testcasename = "HomePageLoadVerification";

    	try{
    		LogInfo("-------------- HOME PAGE LOAD VERIFICATION -----------------");
    		
	    	clearCookies();
	    	
	        pageLoadStartTime = System.currentTimeMillis();
			SiteURLs.clear();
			
			SiteURLs=readTestDataFromExcel(siteURLsExcel, 0, sitename);
			
			System.out.println("--> Navigating to the home page: " + SiteURLs.get(1));
			pageLoadEndTime = System.currentTimeMillis();
			
			driver.get(SiteURLs.get(1));
			pageLoadEndTime = System.currentTimeMillis();
			
			System.out.println(driver.manage().window().getSize());

			switch(sitename) {
				case "DAVI":
				case "JOES":
					if(isElementPresent("FANCYBOX")){
						LogInfo("--> Closing the Fancy Box Pop-Up");
					   	returnLocator("FANCYBOX").click();
					   	waitForLocator("MY_ACCOUNT");
					}
			}		
			
			if(isElementPresent("MY_ACCOUNT")){
				LogInfo(s9Space + "Home page loaded successfully in: " + ((pageLoadEndTime-pageLoadStartTime)/1000) + " seconds");
			}
			        
			else {
				LogInfo("***** AUTOMATION TEST FAILED TO RUN");
			    failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
			    Assert.fail();
			}       
			
			LogInfo("--------------------------");
		}//try-end
    	
    	catch(Exception e){
			pageloadErrorOrException = e.getMessage();
			LogInfo("***** GOT EXCEPTION IN HomePageLoad METHOD --> " + pageloadErrorOrException);
			failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
			Assert.fail();
		}//catch-end
	}//HomePageLoad end
	
 //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MY ACCOUNT SIGN IN ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void MyAccountSignIn() throws Exception {    	
    	testcasename = "MyAccountSignIn";
				
		try{
    		LogInfo("----------- MY ACCOUNT SIGN IN ------------");
    		
			LogInfo("--> Clicking on the My Account button");
			pageLoadStartTime = System.currentTimeMillis();
			LogInfo(s9Space + "Current URL: " + driver.getCurrentUrl());
					
			returnLocator("MY_ACCOUNT").click();
		
			waitForLocator("USERNAME");
			pageLoadEndTime = System.currentTimeMillis();

			LogInfo(s9Space + "My Account page loaded successfully in: " + ((pageLoadEndTime-pageLoadStartTime)/1000) + " seconds");
			LogInfo("--> Entering the Valid Login Credentials");
			
			returnLocator("USERNAME").click();
			returnLocator("USERNAME").clear();
			returnLocator("USERNAME").sendKeys("automation1@onestop.com");
			waitForLocator("PASSWORD");
										
			returnLocator("PASSWORD").click();
			returnLocator("PASSWORD").clear();
			returnLocator("PASSWORD").sendKeys("onestop1");
			
			LogInfo("--> Clicking on Log-In button");
			
			pageLoadStartTime = System.currentTimeMillis();
			waitForLocator("LOG_IN");
			returnLocator("LOG_IN").click();
			LogInfo(s9Space + "Current URL: " + driver.getCurrentUrl());
			waitForLocator("SHOPPING_CART");
			pageLoadEndTime = System.currentTimeMillis();
			
			if(isElementPresent("SHOPPING_CART") == true) {
				LogInfo(s9Space+"Signed in to my account successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
			} 
			
			else {
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
				Assert.fail();
			}					
		}// try-end
    
    	catch(Exception e){
    		pageloadErrorOrException=e.getMessage();
			LogInfo("***** EXCEPTION IN MyAccountSignIn METHOD --> " + pageloadErrorOrException);
			failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
			Assert.fail();
		} //catch end
		
		LogInfo("-------------------------");
} //End "Login method"
    
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ACCOUNT VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void AccountVerification() throws Exception {    	
		testcasename = "AccountVerification";
		String bodyText ;
		
		try{
			LogInfo("-------------- ACCOUNT VERIFICATION  -------------");
			
			bodyText = returnLocator("SIGNED_USER").getText();
			bodyText = bodyText.toUpperCase();
			 
			if(bodyText.contains("AUTOMATION")) {
				LogInfo("--> User account verified and displayed as xxxx: " + bodyText);
			}				
				
			else{
				LogInfo("--> User account is displayed as : " + bodyText);
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
				Assert.fail();
			}	
		}//Try
		
		catch(Exception e){
			pageloadErrorOrException=e.getMessage();
			LogInfo("***** GOT EXCEPTION IN AccountVerification METHOD --> "+pageloadErrorOrException);
			failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
			Assert.fail();
		}
		LogInfo("--------------------");
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SHOPPING CART VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void EmptyShoppingCart() throws Exception {
    	testcasename = "EmptyShoppingCart";
	    	
    	try{
    		LogInfo("----------- SHOPPING CART VERIFICATION ------------");
    		
			switch (sitename) {		
				case "DAVI": 
					waitForLocator("SHOPPING_CART");
			    	returnLocator("SHOPPING_CART").click();
			    break;
		 
				case "JOES":
					LogInfo("--> Hovering over the Shopping Cart button");
					pageLoadStartTime = System.currentTimeMillis();
				    Actions builder = new Actions(driver);
				    				    
				    builder.moveToElement(returnLocator("SHOPPING_CART")).build().perform();
					
					if(isElementPresent("SHOPPING_CART_CHECKOUT") == true){
						returnLocator("SHOPPING_CART_CHECKOUT").click();

						pageLoadEndTime = System.currentTimeMillis();
			    	}
					
			    	else {
			    		LogInfo("--> Shopping Cart is empty");
				    }
				 break;
				 
				 case "AQUA": 
					 LogInfo("--> Clicking on the Shopping Cart button");
					 pageLoadStartTime = System.currentTimeMillis();
					 returnLocator("SHOPPING_CART").click();
					 LogInfo("--> Removing Items from the Shopping Cart");		
				         		
				    if (isElementPresent("EMPTY_CART") == true) {
						LogInfo("--> Shopping Cart has been emptied");
					}
				    	
				    else{
				    	LogInfo("--> Unable to empty Shopping Cart");
				    	failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
				    }
				 break;
			}//switch
		}//try
    	
    	catch(Exception e){
			pageloadErrorOrException=e.getMessage();
			LogInfo("***** EXCEPTION IN EmptyShoppingCart METHOD --> " + pageloadErrorOrException);
			failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
			Assert.fail();
		}
    	
    	LogInfo("----------------------");
    }
    
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SEARCH VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void SearchForAProduct() throws Exception {
    	testcasename = "SearchForAProduct";
    	pageLoadStartTime = System.currentTimeMillis();
    	String objectArray[]=null;
    	String searchCountIdentifierValue;
    	LogInfo("---------------------- SEARCH VERIFICATION ----------------------");
    	
    	try{
    		if("JOES".equals(sitename) || "DAVI".equals(sitename)) {
    			returnLocator("PLP1").click();
				
				if(isElementPresent("FANCYBOX") == true){
					LogInfo("--> Closing the Fancy Box Pop-Up");
					returnLocator("FANCYBOX").click();
					waitForLocator("SEARCH_BOX");
				}
    		}
    		
			LogInfo("--> Searching for a Product");
			
			String countSearch;
			countSearch = returnLocator("SEARCH_COUNT").getText(); 
			LogInfo("--> Number of Search Results displayed on the page: "+ countSearch);
			
			waitForLocator("SEARCH_COUNT");
			objectArray = LocatorProps.getProperty("SEARCH_COUNT").split("__");
			searchCountIdentifierValue=objectArray[1].trim();
			
			List<WebElement> Count = driver.findElements(By.xpath(searchCountIdentifierValue));
				
			if (Count.size()>0){
				LogInfo("--> Number of Search Results displayed on the page: "+ Count.size());	
			} 
			
			else {
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
					Assert.fail();
			}				
			
			if(isElementPresent("FANCYBOX") == true){
				LogInfo("--> Closing the Fancy Box Pop-Up");
				returnLocator("FANCYBOX").click();
			}
		} //try
    	
		catch(Exception e){
			pageloadErrorOrException = e.getMessage();
			LogInfo("***** GOT EXCEPTION IN SearchForAProduct METHOD --> " + pageloadErrorOrException);
			failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
			Assert.fail();
		}//Catch
    	
    	LogInfo("----------------------");
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PLP VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void GoToPLP() throws Exception {
    	testcasename = "GoToPLP";

    	try{   
    		LogInfo("---------------------- PLP VERIFICATION ----------------------");
    		
			LogInfo("--> Navigating to the PLP");
			pageLoadStartTime = System.currentTimeMillis();
	    	
			switch(sitename) {
				case "JOES":
		    		LogInfo("--> Refreshing Browser");
			        driver.navigate().refresh();
			        
					returnLocator("PLP1").click();;
	            	waitForLocator("PLP2");
	            	returnLocator("PLP2").click();;
	            	waitForLocator("PDP");
	            	pageLoadEndTime = System.currentTimeMillis();
	            break;
	            
				case "AQUA":
					waitForLocator("PLP1");
					returnLocator("PLP1").click();
		            waitForLocator("PDP");
		            pageLoadEndTime = System.currentTimeMillis();
				break;
			}//switch
    	}//try
    	
    	catch(Exception e){
			pageloadErrorOrException=e.getMessage();
			LogInfo("***** GOT EXCEPTION IN GoToPLP METHOD --> "+pageloadErrorOrException);
			failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
			Assert.fail();
		}
    	
    	LogInfo("----------------------");
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PDP VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void GoToPDP() throws Exception {
    	testcasename = "GoToPDP";
    	driver.navigate().refresh();
    	
    	pageLoadStartTime = System.currentTimeMillis();
    	LogInfo("---------- PDP VERIFICATION ------------");
		
    	try{
	    	LogInfo("--> Navigating to the PDP");
			
			if("DAVI".equals(sitename)) { 			
				driver.get("http://us.davines.com/awardwinning-minu-shampoo/d/1146C654?CategoryId=8");
				waitForLocator("ADD_TO_CART");
			}
		
			else {
				waitForLocator("PDP");
				returnLocator("PDP").click();
				LogInfo(s9Space+"Current URL: " + driver.getCurrentUrl());
				waitForLocator("ADD_TO_CART");
				pageLoadEndTime = System.currentTimeMillis();			
			}
		
			if(isElementPresent("SHOPPING_CART") == true) {
				LogInfo(s9Space+"PDP loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
			} 
			
			else {
	    		failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
	    		Assert.fail();
	    	}			
		}//try
		
		catch(Exception e){
			pageloadErrorOrException=e.getMessage();
			LogInfo("***** GOT EXCEPTION IN GoToPDP METHOD --> "+pageloadErrorOrException);
			failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
			Assert.fail();
		}
    	LogInfo("----------------------");
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SIZE VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void SelectProductSize() throws Exception {
    	testcasename = "SelectProductSize";
    	LogInfo("------------------ SIZE VERIFICATION ------------------");
    	
		try{
			switch (sitename) {
				case "JOES":						
					if(isElementPresent("FIT_SIZE"))
					{
						LogInfo("--> Selecting Size of the Product");
			 			waitForLocator("SIZE_A");
			 			returnLocator("SIZE_A").click();
			 			waitForLocator("SIZE_VALUE_A");	
			 			returnLocator("SIZE_VALUE_A").click();
					}
					
					else{
						LogInfo("--> Selecting Size of the Product");
			 			waitForLocator("SIZE_B");
			 			returnLocator("SIZE_B").click();
			 			waitForLocator("SIZE_VALUE_B");	
			 			returnLocator("SIZE_VALUE_B").click();
					}
					
				 		
				  if (isElementPresent("COLOR")) { 
					  	LogInfo("--> Selecting Color");
				 		returnLocator("COLOR").click();				 	 					 	 		
				     }			 		
				 		
				 else{
				   	    LogInfo("--> No color is available for selection");
				 }
				 	
				  	LogInfo("Current URL: " + driver.getCurrentUrl());
			 	  break;
				 
			 case "DAVI":
		     case "AQUA":
		     	 if (isElementPresent("SIZE") == true){
					LogInfo("--> Selecting Size of the Product");
					returnLocator("SIZE").click();
					waitForLocator("SIZE_VALUE");
					returnLocator("SIZE_VALUE").click();
					
					LogInfo("Current URL: " + driver.getCurrentUrl());
				}
		     	 
				 else {
					LogInfo("--> No Size is available for selection");
				}
				
		      break;	 
			}//switch		
		}//try
		
		catch(Exception e){
			pageloadErrorOrException=e.getMessage();
			LogInfo("***** EXCEPTION IN SelectProductSize METHOD --> "+ pageloadErrorOrException);
			failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
			Assert.fail();
		}
		
		LogInfo("----------------------");
    }    
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ADD TO CART / CHECKOUT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void AddToCartAndCheckout() throws Exception {
    	testcasename = "AddToCartAndCheckout";

    	try{
    		LogInfo("--> Clicking on the Add To Cart button");
			returnLocator("ADD_TO_CART").click();
			waitForLocator("SHOPPING_CART");
				    
    		pageLoadStartTime = System.currentTimeMillis();
			LogInfo("--> Performing checkout");
								
				if(("DAVI".equals(sitename))){
					returnLocator("CHECKOUT").click();
					waitForLocator("FINAL_CHECKOUT");
					returnLocator("FINAL_CHECKOUT").click();
					
					if(isElementPresent("FINAL_CHECKOUT") == true){
			    		LogInfo("--> Clicking on the continue to checkout button");
						returnLocator("FINAL_CHECKOUT").click();	
						waitForLocator("SPCSHIPPING_INFO_EDIT");
			    	}
				}
				
				else{				  
					Actions builder = new Actions(driver);
			        builder.moveToElement(returnLocator("SHOPPING_CART")).click().build().perform();	
			        waitForLocator("SHOPPING_CART");
				}    
		}//try
    	
    	catch(Exception e){
			pageloadErrorOrException=e.getMessage();
			LogInfo("***** EXCEPTION IN AddToCartAndCheckout METHOD --> "+pageloadErrorOrException);
			failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
			Assert.fail();
		}//catch
    } 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ENTER SHIPPING INFO ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void EnterShippingInfo() throws Exception {
    	testcasename = "EnterShippingInfo";
    	String errorArray[]=null;
    	String strActualState =null;
    	Actions builder = new Actions(driver);
    	
    	LogInfo("--> Scrolling up on the page");
		((JavascriptExecutor) driver).executeScript("scroll(500,0);");
		Thread.sleep(5000);
    	
		try{
			if ("JOES".equals(sitename)){ 
				Thread.sleep(5000);
				
				if (isElementPresent("LOG_IN2")){
				LogInfo("--> Continue as Guest");
							returnLocator("GUESTBUTTON").click();							
						}
					}
				
				if( isElementPresent("SPCSHIPPING_FIRST_NAME") ){
					LogInfo("--> Entering the Shipping Info details");
				}	
				
				else {
					LogInfo("--> Clicking on Shipping Info button");
					waitForLocator("SPCSHIPPING_INFO_EDIT");
					returnLocator("SPCSHIPPING_INFO_EDIT").click();
					waitForLocator("SPCSHIPPING_FIRST_NAME");
				}				
				
				LogInfo("--> Entering the First Name");
				returnLocator("SPCSHIPPING_FIRST_NAME").click();
				returnLocator("SPCSHIPPING_FIRST_NAME").clear();
				returnLocator("SPCSHIPPING_FIRST_NAME").sendKeys("Onestop");
				
				LogInfo("--> Entering the Last Name");
				waitForLocator("SPCSHIPPING_LAST_NAME");
				returnLocator("SPCSHIPPING_LAST_NAME").click();
				returnLocator("SPCSHIPPING_LAST_NAME").clear();
				returnLocator("SPCSHIPPING_LAST_NAME").sendKeys("QA");
				
				LogInfo("--> Entering Address");
				waitForLocator("SPCSHIPPING_ADDRESS");
				returnLocator("SPCSHIPPING_ADDRESS").click();
				returnLocator("SPCSHIPPING_ADDRESS").clear();
				
				if( sitename.equals("DLAM")){ 
					returnLocator("SPCSHIPPING_ADDRESS").sendKeys("W 77th St"); }
				
				else{
					returnLocator("SPCSHIPPING_ADDRESS").sendKeys("550 CONTINENTAL BLVD.");
				}
								
				LogInfo("--> Entering City Name");
				
				waitForLocator("SPCSHIPPING_CITY");
				returnLocator("SPCSHIPPING_CITY").click();
				returnLocator("SPCSHIPPING_CITY").clear();
				
				if(sitename.equals("DLAM")){ 
					returnLocator("SPCSHIPPING_CITY").sendKeys("New York"); 
				}
				
				else{ 
					returnLocator("SPCSHIPPING_CITY").sendKeys("EL SEGUNDO"); }
									
				if(!"DAVI".equals(sitename) )
				{
					strActualState = new String(returnLocator("SPCSHIPPING_STATE").getText());
				    //Validate if a state is NOT selected, using the text from state field
					if(strActualState.indexOf( '�' ) > -1 ){
						LogInfo("--> Selecting State Name");
						((JavascriptExecutor) driver).executeScript("scroll(0,400);");
						waitForLocator("SPCSHIPPING_STATE");
						builder.moveToElement(returnLocator("SPCSHIPPING_STATE")).click().build().perform();
						returnLocator("SPCSHIPPING_STATE_SELECT").click();
					}
					
					LogInfo("STATE:"+strActualState);
					LogInfo("--> State already selected");	
				 }
			
				LogInfo("--> Scrolling down on the page");
				((JavascriptExecutor) driver).executeScript("scroll(0,250);");
				Thread.sleep(5000);
				
				LogInfo("--> Entering the Zip Code");
				waitForLocator("SPCSHIPPING_ZIPCODE");
				returnLocator("SPCSHIPPING_ZIPCODE").click();
				returnLocator("SPCSHIPPING_ZIPCODE").clear();
				
				if( sitename.equals("DLAM")){
					returnLocator("SPCSHIPPING_ZIPCODE").sendKeys("10024-5153"); }
				
				else
				{
					returnLocator("SPCSHIPPING_ZIPCODE").sendKeys("90245-5049"); 
				}
								
					LogInfo("--> Entering the Email Address");
					waitForLocator("SPCSHIPPING_EMAIL");
					returnLocator("SPCSHIPPING_EMAIL").click();
					returnLocator("SPCSHIPPING_EMAIL").clear();
					returnLocator("SPCSHIPPING_EMAIL").sendKeys("automation1@onestop.com");
					
					LogInfo("--> Entering the Phone Number");
					waitForLocator("SPCSHIPPING_PHONE");
					returnLocator("SPCSHIPPING_PHONE").click();
					returnLocator("SPCSHIPPING_PHONE").clear();
					returnLocator("SPCSHIPPING_PHONE").sendKeys("3108947724");
					
					if("DAVI".equals(sitename)){}
					else{
					
						if(isElementPresent("SPCBILLING_FIRST_NAME") == true){
							LogInfo("--> Selecting the checkbox to enter the same Shipping Info");
							((JavascriptExecutor) driver).executeScript("scroll(250,0);");
							Thread.sleep(5000);
							returnLocator("SPCBILLING_SAME_AS_SHIPPING").click();
							Thread.sleep(20000);
						}
					}

		} //try
		
		catch(Exception e){
			pageloadErrorOrException=e.getMessage();
			LogInfo("***** GOT EXCEPTION IN EnterShippingInfo METHOD --> "+pageloadErrorOrException);
			failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
			Assert.fail();
		}
    }  
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ENTER PAYMENT INFO ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void SelectShippingChoice() throws Exception {
    	testcasename = "SelectShippingChoice";
    	
		
    	try{
    		
			switch (sitename) {	
			      case "TRTL":
			      case "TRUK":
			      case "TRDE":
			      case "TRIT":
			      case "TREU":
			      case "TRES":
			      case "TRNL":
			      case "TRFR":
			
					   waitForLocator("SPCSHIPPING_INFO_CONTINUE");						
			    	   returnLocator("SPCSHIPPING_INFO_CONTINUE").click();
			    	   waitForLocator("FREE_SHIPPING");
			    	   returnLocator("FREE_SHIPPING").click();
			    	   waitForLocator("SPCSHIPPINGCHOICECONTINUE");
			    	   returnLocator("SPCSHIPPINGCHOICECONTINUE").click();
			    	   
			    	   //waitForLocator("SALES_TAX");
			    	   break;
		    	   
				  case "TRYN":
					   Thread.sleep(20000);	
					   if(! isElementPresent("SPCSHIPPING_CHOICE_CONTINUE"))
					     { 
						   System.out.println("--> Refreshing Page");
				    	   APP_LOGS.debug("--> Refreshing Page");
						   driver.navigate().refresh();
						   Thread.sleep(20000);	
						   ((JavascriptExecutor) driver).executeScript("scroll(0,650);");
						  }
					   
					   waitForLocator("SPCSHIPPING_CHOICE_CONTINUE");						
			    	   returnLocator("SPCSHIPPING_CHOICE_CONTINUE").click();
			    	   waitForLocator("SALES_TAX");
			    	   break;
			    	   
			       case "FRYE":			    	  
			    	   System.out.println("--> Scrolling down on the page");
					   APP_LOGS.debug("--> Scrolling down on the page");
					   ((JavascriptExecutor) driver).executeScript("scroll(0,1250);");	    	 
//			    	   System.out.println("--> Refreshing Page");
//			    	   APP_LOGS.debug("--> Refreshing Page");
//					   driver.navigate().refresh();
//					   Thread.sleep(20000);						   	
//					   ((JavascriptExecutor) driver).executeScript("scroll(0,1900);");
//					   
					   		   
					   if ( isElementPresent("SPCSHIPPING_CHOICE") )
					   {		
						   	 moveToElement("SPCSHIPPING_CHOICE_B");
							 returnLocator("SPCSHIPPING_CHOICE_B").click();	
					   }				  	
					   else if(isElementPresent("SPCSHIPPING_CHOICE_B"))
					   {
						   	 moveToElement("SPCSHIPPING_CHOICE");
							 returnLocator("SPCSHIPPING_CHOICE").click();	
					   }
					   else if(isElementPresent("SPCSHIPPING_CHOICE_C"))
					   {
						   	 moveToElement("SPCSHIPPING_CHOICE_C");
							 returnLocator("SPCSHIPPING_CHOICE_C").click();	
					   }
					 
					   while ( ! isElementPresent("SPCSHIPPING_CHOICE_CONTINUE") )
				       { Thread.sleep(50000); }
					   
					   waitForLocator("SPCSHIPPING_CHOICE_CONTINUE");						
			    	   returnLocator("SPCSHIPPING_CHOICE_CONTINUE").click();
			    	   waitForLocator("SALES_TAX");
			    	   break;
			      
			       default:
			    	   		System.out.println("--> Clicking on Continue button to select Shipping Choice");
			    	   		APP_LOGS.debug("--> Clicking on Continue button to select Shipping Choice");
			    	   		returnLocator("SPCSHIPPING_INFO_CONTINUE").sendKeys(Keys.ENTER);
			    	   		Thread.sleep(10000);
					
				
			    	   		if("EAGL".equals(sitename)){
			    	   			if(isElementPresent("NO_THANKS") == true){
			    	   				System.out.println("--> Closing the Fancy Box Pop-Up");
			    	   				APP_LOGS.debug("--> Closing the Fancy Box Pop-Up");
			    	   				returnLocator("NO_THANKS").click();
			    	   				waitForLocator("PDP");
					              }   
			    	   		}
					
			    	   		if(isElementPresent("ADDRESS_VERIFICATION") == true){
			    	   			System.out.println("--> Selecting Address on the Verification Pop-Up");
			    	   			APP_LOGS.debug("--> Selecting the address on the Verification Pop-Up");
			    	   			returnLocator("ADDRESS_VERIFICATION").click();
			    	   			((JavascriptExecutor) driver).executeScript("scroll(250,0);");
			    	   		
			    	   			waitForLocator("SELECT_ADDRESS");
			    	   			returnLocator("SELECT_ADDRESS").click();
			    	   			waitForLocator("SPCSHIPPINGCHOICECONTINUE");
			    	   			Thread.sleep(20000);
			    	   			((JavascriptExecutor) driver).executeScript("scroll(250,0);");
			    	   			
			    	   		}
				
			    	   		
			    	   		if( "LYCA".equals(sitename) ){
			    	   		    		
			    	   		   if( isElementPresent("BILLING_ADDRESS_NAME") ){
			    	   		       returnLocator("USE_SHIPPING_ADDRESS").click(); 
			    	   		       Thread.sleep(5000);
			    	   		      }
			    	   		}
			    	   		
			    	   		
			    	   		if(isElementPresent("SPCSHIPPINGCHOICECONTINUE") == true){
			    	   			System.out.println("--> Clicking on Continue button to enter Payment Info");
			    	   			APP_LOGS.debug("--> Clicking on Continue button to enter Payment Info");
			    	   			returnLocator("SPCSHIPPINGCHOICECONTINUE").sendKeys(Keys.ENTER);
				              }
			    	   		
			    	   		if(isElementPresent("ADDRESS_VERIFICATION") == true){
			    	   			System.out.println("--> Selecting Address on the Verification Pop-Up");
			    	   			APP_LOGS.debug("--> Selecting the address on the Verification Pop-Up");
			    	   			returnLocator("ADDRESS_VERIFICATION").click();
			    	   			waitForLocator("SELECT_ADDRESS");
			    	   			returnLocator("SELECT_ADDRESS").click();
			    	   			waitForLocator("SPCSHIPPINGCHOICECONTINUE");
			    	   			Thread.sleep(20000);
			    	   			((JavascriptExecutor) driver).executeScript("scroll(250,0);");
			    	   		}
			    	   				
			    	   		if( "LYCA".equals(sitename) ){
	    	   		    		
				    	   		   if( isElementPresent("BILLING_ADDRESS_NAME") ){
				    	   		       returnLocator("USE_SHIPPING_ADDRESS").click(); 
				    	   		       Thread.sleep(5000);
				    	   		      }
				    	   		}
			    	   		
			    	   		waitForLocator("SALES_TAX");
			    	   		break; //default
				
			 
			}//switch
			
		}catch(Exception e){
			pageloadErrorOrException=e.getMessage();
			System.out.println("***** GOT EXCEPTION IN SelectShippingChoice METHOD --> "+pageloadErrorOrException);
			APP_LOGS.debug("***** GOT EXCEPTION IN SelectShippingChoice METHOD --> "+pageloadErrorOrException);
			failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
			Assert.fail();
		}	
		}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ VERIFY SALES TAX ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void VerifySalesTaxIsNotZero_SPC() throws Exception{
		testcasename ="VerifySalesTaxIsNotZero_SPC";
		
		try{
			System.out.println("--> Verifying Sales Tax on SPC");
			APP_LOGS.debug("--> Verifying Sales Tax on SPC");		
			Thread.sleep(15000);
			
			if( "LYCA".equals(sitename) ){
				((JavascriptExecutor) driver).executeScript("scroll(0,10);");
			}
			
			String salesTax=returnLocator("SALES_TAX").getText().replace("\n", "").replace("Sales Tax:", "").trim();
			
			if(("DAVI".equals(sitename)) ){}
			else{
			  if(salesTax.equals("$0.00") || salesTax.equals("[TBD]")){
				System.out.println("-> SPC Sales tax value is displayed as: "+salesTax);
				APP_LOGS.debug("-> SPC Sales tax value is displayed as: "+salesTax);	
				failure(testcasename, "RESULT: FAIL - SPC Sales tax value is $0.00 OR [TBD]","","","","");
				Assert.fail();
			}else{
				System.out.println(s9Space+"SPC Sales tax value is displayed as: "+salesTax);
				APP_LOGS.debug(s9Space+"SPC Sales tax value is displayed as: "+salesTax);				
			}
			}
		}catch(Exception e){
			pageloadErrorOrException=e.getMessage();
			System.out.println("***** GOT EXCEPTION IN VerifySalesTaxIsNotZero_SPC METHOD --> "+pageloadErrorOrException);
			APP_LOGS.debug("***** GOT EXCEPTION IN VerifySalesTaxIsNotZero_SPC METHOD --> "+pageloadErrorOrException);
			failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
			Assert.fail();
		}	
		}
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ BORDER FREE VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void BorderFree() throws Exception {
    	testcasename = "BorderFree";
    	String strColorName = null;
    			
		try{		
					HomePageLoad();
					
					if( ! isElementPresent("SHIPTO") )
					  { 
						driver.navigate().refresh();
						Thread.sleep(8000);
						System.out.println("Closing Fancybox!");
						APP_LOGS.debug("Closing Fancybox!");
/*						driver.getKeyboard().sendKeys(Keys.ESCAPE);*/
					  }
			  	 	
					waitForLocator("SHIPTO");	
				 	System.out.println("--> Selecting Ship to ");
					APP_LOGS.debug("--> Selecting Ship To");
					returnLocator("SHIPTO").click();
					waitForLocator("SHIPTO_COUNTRY");
					returnLocator("SHIPTO_COUNTRY").click();		
					returnLocator("SHIPTO_COUNTRY_VALUE").click();
					returnLocator("SHIPTO_COUNTRY").click();
					waitForLocator("SHIPTO_CONTINUE");
					returnLocator("SHIPTO_CONTINUE").click();
					
					if(  isElementPresent("FANCY_INT") )
					  {
						 System.out.println("--> Closing the Fancy International");
						 APP_LOGS.debug("--> Closing the Fancy International"); 
						 returnLocator("FANCY_INT").click();
						 Thread.sleep(4000);
					  }
					
					//Go to Int PDP
					driver.get("https://www.thefryecompany.com/addie-hiker-d-76614");
					System.out.println("Closing Fancybox!");
					APP_LOGS.debug("Closing Fancybox!");

					 while (! isElementPresent("COLOR_NAME2") )
					 {
						 System.out.println("--> Refreshing Browser");
				 		 APP_LOGS.debug("--> Refreshing Browser");
						 driver.navigate().refresh();
						 Thread.sleep(8000);
						 System.out.println("Closing Fancybox!");
						 APP_LOGS.debug("Closing Fancybox!");

						 
					 }					 
						
					 strColorName = new String(returnLocator("COLOR_NAME2").getText());
					 
					 if ( strColorName.equals("") )
						{
						 	System.out.println("--> Selecting Color of the Product");
				 			APP_LOGS.debug("--> Selecting Size of the Product");
				 			returnLocator("COLOR_WAIT").click();
				 			waitForLocator("ADD_TO_CART");	
					    }
					 else {
				 			System.out.println("--> Color selected");
				 			APP_LOGS.debug("--> Color selected");
				 		}
					
					waitForLocator("INT_SIZE");					
					if ( !isElementPresent("INT_SIZE")  )
					{ System.out.println("--> Closing fancybox");
		 			  APP_LOGS.debug("--> Closing fancybox");
/*					  driver.getKeyboard().sendKeys(Keys.ESCAPE); */
					} 					
					returnLocator("INT_SIZE").click();
					((JavascriptExecutor) driver).executeScript("scroll(0,250);");
					
					
					
					/*							
					WebElement weRecomendations = returnLocator("RECOMMENDATION");
					List<WebElement> liCounter = weRecomendations.findElements(By.xpath("//img"));
				    			    
				    System.out.println("List size is: " + liCounter.size() );
				    
				    if (liCounter.size() > 0 )
				    {
				    	System.out.println( "Under the PDP " + liCounter.size() + "are displayed" );
						APP_LOGS.debug( "Under the PDP " + liCounter.size() + "are displayed" );
				    }
				    else
				    {
				    	System.out.println("-> Under the PDP there is not any recommendation" );
						APP_LOGS.debug("-> Under the PDP there is not any recommendation" );	
						failure(testcasename, "-> Under the PDP there is not any recommendation","","","","");
						Assert.fail();
				    }
				    */
					
					//Validate if recommendation are displayed
					if( isElementPresent("RECOMMENDATION") ) {										
						System.out.println("--> Under the PDP recommendations are displayed " );
						APP_LOGS.debug("-->Under the PDP recommendations are displayed");
					}
					else{						
						System.out.println("-->Under the PDP recommendations are not displayed");
						APP_LOGS.debug("-->Under the PDP recommendations are not displayedy" );	
						failure(testcasename, "-->Under the PDP recommendations are not displayed","","","","");
						Assert.fail();
					    }
					
					waitForLocator("ADD_TO_CART");
					returnLocator("ADD_TO_CART").click();
					waitForLocator("SUCCESS_ADD");
					driver.get("https://www.thefryecompany.com/checkout/cart/");
					
					if ( !isElementPresent("INT_CHECKOUT")  )
					{ System.out.println("--> Closing fancybox");
		 			  APP_LOGS.debug("--> Closing fancybox");
/*					  driver.getKeyboard().sendKeys(Keys.ESCAPE); */
					} 
					
					waitForLocator("INT_CHECKOUT");
					returnLocator("INT_CHECKOUT").click();
									
					String strBorderFree = driver.getCurrentUrl();
					
					if( strBorderFree.equals( "https://www.thefryecompany.com/checkout/cart/" )){
						System.out.println("Borderfree loaded correctly " );
						APP_LOGS.debug("Borderfree loaded correctly ");
					}
					else{						
						System.out.println("Borderfree not loaded");
						APP_LOGS.debug("Borderfree not loaded" );	
						failure(testcasename, "-> Borderfree not loaded" + strBorderFree,"","","","");
						Assert.fail();
					    }
				
			
				
			
		}catch(Exception e){
			pageloadErrorOrException=e.getMessage();
			System.out.println("***** GOT EXCEPTION IN BorderFree METHOD --> "+pageloadErrorOrException);
			APP_LOGS.debug("***** GOT EXCEPTION IN BorderFree METHOD --> "+pageloadErrorOrException);
			failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
			Assert.fail();
		}	
		}
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Gift Card Balance  VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void CheckGiftCardBalance() throws Exception {
    	testcasename = "CheckGiftCardBalance";
		
		try{
					HomePageLoad();
			  	 	waitForLocator("GIFT_CARD");	
				 	System.out.println("--> Selecting Gift Card link ");
					APP_LOGS.debug("--> Selecting Gift Card link ");
					returnLocator("GIFT_CARD").sendKeys(Keys.ENTER);										
					waitForLocator("GIFT_CARD_CODE");
					returnLocator("GIFT_CARD_CODE").sendKeys("123");
					waitForLocator("GIFT_CARD_PIN");
					returnLocator("GIFT_CARD_PIN").sendKeys("123");
					waitForLocator("GIFT_BALANCE");				
					returnLocator("GIFT_BALANCE").sendKeys(Keys.ENTER);									
			
		}catch(Exception e){
			pageloadErrorOrException=e.getMessage();
			System.out.println("***** GOT EXCEPTION IN CheckGiftCardBalance METHOD --> "+pageloadErrorOrException);
			APP_LOGS.debug("***** GOT EXCEPTION IN CheckGiftCardBalance METHOD --> "+pageloadErrorOrException);
			failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
			Assert.fail();
		}	
		}
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~E - Gift Card  VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void CheckEGiftCard() throws Exception {
    	testcasename = "CheckEGiftCard";
		try{
					HomePageLoad();
					((JavascriptExecutor) driver).executeScript("scroll(0,2000);");
					Thread.sleep(5000);
			  	 	waitForLocator("GIFT_CARD");	
				 	System.out.println("--> Selecting Gift Card link ");
					APP_LOGS.debug("--> Selecting Gift Card link ");
					returnLocator("GIFT_CARD").sendKeys(Keys.ENTER);												
					waitForLocator("PURCHASE_EGIFTC");
					returnLocator("PURCHASE_EGIFTC").sendKeys(Keys.ENTER);
							
					while (!isElementPresent("ECHOOSE_AMOUNT")  )
						  {
							System.out.println("--> Refreshing Page");
							APP_LOGS.debug("--> Refreshing Page");
							driver.navigate().refresh();
							Thread.sleep(5000);
						  }
					 
					returnLocator("ECHOOSE_AMOUNT").sendKeys(Keys.ENTER);
					 					 					 
					//Validate if E - Gift Card Page is displayed.
					if( isElementPresent("EGIFTC_ADDTOCARD") ) {										
						System.out.println("-->E - Gift Card Page Displayed Correctly " );
						APP_LOGS.debug("-->E - Gift Card Page Displayed Correctly");
					}
					else{						
						System.out.println("-->E - Gift Card Page not displayed Correctly");
						APP_LOGS.debug("-->E - Gift Card Page not displayed Correctly" );	
						failure(testcasename, "-->E - Gift Card Page not displayed Correctly","","","","");
						Assert.fail();
					    }
				
					
			
		}catch(Exception e){
			pageloadErrorOrException=e.getMessage();
			System.out.println("***** GOT EXCEPTION IN CheckEGiftCard METHOD --> "+pageloadErrorOrException);
			APP_LOGS.debug("***** GOT EXCEPTION IN CheckEGiftCard METHOD --> "+pageloadErrorOrException);
			failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
			Assert.fail();
		}	
		}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ TAG VERIFICATION METHOD ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

		public static void TagVerification() throws Exception {
	    	testcasename = "TagVerification";
    		boolean uTagAvailable = false;
    		boolean convertoTagAvailable = false;
	    	try{
				if(("BKRM".equals(sitename)))
				{
					return;
				}
	    		Thread.sleep(5000);
	    		System.out.println("--> Getting Page Source Code");
	    		APP_LOGS.debug("--> Getting Page Source Code");
	    		pageSource = driver.getPageSource();
	    		Thread.sleep(5000);
	    		System.out.println("--> Verifying UTag: "+UT+" is present in Page Source Code");
	    		APP_LOGS.debug("--> Verifying UTag: "+UT+" is present in Page Source Code");
	    		uTagAvailable = pageSource.contains(UT);
	    		
	    		if(uTagAvailable == true)
	    		{
	    			System.out.println("--> UTag: "+UT+" is found in Page Source Code");
	    			APP_LOGS.debug("--> UTag: "+UT+" is found in Page Source Code");
	    			Thread.sleep(7000);
	    		}
	    		else
	    		{
	    			System.out.println("--> UTag: "+UT+" is NOT found on the Page.");
	    			APP_LOGS.debug("--> UTag: "+UT+" is NOT found on the Page.");
	    			failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
	    			Assert.fail();
	    		}
	    		
	    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONVERTO VERIFICATION ~~~~~~~~~~~~~~~~~~~	
	    		switch (sitename) {
				 case "DAVI":
				 case "DLAM":
				 case "FRYE":
				 case "JCTR":
				 case "PEBA":
				 case "POLK":
				 case "SEVN":
				 case "SHEX":
				 case "SPDR":
				 case "TRIH":
					 LogInfo("--> Converto Tag: "+CO+" doesn't exist for this client-Tested on 28/01/16");
					 break;

				 case "AQUA":
				 case "CAML":
				 case "CBTL":
				 case "EAGL":
				 case "HUDS":
				 case "NYDJ":
				 case "PAIG":
				 case "SPLD":
						LogInfo("--> Verifying Converto Tag: "+CO+" is present in Page Source Code");
						
			    		convertoTagAvailable = pageSource.contains(CO);
			    		
			    		if(convertoTagAvailable == true){
			    			LogInfo("--> Converto Tag: "+CO+" is found in Page Source Code");
			    		}
			    		
			    		else
			    		{
			    			System.out.println("--> Converto Tag: "+CO+" is NOT found in Page Source Code");
			    			APP_LOGS.debug("--> Converto Tag: "+CO+" is NOT found in Page Source Code");
			    			//failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
			    			//Assert.fail();
			    		}
			    		break;
	    		}
	    		
	    	}catch(Exception e){
				pageloadErrorOrException=e.getMessage();
				System.out.println("***** GOT EXCEPTION IN TagVerification METHOD --> "+pageloadErrorOrException);
				APP_LOGS.debug("***** GOT EXCEPTION IN TagVerification METHOD --> "+pageloadErrorOrException);
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
				Assert.fail();
			}
	    }
		
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Verify Track Order ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	    public static void VerifyTrackOrder() throws Exception{
			
	    	testcasename ="VerifyTrackOrder";
	    	String strOrderInformation;	
	    	String strOrderId = "058256908";    //"058217792";
	    	String strLastName = "Muthukumarasamy";
	    	String strMail = "padma.m@onestop.com";
	    	String strPassword = "Onestop123#";
	    		    	
			try{
							
				//Search Order
				System.out.println("--> Verify Track Order");
				APP_LOGS.debug("--> Verify Track Order");
				Thread.sleep(20000);
				
				if(isElementPresent("FANCYBOX") == true){
			    	 System.out.println("--> Closing the Fancy Box Pop-Up");
			    	 APP_LOGS.debug("--> Closing the Fancy Box Pop-Up"); 
			    	 returnLocator("FANCYBOX").click();
				}	
				
				waitForLocator("TRACK_ORDER");
/*				driver.getKeyboard().sendKeys(Keys.ESCAPE);*/
				returnLocator("TRACK_ORDER").sendKeys(Keys.ENTER);				
				Thread.sleep(10000);
				waitForLocator("ORDER_ID");
				returnLocator("ORDER_ID").sendKeys( strOrderId );
				returnLocator("ORDER_LAST_NAME").sendKeys( strLastName );
				returnLocator("ORDER_EMAIL").sendKeys( strMail );
				((JavascriptExecutor) driver).executeScript("scroll(0,250);");		
/*				driver.getKeyboard().sendKeys(Keys.ESCAPE);*/
				Thread.sleep(3000);	
				returnLocator("ORDER_CONTINUE").click();
				Thread.sleep(15000);			
				
				/*
				waitForLocator("USERNAME");
				returnLocator("USERNAME").sendKeys( strMail );
				returnLocator("PASSWORD").sendKeys( strPassword );
				returnLocator("LOG_IN").click();	
				Thread.sleep(15000);
				*/
				
				//Validate if the Order was placed.
				if( isElementPresent("ORDER_INFORMATION") ) {
					strOrderInformation = returnLocator("ORDER_INFORMATION").getText();
					
					if( strOrderInformation.indexOf( strOrderId ) > -1){
						System.out.println(s9Space + "-> This order was placed: " + strOrderId );
						APP_LOGS.debug(s9Space + "-> This order was placed: " + strOrderId);
					}
					else{						
						System.out.println("-> This order was not placed: " + strOrderId );
						APP_LOGS.debug("-> This order was not placed: " + strOrderId );	
						failure(testcasename, "-> This order was not placed: " + strOrderId,"","","","");
						Assert.fail();
					    }
				
				}else{	
					System.out.println("-> You entered incorrect data, this order was not placed: " + strOrderId );
					APP_LOGS.debug("-> You entered incorrect data, this order was not placed: " + strOrderId );	
					failure(testcasename, "-> You entered incorrect data, this order was not placed: " + strOrderId,"","","","");
					Assert.fail();	
				}					
										
			}catch(Exception e){
				pageloadErrorOrException=e.getMessage();
				System.out.println("***** GOT EXCEPTION IN VerifyTrackOrder METHOD --> "+pageloadErrorOrException);
				APP_LOGS.debug("***** GOT EXCEPTION IN VerifyTrackOrder METHOD --> "+pageloadErrorOrException);
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
				Assert.fail();
			 }	
			
		}//Method VerifyTrackOrder		
		
	    
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Return Order ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	    public static void ReturnOrder() throws Exception{
			
	    	testcasename ="ReturnOrder";
	    	
			try{
				System.out.println("--> Return Order");
				APP_LOGS.debug("--> Return Order");		
				
				/*
				returnLocator("VIEW_DETAILS").click();
				Thread.sleep(15000);
				*/
				
				//Validate if Return tab is displayed
				if( isElementPresent("RETURN") ) {
					    System.out.println(s9Space + "-> Return Order page is loading correctly." );
						APP_LOGS.debug(s9Space + "-> Return Order page is loading correctly" );
					}
				else{						
						System.out.println("-> Return Order page is NOT loading correctly." );
						APP_LOGS.debug("-> Return Order page is NOT loading correctly." );	
						failure(testcasename, "-> Return Order page is NOT loading correctly.","","","","");
						Assert.fail();
					 }
				
				//SIGN OUT
				LogInfo("--> Sign Out");
				driver.get("https://www.thefryecompany.com/customer/account/logout/");
				Thread.sleep(9000);
				
			}catch(Exception e){
				pageloadErrorOrException=e.getMessage();
				LogInfo("***** GOT EXCEPTION IN ReturnOrder METHOD --> "+pageloadErrorOrException);
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
				Assert.fail();
			 }	
			
		}//ReturnOrder	
		
		
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Sign Up Newsletter ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	    public static void SignUpNewsletter() throws Exception{
			
	    	testcasename ="SignUpNewsletter";
	    		    	
			try{
				HomePageLoad();
				System.out.println("--> Sign Up Newsletter");
				APP_LOGS.debug("--> Sign Up Newsletter");		
				
				waitForLocator("EMAIL_NEWSLETTER");
				((JavascriptExecutor) driver).executeScript("scroll(0,2000);");
				Thread.sleep(5000);
				returnLocator("EMAIL_NEWSLETTER").sendKeys(Keys.ENTER);
				returnLocator("EMAIL_NEWSLETTER").sendKeys( "automation3@onestop.com" );
				System.out.println("Closing Fancybox!");
				APP_LOGS.debug("Closing Fancybox!");
/*				driver.getKeyboard().sendKeys(Keys.ESCAPE);	*/			
				waitForLocator("SUBMIT_NEWSLETTER");
				returnLocator("SUBMIT_NEWSLETTER").sendKeys(Keys.ENTER);
				Thread.sleep(20000);
				
				System.out.println(s9Space + "-> Sing Up Newsletter is working" );
				APP_LOGS.debug(s9Space + "-> Sing Up Newsletter is working");
							
																
			}catch(Exception e){
				pageloadErrorOrException=e.getMessage();
				System.out.println("***** GOT EXCEPTION IN SignUpNewsletter METHOD, Sing Up Newsletter is NOT working as expected --> "+pageloadErrorOrException);
				APP_LOGS.debug("***** GOT EXCEPTION IN SignUpNewsletter METHOD, Sing Up Newsletter is NOT working as expected --> "+pageloadErrorOrException);
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError,dropDownElementSelectionError);
				Assert.fail();
			 }	
			
		}//SignUpNewsletter
	    
		
}//Calss