package TestCases;

import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import ConfigFiles.Config;

/**
 * @author Neha Chowdhry
 */

public class CMS_Test_Suite extends Config {
	
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ACME ADMIN PAGE LOAD VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	public static void ACMEAdminVerification() throws Exception {
    	testcasename = "AcmeAdminVerification";
    	
		try{
			pageLoadStartTime = System.currentTimeMillis();
        
			if(isElementPresent("USERNAME") == true) {
				pageLoadEndTime = System.currentTimeMillis();
				System.out.println("--> ACME admin page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
				APP_LOGS.debug("--> ACME admin page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
	        }
			
			else {
	        	System.out.println("--> Automation Test failed to run");
	            APP_LOGS.debug("--> Automation Test failed to run");
	            Assert.fail();
			}
		}
		
		catch(Exception e){
			failure(testcasename);
			Assert.fail();
		}
    } 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ACME ADMIN LOG IN VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
		public static void ACMEAdminLogInVerification() throws Exception {
	    	testcasename = "AcmeAdminLogInVerification";
	    	
	    	pageLoadStartTime = System.currentTimeMillis();
	    	System.out.println("--> Entering the credentials");
            APP_LOGS.debug("--> Entering the credentials");
			try{
	        pageLoadEndTime = System.currentTimeMillis();
	        returnLocator("USERNAME").sendKeys("neha@onestop.com");
	        returnLocator("PASSWORD").sendKeys("onestop1@");
	        returnLocator("LOG_IN").click();
	        waitForLocator("LAYOUT");
	        
	        if(isElementPresent("LAYOUT") == true) {
	        System.out.println("--> ACME admin page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
	        APP_LOGS.debug("--> ACME admin page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
	        }else {
	        	System.out.println("--> Admin Page did not load");
	            APP_LOGS.debug("--> Admin Page did not load");
	        	failure(testcasename);
				Assert.fail();
	        }
			}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
	    } 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ LAYOUT PAGE LOAD VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
		public static void LayoutPageLoadVerification() throws Exception {
			testcasename = "LayoutPageLoadVerification";
			    	
			System.out.println("--> Navigating to the layout page");
		    APP_LOGS.debug("--> Navigating to the layout page");
			try{
			pageLoadStartTime = System.currentTimeMillis();
			System.out.println("--> Clicking on the layout button");
		    APP_LOGS.debug("--> Clicking on the layout button");
		    returnLocator("LAYOUT").click();
		    waitForLocator("CREATE_NEW_LAYOUT");
			pageLoadEndTime = System.currentTimeMillis();
			        
			if(isElementPresent("CREATE_NEW_LAYOUT") == true) {
			System.out.println("--> Layout page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
			APP_LOGS.debug("--> Layout page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
			}else {
			System.out.println("--> Layout Page did not load");
	        APP_LOGS.debug("--> Layout Page did not load");
			failure(testcasename);
			Assert.fail();
			}
			}catch(Exception e){
			failure(testcasename);
			Assert.fail();
			}
		} 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ DELETE PREVIOUSLY CREATED LAYOUT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
		public static void deleteOldLayout() throws Exception {
			testcasename = "DeleteOldLayout";
			
			try{
			if(("Automation Test Layout").equals(returnLocator("OLD_LAYOUT_NAME").getText()) == true){
			System.out.println("--> Deleting the previously created layout");
		    APP_LOGS.debug("--> Deleting the previously created layout");

			System.out.println("--> Clicking on the checkbox");
		    APP_LOGS.debug("--> Clicking on the checkbox");
		    returnLocator("OLD_LAYOUT_CHECKBOX").click();
		    Thread.sleep(1000);
		    System.out.println("--> Clicking on the delete button");
		    APP_LOGS.debug("--> Clicking on the delete button");
		    returnLocator("OLD_LAYOUT_DELETE").click();
		    Thread.sleep(1000);
		    driver.switchTo().alert().accept();
		    Thread.sleep(5000);
		    
		    if(("That Layout has been removed.").equals(returnLocator("OLD_LAYOUT_DELETE_VERIFICATION").getText()) == true){
		    	System.out.println("--> Old layout deleted successfully");
			    APP_LOGS.debug("--> Old layout deleted successfully");
			} else {
				System.out.println("--> Previously created layout did not get deleted");
		        APP_LOGS.debug("--> Previously created layout did not get deleted");
				failure(testcasename);
				Assert.fail();
			}
			}
			
			else
			{
				System.out.println("--> Old Layout not found");
			    APP_LOGS.debug("--> Old Layout not found");
			}
			}catch(Exception e){
			failure(testcasename);
			Assert.fail();
			}
		} 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CREATE NEW LAYOUT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
		public static void createLayout() throws Exception {
			testcasename = "createLayout";
					    	
			try{
			System.out.println("--> Creating a new layout");
			APP_LOGS.debug("--> Creating a new layout");	
		    returnLocator("CREATE_NEW_LAYOUT").click();
		    waitForLocator("LAYOUT_TITLE");
		    
		    
		    System.out.println("--> Entering the title for the layout");
		    APP_LOGS.debug("--> Entering the title for the layout");
		    returnLocator("LAYOUT_TITLE").sendKeys("Automation Test Layout");
		    
		    
		    System.out.println("--> Clicking on the XML tab");
		    APP_LOGS.debug("--> Clicking on the XML tab");
		    returnLocator("LAYOUT_XML").click();
		    waitForLocator("LAYOUT_XML_BOX");
		    returnLocator("LAYOUT_XML_BOX").clear();
		    returnLocator("LAYOUT_XML_BOX").sendKeys("<layout> <row height='50%'> <column span='4' width='20%'> </column> <column span='4' width='60%'> </column> <column span='4' width='20%'> </column> </row> </layout>");
		    
		    
		    System.out.println("--> Saving the newly created layout");
		    APP_LOGS.debug("--> Saving the newly created layout");
		    returnLocator("LAYOUT_SAVE").click();
		    waitForLocator("LAYOUT_SAVE_VERIFICATION");
		    
		    if(("Your Layout has been created.").equals(returnLocator("LAYOUT_SAVE_VERIFICATION").getText()) == true){
				System.out.println("--> New Layout created successfully");
				APP_LOGS.debug("--> New Layout created successfully");
				}else {
				System.out.println("--> New Layout not created");
			    APP_LOGS.debug("--> New Layout not created");
				failure(testcasename);
				Assert.fail();
				}
			}catch(Exception e){
			failure(testcasename);
			Assert.fail();
			}
		} 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ DELETE PREVIOUSLY CREATED TEMPLATE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
		public static void deleteOldTemplate() throws Exception {
			testcasename = "deleteOldTemplate";
				
			try{
				System.out.println("--> Navigating to the ACME dashboard");
				APP_LOGS.debug("--> Navigating to the ACME dashboard");
				returnLocator("ACME_DASHBOARD").click();
				waitForLocator("TEMPLATE");
				System.out.println("--> Clicking on the template button");
				APP_LOGS.debug("--> Clicking on the template button");
			    returnLocator("TEMPLATE").click();
			    waitForLocator("CREATE_NEW_TEMPLATE");
		    
				if(("Automation Test Template").equals(returnLocator("OLD_TEMPLATE_NAME").getText()) == true){
					System.out.println("--> Deleting the previously created template");
				    APP_LOGS.debug("--> Deleting the previously created template");
		
					System.out.println("--> Clicking on the checkbox");
				    APP_LOGS.debug("--> Clicking on the checkbox");
				    returnLocator("OLD_TEMPLATE_CHECKBOX").click();
				    Thread.sleep(1000);
				    System.out.println("--> Clicking on the delete button");
				    APP_LOGS.debug("--> Clicking on the delete button");
				    returnLocator("OLD_TEMPLATE_DELETE").click();
				    Thread.sleep(1000);
				    driver.switchTo().alert().accept();
				    Thread.sleep(5000);
					    
				    if(("That Template has been removed.").equals(returnLocator("OLD_TEMPLATE_DELETE_VERIFICATION").getText()) == true){
				    	System.out.println("--> Old template deleted successfully");
					    APP_LOGS.debug("--> Old template deleted successfully");
					} 
				    
				    else {
						failure(testcasename);
						Assert.fail();
					}
				}
						
				else{
					System.out.println("--> Previously created template did not get deleted");
			        APP_LOGS.debug("--> Previously created template did not get deleted");
					System.out.println("--> Old Template not found");
				    APP_LOGS.debug("--> Old Template not found");
				}
			}
			
			catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
		} 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CREATE NEW TEMPLATE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
		public static void createTemplate() throws Exception {
			testcasename = "createTemplate";
					
			try{
				System.out.println("--> Creating a new template");
			    APP_LOGS.debug("--> Creating a new template");
			    returnLocator("CREATE_NEW_TEMPLATE").click();
			    waitForLocator("TEMPLATE_TITLE");
				
			    System.out.println("--> Entering the title for the template");
			    APP_LOGS.debug("--> Entering the title for the template");
			    returnLocator("TEMPLATE_TITLE").sendKeys("Automation Test Template");
					    
			    System.out.println("--> Clicking on the template icon");
			    APP_LOGS.debug("--> Clicking on the template icon");
			    returnLocator("TEMPLATE_ICON").click();
			    System.out.println("--> Selecting the layout");
			    APP_LOGS.debug("--> Selecting the layout");
			    Select layout = new Select(returnLocator("SELECT_LAYOUT"));
		        layout.selectByIndex(1);
		        System.out.println("--> Clicking on the save button");
			    APP_LOGS.debug("--> Clicking on the save button");
			    returnLocator("TEMPLATE_SAVE").click();
			    waitForLocator("TEMPLATE_SAVE_VERIFICATION");
				    
			    if(("Your Template has been created.").equals(returnLocator("TEMPLATE_SAVE_VERIFICATION").getText()) == true){
					System.out.println("--> Layout for the template selected successfully");
					APP_LOGS.debug("--> Layout for the template selected successfully");
					}else {
					failure(testcasename);
					Assert.fail();
					}
			    
			    System.out.println("--> Clicking on the XML tab");
			    APP_LOGS.debug("--> Clicking on the XML tab");
			    returnLocator("TEMPLATE_XML").click();
			    waitForLocator("TEMPLATE_XML_BOX");
			    returnLocator("TEMPLATE_XML_BOX").clear();
			    returnLocator("TEMPLATE_XML_BOX").sendKeys("<layout><row height='50%'><column span='4' width='20%'><img height='40%' width='100%' /><video height='70%' width='65%'></video></column><column span='4' width='60%'><container width='100%' height='40%'><part part='BodyPart'></part></container><container height='40%' top='100px' width='96.2%'><part part='BodyPart'></part></container></column><column span='4' width='20%'><text title='Test Col. 1' class='auto-css-1'></text></column></row></layout>");
			    System.out.println("--> Clicking on the save button");
			    APP_LOGS.debug("--> Clicking on the save button");
			    returnLocator("TEMPLATE_SAVE").click();
			    waitForLocator("TEMPLATE_SAVE_VERIFICATION");
				    
			    if(("Your Template has been saved.").equals(returnLocator("TEMPLATE_SAVE_VERIFICATION").getText()) == true){
					System.out.println("--> New template created successfully");
					APP_LOGS.debug("--> New template created successfully");
					}else {
					System.out.println("--> New template not created");
					APP_LOGS.debug("--> New template not created");
					failure(testcasename);
					Assert.fail();
					}
			    
				}catch(Exception e){
				failure(testcasename);
				Assert.fail();
				}
		}		    	
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ DELETE PREVIOUSLY CREATED SLIDESHOW ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
		public static void deleteOldSlideshow() throws Exception {
			testcasename = "deleteOldSlideshow";
					
			try{
			System.out.println("--> Navigating to the ACME dashboard");
			APP_LOGS.debug("--> Navigating to the ACME dashboard");
			returnLocator("ACME_DASHBOARD").click();
			waitForLocator("TEMPLATE");
			System.out.println("--> Clicking on the slidehsow button");
			APP_LOGS.debug("--> Clicking on the slideshow button");
		    returnLocator("SLIDESHOW").click();
		    waitForLocator("CREATE_NEW_SLIDESHOW");
				    
					
			if(("Automation Test Slideshow").equals(returnLocator("OLD_SLIDESHOW_NAME").getText()) == true){
			System.out.println("--> Deleting the previously created slideshow");
		    APP_LOGS.debug("--> Deleting the previously created slideshow");
			System.out.println("--> Clicking on the checkbox");
		    APP_LOGS.debug("--> Clicking on the checkbox");
		    returnLocator("OLD_SLIDESHOW_CHECKBOX").click();
		    Thread.sleep(1000);
		    
		    if(isElementPresent("OLD_SLIDESHOW_DELETE1") == true) {
		    System.out.println("--> Clicking on the delete button");
			APP_LOGS.debug("--> Clicking on the delete button");
			returnLocator("OLD_SLIDESHOW_DELETE1").click();
			Thread.sleep(1000);
			driver.switchTo().alert().accept();
			Thread.sleep(5000);
		    }
		    else if(isElementPresent("OLD_SLIDESHOW_DELETE2") == true) {
			System.out.println("--> Clicking on the delete button");
			APP_LOGS.debug("--> Clicking on the delete button");
			returnLocator("OLD_SLIDESHOW_DELETE2").click();
			Thread.sleep(1000);
			driver.switchTo().alert().accept();
			Thread.sleep(5000);
			}
						    
			if(("That Slide Show has been removed.").equals(returnLocator("OLD_SLIDESHOW_DELETE_VERIFICATION").getText()) == true){
			   	System.out.println("--> Old template deleted successfully");
			    APP_LOGS.debug("--> Old template deleted successfully");
			} else {
				System.out.println("--> Previously created slideshow did not get deleted");
		        APP_LOGS.debug("--> Previously created slideshow did not get deleted");
				failure(testcasename);
				Assert.fail();
			}
			}
							
			else
			{
				System.out.println("--> Old Slideshow not found");
			    APP_LOGS.debug("--> Old Slideshow not found");
			}
			}catch(Exception e){
			failure(testcasename);
			Assert.fail();
			}
		} 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CREATE NEW SLIDESHOW ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
		public static void createSlideshow() throws Exception {
			testcasename = "createSlideshow";
			
			try{
				System.out.println("--> Creating a new slideshow");
				APP_LOGS.debug("--> Creating a new slideshow");
			    returnLocator("CREATE_NEW_SLIDESHOW").click();
			    waitForLocator("SLIDESHOW_TITLE");
			    
			    
			    System.out.println("--> Entering the title for the slideshow");
			    APP_LOGS.debug("--> Entering the title for the slideshow");
			    returnLocator("SLIDESHOW_TITLE").sendKeys("Automation Test Slideshow");
			    
			    
			    System.out.println("--> Creating a link for the slideshow");
			    APP_LOGS.debug("--> Creating a link for the slideshow");
			    returnLocator("SLIDESHOW_LINK").sendKeys("auto-test");
			    
			    System.out.println("--> Saving the newly created slideshow");
			    APP_LOGS.debug("--> Saving the newly created slideshow");
			    returnLocator("SLIDESHOW_SAVE").click();
			    waitForLocator("LAYOUT_SAVE_VERIFICATION");
			    
			    if(("Your Slide Show has been created.").equals(returnLocator("SLIDESHOW_SAVE_VERIFICATION").getText()) == true){
					System.out.println("--> New slideshow created successfully");
					APP_LOGS.debug("--> New slideshow created successfully");
					}else {
					failure(testcasename);
					Assert.fail();
					}
				}catch(Exception e){
				failure(testcasename);
				Assert.fail();
				}
		}		  
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ADD SLIDES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
		public static void addSlides() throws Exception {
			testcasename = "AddSlides";
				
			try{
				System.out.println("--> Adding new slide");
				APP_LOGS.debug("--> Adding new slide");
			    returnLocator("ADD_SLIDES").click();
			    waitForLocator("SELECT_TEMPLATE");
				    
					    
			    System.out.println("--> Selecting the template for the slideshow");
			    APP_LOGS.debug("--> Selecting the template for the slideshow");
			    Select slide1 = new Select(returnLocator("SELECT_TEMPLATE"));
		        slide1.selectByIndex(1);
		        System.out.println("--> Clicking the save button");
			    APP_LOGS.debug("--> Clicking the save button");
			    returnLocator("NEW_SLIDE_SAVE").click();
			    waitForLocator("IMAGE_URL");
					    
					    
			    System.out.println("--> Adding an image to the slide");
			    APP_LOGS.debug("--> Adding an image to the slide");
			    returnLocator("IMAGE_URL").click();
			    returnLocator("IMAGE_URL").sendKeys("~/Media/ACME/Test/Penguins.jpg");
			    Thread.sleep(9000);
			    
			    System.out.println("--> Adding text to the slide");
			    APP_LOGS.debug("--> Adding text to the slide");
			    returnLocator("TEXT_BOX").sendKeys("CMS Automation Script");
			    Thread.sleep(9000);
					    
			    System.out.println("--> Saving the newly created slide");
			    APP_LOGS.debug("--> Saving the newly created slide");
			    returnLocator("SLIDE_SAVE").click();
			    waitForLocator("SLIDE_SAVE_VERIFICATION");
					    
			    if(("Your Slide has been saved.").equals(returnLocator("SLIDE_SAVE_VERIFICATION").getText()) == true){
					System.out.println("--> New slide created successfully");
					APP_LOGS.debug("--> New slide created successfully");
					}else {
					System.out.println("--> New slide not created");
				    APP_LOGS.debug("--> New slide not created");
					failure(testcasename);
					Assert.fail();
				}
			    System.out.println("--> Adding another slide");
				APP_LOGS.debug("--> Adding another slide");
			    returnLocator("ADD_SLIDES").click();
			    waitForLocator("SELECT_TEMPLATE");
				    
					    
			    System.out.println("--> Selecting the template for the slideshow");
			    APP_LOGS.debug("--> Selecting the template for the slideshow");
			    Select slide2 = new Select(returnLocator("SELECT_TEMPLATE"));
		        slide2.selectByIndex(1);
		        System.out.println("--> Clicking the save button");
			    APP_LOGS.debug("--> Clicking the save button");
			    returnLocator("NEW_SLIDE_SAVE").click();
			    waitForLocator("IMAGE_URL");
					    
			    System.out.println("--> Adding an image to the slide");
			    APP_LOGS.debug("--> Adding an image to the slide");
			    returnLocator("IMAGE_URL").click();
			    returnLocator("IMAGE_URL").sendKeys("~/Media/ACME/Test/Hydrangeas.jpg");
			    Thread.sleep(9000);
			    
			    System.out.println("--> Adding text to the slide");
			    APP_LOGS.debug("--> Adding text to the slide");
			    returnLocator("TEXT_BOX").sendKeys("Automation Rocks!!!");
			    Thread.sleep(9000);
					    
			    System.out.println("--> Saving the newly created slide");
			    APP_LOGS.debug("--> Saving the newly created slide");
			    returnLocator("SLIDE_SAVE").click();
			    waitForLocator("SLIDE_SAVE_VERIFICATION");
					    
			    if(("Your Slide has been saved.").equals(returnLocator("SLIDE_SAVE_VERIFICATION").getText()) == true){
					System.out.println("--> New slide created successfully");
					APP_LOGS.debug("--> New slide created successfully");
					}else {
					System.out.println("--> New slide not created");
					APP_LOGS.debug("--> New slide not created");
					failure(testcasename);
					Assert.fail();
				}
			    
			    System.out.println("--> Clicking on the save button for saving the slideshow");
			    APP_LOGS.debug("--> Clicking on the save button for saving the slideshow");
			    returnLocator("SLIDESHOW_SAVE").click();
			    waitForLocator("SLIDESHOW_SAVE_VERIFICATION");
			    
			    if(("Your Slide Show has been saved.").equals(returnLocator("SLIDESHOW_SAVE_VERIFICATION").getText()) == true){
					System.out.println("--> New slideshow saved successfully");
					APP_LOGS.debug("--> New slideshow saved successfully");
					}else {
					System.out.println("--> Slideshow could not be saved");
					APP_LOGS.debug("--> Slideshow could not be saved");
					failure(testcasename);
					Assert.fail();
					}
			    
			    
				}catch(Exception e){
				failure(testcasename);
				Assert.fail();
				}
		}		  
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ METHOD CAPTURE ON SCRIPT FAILURE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void failure(String testcasename) throws Exception{
		String currentURL=driver.getCurrentUrl();
		System.out.println(":=> Failed while performing "+ testcasename +" ("+currentURL + ")");
		APP_LOGS.debug(":=> Failed while performing "+ testcasename +" ("+currentURL + ")");
		System.out.println("Capturing the screenshot");
		APP_LOGS.debug("Capturing the screenshot");
		captureScreenShot(sitename + "_" + envname + "_");
		System.out.println("Capturing the response headers on the page");
		APP_LOGS.debug("Capturing the response header on the page");
		captureHeaders(sitename + "_" + envname + "_");
		ErrorList.add("Failed while performing " + testcasename +" ("+currentURL + ")");
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
}  