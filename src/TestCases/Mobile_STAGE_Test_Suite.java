package TestCases;

import java.util.List;

/**
 * @author Neha Chowdhry
 */







import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import ConfigFiles.Config;


public class Mobile_STAGE_Test_Suite extends Config  {
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ HOME PAGE LOAD VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
		public static void HomePageLoad() throws Exception {
	    	testcasename = "HomePageLoadVerification";
	    	
			System.out.println("--> Navigating to the home page");
			APP_LOGS.debug("--> Navigating to the home page");
			try{
			pageLoadStartTime = System.currentTimeMillis();
			SiteURLs.clear();
			SiteURLs=readTestDataFromExcel(siteURLsExcel, 6, sitename);
			System.out.println(SiteURLs.get(1));
			APP_LOGS.debug(SiteURLs.get(1));
			driver.get(SiteURLs.get(1));
			
			if("NYDJ".equals(sitename)) {
				if(isElementPresent("FANCYBOX") == true) {
				System.out.println("--> Closing the pop-up");
		 	    APP_LOGS.debug("--> Closing the pop-up");
		 	    returnLocator("FANCYBOX").click(); 
				}
			}
			
			waitForLocator("MY_ACCOUNT");
	        pageLoadEndTime = System.currentTimeMillis();
	        System.out.println("--> Home page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
	 	    APP_LOGS.debug("--> Home page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
			}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
		}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MY ACCOUNT SIGN IN ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	    public static void MyAccountSignIn() throws Exception {    	
			testcasename = "MyAccountSignIn";
			
			System.out.println("--> Signing in to my account");
			APP_LOGS.debug("--> Signing in to my account");
			try{
				System.out.println("--> Clicking on the My Account button");
				APP_LOGS.debug("--> Clicking on the My Account button");
				((JavascriptExecutor) driver).executeScript("scroll(0,250);");
				pageLoadStartTime = System.currentTimeMillis();
				
				if("TRIH".equals(sitename)) 
					driver.get("http://trih-np.stage.onestop.com/login");
				else if("CBTL".equals(sitename)) 
					driver.get("http://cbtl-np.stage.onestop.com/login");
				else if("PAIG".equals(sitename)) 
					driver.get("http://paig-np.stage.onestop.com/login");
				else
					returnLocator("MY_ACCOUNT").sendKeys(Keys.RETURN);
				
				waitForLocator("USERNAME");
				Thread.sleep(10000);
				pageLoadEndTime = System.currentTimeMillis();
				
				System.out.println("--> Entering the credentials");
				APP_LOGS.debug("--> Entering the credentials");
		 	    returnLocator("USERNAME").click();
				returnLocator("USERNAME").sendKeys("automation1@onestop.com");
				returnLocator("PASSWORD").click();
				returnLocator("PASSWORD").sendKeys("onestop1");
				pageLoadStartTime = System.currentTimeMillis();
				System.out.println("--> Clicking on the log in button");
				APP_LOGS.debug("--> Clicking on the log in button");
				returnLocator("LOG_IN").sendKeys(Keys.RETURN);
			//	waitForLocator("CLICK_HERE_TO_LOG_OFF");
				Thread.sleep(10000);
				pageLoadEndTime = System.currentTimeMillis();
				
				if(isElementPresent("CLICK_HERE_TO_LOG_OFF") == true) {
				System.out.println("--> Account Details page loaded successfully in: "+(((pageLoadEndTime-pageLoadStartTime)/1000) - 10) + " seconds");
				APP_LOGS.debug("--> Account Details page loaded successfully in: "+(((pageLoadEndTime-pageLoadStartTime)/1000) - 10) + " seconds");
				} else {
					failure(testcasename);
					Assert.fail();
				}
			}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
	    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SHOPPING CART VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	    public static void EmptyShoppingCart() throws Exception {
	    	testcasename = "EmptyShoppingCart";
			
			System.out.println("--> Clicking on the shopping cart button");
			APP_LOGS.debug("--> Clicking on the shopping cart button");
			try {
				Thread.sleep(5000);
			
				driver.get(SiteURLs.get(1));
			returnLocator("SHOPPING_CART").click();
			
			if(isElementPresent("EDIT_ITEM")) {
				while (returnLocator("EDIT_ITEM").isDisplayed()==true) 
				{
				System.out.println("--> Clicking on the edit item button");
				APP_LOGS.debug("--> Clicking on the edit item button");
				returnLocator("EDIT_ITEM").click();
				Thread.sleep(10000);
				
				System.out.println("--> Removing items from the shopping cart");
			    APP_LOGS.debug("--> Removing items from the shopping cart");
	    		returnLocator("DELETE_ITEM").click();
				
				waitForLocator("SHOPPING_CART");
				returnLocator("SHOPPING_CART").click();
				Thread.sleep(5000);
				if(isElementPresent("EDIT_ITEM")==false)
				{
					System.out.println("--> Shopping cart is empty");
		    	    APP_LOGS.debug("--> Shopping cart is empty");
		    	    returnLocator("SHOPPING_CART").click();
					break;
				}
				
				}
			
				Thread.sleep(10000);
			} else {
					System.out.println("--> Shopping cart is already empty");
			    	APP_LOGS.debug("--> Shopping cart is already empty");
			    	System.out.println("--> Moving back to the home page");
				    APP_LOGS.debug("--> Moving back to the home page");
				    driver.get(SiteURLs.get(1));
			} 
			}catch (Exception e){
				failure(testcasename);
				Assert.fail();
			}
	    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SEARCH VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	    public static void SearchForAProduct() throws Exception {
	    	testcasename = "SearchForAProduct";
	    	pageLoadStartTime = System.currentTimeMillis();
	    	
			System.out.println("--> Searching for a product");
			APP_LOGS.debug("--> Searching for a product");
			try{
				switch (sitename) {
				 case "CBTL":
				 case "PAIG":
				  case "CAML":
					 break;
					 
				default:
				returnLocator("SEARCH_LINK").click();
				waitForLocator("SEARCH_BOX");
				returnLocator("SEARCH_BOX").sendKeys(LocatorProps.getProperty("SEARCH_KEYWORD"));
				returnLocator("SEARCH_BOX").sendKeys(Keys.RETURN);
				Thread.sleep(10000);
				waitForLocator("PLP1");
				pageLoadEndTime = System.currentTimeMillis();
				
				if(isElementPresent("PLP1") == true) {
					System.out.println("--> Search Results page loaded successfully in: "+(((pageLoadEndTime-pageLoadStartTime)/1000)-10) + " seconds");
					APP_LOGS.debug("--> Search Results page loaded successfully in: "+(((pageLoadEndTime-pageLoadStartTime)/1000) - 10) + " seconds");
					} else {
						failure(testcasename);
						Assert.fail();
					}
				
				List<WebElement> Count = driver.findElements(By.xpath(LocatorProps.getProperty("SEARCH_COUNT")));
				if (Count.size()>0)
				{
					System.out.println("--> Number of Search Results displayed on the page: "+ Count.size());		
					APP_LOGS.debug("--> Number of Search Results displayed on the page: "+ Count.size());		
				} else {
					failure(testcasename);
					Assert.fail();
				}
				break;
				}
			}catch(Exception e){
					failure(testcasename);
					Assert.fail();
				}
	    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PLP VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	    public static void GoToPLP() throws Exception {
	    	testcasename = "GoToPLP";
	    	pageLoadStartTime = System.currentTimeMillis();
			
			System.out.println("--> Navigating to the PLP");
			APP_LOGS.debug("--> Navigating to the PLP");
			try{
				returnLocator("PLP1").click();
				Thread.sleep(10000);
				waitForLocator("PLP2");
				
	 			returnLocator("PLP2").click();
				waitForLocator("PLP3");

				if("SEVN".equals(sitename)) {
					returnLocator("PLP4").click();
					waitForLocator("PLP3");
				}
				
				returnLocator("PLP3").click();
				waitForLocator("PDP");
				Thread.sleep(10000);
				pageLoadEndTime = System.currentTimeMillis();
				
				if(isElementPresent("PDP") == true) {
					System.out.println("--> PLP loaded successfully in: "+(((pageLoadEndTime-pageLoadStartTime)/1000) - 10) + " seconds");
					APP_LOGS.debug("--> PLP loaded successfully in: "+(((pageLoadEndTime-pageLoadStartTime)/1000) - 10) + " seconds");
					} else {
						failure(testcasename);
						Assert.fail();						
					}
			}catch(Exception e){
					failure(testcasename);
					Assert.fail();
				}
	    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PDP VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	    public static void GoToPDP() throws Exception {
			testcasename = "GoToPDP";
	    	pageLoadStartTime = System.currentTimeMillis();
			
			System.out.println("--> Navigating to the PDP");
			APP_LOGS.debug("--> Navigating to the PDP");
			try{
				returnLocator("PDP").click();
				waitForLocator("ADD_TO_CART");
				Thread.sleep(10000);
				pageLoadEndTime = System.currentTimeMillis();
				
				if(isElementPresent("ADD_TO_CART") == true) {
					System.out.println("--> PDP loaded successfully in: "+(((pageLoadEndTime-pageLoadStartTime)/1000)-10) + " seconds");
					APP_LOGS.debug("--> PDP loaded successfully in: "+(((pageLoadEndTime-pageLoadStartTime)/1000)-10) + " seconds");
					} else {
						failure(testcasename);
						Assert.fail();						
					}
			}catch(Exception e){
					failure(testcasename);
					Assert.fail();
				}
	    }  
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SIZE/COLOR VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	    public static void SelectProductSizeOrColor() throws Exception {
	    	testcasename = "SelectProductSizeOrColor";
			try{
				switch (sitename) {
				 case "CAML":
				 case "CBTL":
					 break;

				 case "HUDS":
				 case "PAIG":
				 case "SEVN":
					 if (isElementPresent("SIZE") == true) 
						{
						System.out.println("--> Selecting the size/color of the product");
						APP_LOGS.debug("--> Selecting the size/color of the product");
						((JavascriptExecutor) driver).executeScript("scroll(0,250);");
						returnLocator("SIZE").sendKeys(Keys.RETURN);
						waitForLocator("ADD_TO_CART");	
						Thread.sleep(9000);
						}else {
							System.out.println("--> No size/color is available for selection");
							APP_LOGS.debug("--> No size/color is available for selection");
						}
					 break;

				 case "PEBA":
				 case "TRIH":
					 if (isElementPresent("SIZE") == true) 
						{
						System.out.println("--> Selecting the size of the product");
						APP_LOGS.debug("--> Selecting the size of the product");
						((JavascriptExecutor) driver).executeScript("scroll(0,250);");
						returnLocator("SIZE").sendKeys(Keys.RETURN);
						waitForLocator("ADD_TO_CART");	
						Thread.sleep(9000);
						}else {
							System.out.println("--> No size is available for selection");
							APP_LOGS.debug("--> No size is available for selection");
						}
					 if (isElementPresent("COLOR") == true) 
						{
						System.out.println("--> Selecting the color of the product");
						APP_LOGS.debug("--> Selecting the color of the product");
						returnLocator("COLOR").sendKeys(Keys.RETURN);
						waitForLocator("ADD_TO_CART");	
						Thread.sleep(9000);
						}else {
							System.out.println("--> No color is available for selection");
							APP_LOGS.debug("--> No color is available for selection");
						}
					 break;

				 case "NYDJ":
				 case "SHEX":
				 case "SPLD":
					 if (isElementPresent("SIZE") == true) 
						{
						System.out.println("--> Selecting the size of the product");
						APP_LOGS.debug("--> Selecting the size of the product");
						((JavascriptExecutor) driver).executeScript("scroll(0,250);");
						returnLocator("SIZE").sendKeys(Keys.RETURN);
						waitForLocator("ADD_TO_CART");	
						Thread.sleep(9000);
						}else {
							System.out.println("--> No size is available for selection");
							APP_LOGS.debug("--> No size is available for selection");
						}
					 if (isElementPresent("COLOR") == true) 
						{
						System.out.println("--> Selecting the color of the product");
						APP_LOGS.debug("--> Selecting the color of the product");
						((JavascriptExecutor) driver).executeScript("scroll(0,250);");
						returnLocator("COLOR").sendKeys(Keys.RETURN);
						waitForLocator("ADD_TO_CART");	
						Thread.sleep(9000);
						}else {
							System.out.println("--> No color is available for selection");
							APP_LOGS.debug("--> No color is available for selection");
						}
					 break;
				}
				}catch(Exception e){
					failure(testcasename);
					Assert.fail();
				}
			}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ADD TO CART / CHECKOUT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	    public static void AddToCartAndCheckout() throws Exception {
	    	testcasename = "AddToCartAndCheckout";
			try{
					System.out.println("--> Clicking on the add to cart button");
					APP_LOGS.debug("--> Clicking on the add to cart button");
					
					if("PAIG".equals(sitename))
					returnLocator("ADD_TO_CART").sendKeys(Keys.RETURN);	
					else if("HUDS".equals(sitename))
					returnLocator("ADD_TO_CART").sendKeys(Keys.RETURN);	
					else
					returnLocator("ADD_TO_CART").click();
					
					Thread.sleep(20000);
					pageLoadStartTime = System.currentTimeMillis();
					
					System.out.println("--> Clicking on the checkout button");
					APP_LOGS.debug("--> Clicking on the checkout button");
					returnLocator("CHECKOUT").sendKeys(Keys.RETURN);
					Thread.sleep(20000);
				
					
					waitForLocator("SPCSHIPPING_INFO");
					Thread.sleep(20000);
					pageLoadEndTime = System.currentTimeMillis();
					
					if(isElementPresent("SPCSHIPPING_INFO") == true){
					System.out.println("--> Final checkout page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
					APP_LOGS.debug("--> Final checkout page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
					} else {
						failure(testcasename);
						Assert.fail();
					}
			}catch(Exception e){
					failure(testcasename);
					Assert.fail();
				}
	    } 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SHIPPING INFO ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	    public static void enterShippingInfo() throws Exception {
	    	testcasename = "enterShippingInfo";
			try{
				
				Thread.sleep(20000);
				switch (sitename) {
										
		/*		case "HUDS":
					System.out.println("--> Clicking on the shipping info button");
					APP_LOGS.debug("--> Clicking on the shipping info button");
					returnLocator("SPCSHIPPING_INFO").click();
				
					Thread.sleep(7000);
					System.out.println("--> Selecting the default address for automation");
					APP_LOGS.debug("--> Selecting the default address for automation");
					Select shippinginfo5 = new Select(driver.findElement(By.xpath("//*[@id='shipping-info']/div/form/div[1]/div/div[1]/select")));
					shippinginfo5.selectByIndex(0);
					Thread.sleep(9000);											
					System.out.println("--> Clicking on the continue button");
					APP_LOGS.debug("--> Clicking on the continue button");
					((JavascriptExecutor) driver).executeScript("scroll(0,250);");
					returnLocator("SPCSHIPPING_INFO_CONTINUE").click();
					Thread.sleep(10000);
					if(isElementPresent("SPCSHIPPING_CHOICE_CONTINUE") == true) {
						System.out.println("--> Clicking on the shipping info continue button");
						APP_LOGS.debug("--> Clicking on the shipping choice continue button");
						((JavascriptExecutor) driver).executeScript("scroll(0,250);");
						returnLocator("SPCSHIPPING_CHOICE_CONTINUE").click();
						Thread.sleep(10000);
						}
						else
						{
							driver.findElement(By.xpath("//*[@id='checkout-details']/div[3]/div[1]/h4/a")).click();
							Thread.sleep(2000);
							System.out.println("--> Clicking on the shipping info continue button");
							APP_LOGS.debug("--> Clicking on the shipping choice continue button");
							((JavascriptExecutor) driver).executeScript("scroll(0,250);");
							returnLocator("SPCSHIPPING_CHOICE_CONTINUE").click();
						}
					break;*/
					
				
				case "EAGL":
				//case "NYDJ":
				case "PAIG":

				case "SEVN":
			
					if(isElementPresent("SPCSHIPPING_CHOICE_CONTINUE") == true) {
						
					}
					else
					{
						driver.findElement(By.xpath("//*[@id='checkout-details']/div[2]/div[1]/h4/a")).click();
					}
					System.out.println("--> Selecting the default address for automation");
					APP_LOGS.debug("--> Selecting the default address for automation");
					Select shippinginfo4 = new Select(driver.findElement(By.xpath("//div[@id='shipping-info']/div/form/div[1]/div/div[1]/select")));
					shippinginfo4.selectByIndex(0);
					Thread.sleep(9000);
					System.out.println("--> Clicking on the continue button");
					APP_LOGS.debug("--> Clicking on the continue button");
					((JavascriptExecutor) driver).executeScript("scroll(0,250);");
					returnLocator("SPCSHIPPING_INFO_CONTINUE").click();
					Thread.sleep(10000);
					if(isElementPresent("SPCSHIPPING_CHOICE_CONTINUE") == true) {
					System.out.println("--> Clicking on the shipping info continue button");
					APP_LOGS.debug("--> Clicking on the shipping choice continue button");
					((JavascriptExecutor) driver).executeScript("scroll(0,250);");
					returnLocator("SPCSHIPPING_CHOICE_CONTINUE").click();
					Thread.sleep(10000);
					}
					else
					{
						driver.findElement(By.xpath("//*[@id='checkout-details']/div[3]/div[1]/h4/a")).click();
						Thread.sleep(2000);
						System.out.println("--> Clicking on the shipping info continue button");
						APP_LOGS.debug("--> Clicking on the shipping choice continue button");
						((JavascriptExecutor) driver).executeScript("scroll(0,250);");
						returnLocator("SPCSHIPPING_CHOICE_CONTINUE").click();
					}
					
					break;
				
				case "SPLD":
				case "NYDJ":
					if(isElementPresent("SPCSHIPPING_INFO")==true){
						System.out.println("--> Clicking on the shipping info button");
					APP_LOGS.debug("--> Clicking on the shipping info button");
					returnLocator("SPCSHIPPING_INFO").click();
											
					}
						
					System.out.println("--> Selecting the default address for automation");
					APP_LOGS.debug("--> Selecting the default address for automation");
					Select shippinginfo6 = new Select(driver.findElement(By.xpath("//div[@id='shipping-info']/div/form/div[1]/div/div[1]/select")));
					shippinginfo6.selectByVisibleText("Manish US");
					Thread.sleep(9000);
					System.out.println("--> Clicking on the continue button");
					APP_LOGS.debug("--> Clicking on the continue button");
					((JavascriptExecutor) driver).executeScript("scroll(0,250);");
					returnLocator("SPCSHIPPING_INFO_CONTINUE").click();
					Thread.sleep(10000);
					if(isElementPresent("SPCSHIPPING_CHOICE_CONTINUE") == true) {
						System.out.println("--> Clicking on the shipping info continue button");
						APP_LOGS.debug("--> Clicking on the shipping choice continue button");
						((JavascriptExecutor) driver).executeScript("scroll(0,250);");
						returnLocator("SPCSHIPPING_CHOICE_CONTINUE").click();
						Thread.sleep(10000);
						}
						else
						{
							driver.findElement(By.xpath("//*[@id='checkout-details']/div[3]/div[1]/h4/a")).click();
							Thread.sleep(2000);
							System.out.println("--> Clicking on the shipping info continue button");
							APP_LOGS.debug("--> Clicking on the shipping choice continue button");
							((JavascriptExecutor) driver).executeScript("scroll(0,250);");
							returnLocator("SPCSHIPPING_CHOICE_CONTINUE").click();
						}
					break;
				case "CAML":
				case "CBTL":
				case "TRIH":
				case "HUDS":
					
if(isElementPresent("SPCSHIPPING_INFO") == true) {
	
	System.out.println("--> Clicking on the shipping info button");
	APP_LOGS.debug("--> Clicking on the shipping info button");
	returnLocator("SPCSHIPPING_INFO").click();

						
					}

					
					System.out.println("--> Entering the First Name");
					APP_LOGS.debug("--> Entering the First Name");
					returnLocator("SPCSHIPPING_FIRST_NAME").clear();
					returnLocator("SPCSHIPPING_FIRST_NAME").click();
					returnLocator("SPCSHIPPING_FIRST_NAME").sendKeys("Onestop");
					
					System.out.println("--> Entering the Last Name");
					APP_LOGS.debug("--> Entering the Last Name");
					returnLocator("SPCSHIPPING_LAST_NAME").clear();
					returnLocator("SPCSHIPPING_LAST_NAME").click();
					returnLocator("SPCSHIPPING_LAST_NAME").sendKeys("QA");
					
					System.out.println("--> Entering the address");
					APP_LOGS.debug("--> Entering the address");
					returnLocator("SPCSHIPPING_ADDRESS").clear();
					returnLocator("SPCSHIPPING_ADDRESS").click();
					returnLocator("SPCSHIPPING_ADDRESS").sendKeys("3040 E Ana St.");
					
					System.out.println("--> Entering the city");
					APP_LOGS.debug("--> Entering the city");
					returnLocator("SPCSHIPPING_CITY").clear();
					returnLocator("SPCSHIPPING_CITY").click();
					returnLocator("SPCSHIPPING_CITY").sendKeys("Compton");
					
					System.out.println("--> Selecting the state ");
					APP_LOGS.debug("--> Selecting the state ");
					Select state3 = new Select(returnLocator("SPCSHIPPING_STATE"));
					state3.selectByVisibleText("California (US)");
					
					System.out.println("--> Entering the zip code");
					APP_LOGS.debug("--> Entering the zip code");
					returnLocator("SPCSHIPPING_ZIPCODE").clear();
					returnLocator("SPCSHIPPING_ZIPCODE").click();
					returnLocator("SPCSHIPPING_ZIPCODE").sendKeys("90221");
					
					System.out.println("--> Entering the phone number");
					APP_LOGS.debug("--> Entering the phone number");
					returnLocator("SPCSHIPPING_PHONE").clear();
					returnLocator("SPCSHIPPING_PHONE").click();
					returnLocator("SPCSHIPPING_PHONE").sendKeys("619-888-4235");
					
					System.out.println("--> Entering the email");
					APP_LOGS.debug("--> Entering the email");
					returnLocator("SPCSHIPPING_EMAIL").clear();
					returnLocator("SPCSHIPPING_EMAIL").click();
					returnLocator("SPCSHIPPING_EMAIL").sendKeys("automation1@onestop.com");
					
				if(sitename.equals("TRIH")){
						Thread.sleep(10000);
						System.out.println("--> Clicking on the billing shipping check box");
						APP_LOGS.debug("--> Clicking on the continue button");
						((JavascriptExecutor) driver).executeScript("scroll(0,250);");
						returnLocator("SPCSHIPPING_INFO_CHKBOX").click();
					}
				Thread.sleep(10000);
					System.out.println("--> Clicking on the continue button");
					APP_LOGS.debug("--> Clicking on the continue button");
					((JavascriptExecutor) driver).executeScript("scroll(0,250);");
					returnLocator("SPCSHIPPING_INFO_CONTINUE").click();
					Thread.sleep(10000);
					}
			
			}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
	    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ VERIFY SALES TAX ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	    public static void VerifySalesTax() throws Exception {
	    	testcasename = "VerifySalesTax";
			try{
					System.out.println("--> Verifying the sales tax");
					APP_LOGS.debug("--> Verifying the sales tax");
					((JavascriptExecutor) driver).executeScript("scroll(0,250);");

					String salesTax=returnLocator("SALES_TAX").getText().replace("\n", "").replace("Sales Tax:", "").trim();
					if((!salesTax.equals("$0.00") && (!salesTax.equals("[TBD]")))){
						System.out.println("Sales tax value is displayed as: " + salesTax);
						APP_LOGS.debug("Sales tax value is displayed as: " + salesTax);											
					}else{
						failure("RESULT: FAIL - SPC Sales tax value $0.00 / [TBD]");
						
					}
					
			}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}
			}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PAYPAL VERIFICATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	    public static void VerifyPaypalPage() throws Exception {
	    	testcasename = "VerifyPaypalPage";
			
	    	try{
	    		if("PEBA".equals(sitename)){
	    			System.out.println("--> Clicking on the payment info tab");
					APP_LOGS.debug("--> Clicking on the payment info tab");
					returnLocator("SPCPAYMENT_INFO").click();
					Thread.sleep(9000);
	    		}
	    		if("HUDS".equals(sitename)){
	    			System.out.println("--> Clicking on the  shipping choice continue button");
					APP_LOGS.debug("--> Clicking on the  shipping choice continue button");
					returnLocator("SPCSHIPPING_CHOICE_CONTINUE").click();
					Thread.sleep(9000);
	    			
	    		}
	    		
					System.out.println("--> Clicking on the paypal tab");
					APP_LOGS.debug("--> Clicking on the paypal tab");
					((JavascriptExecutor) driver).executeScript("scroll(0,250);");
					returnLocator("SPC_PAYPAL_TAB").sendKeys(Keys.RETURN);
					waitForLocator("SPC_PAYPAL_NEXT");
									
					pageLoadStartTime = System.currentTimeMillis();
					System.out.println("--> Clicking on the next button to go to paypal page");
					APP_LOGS.debug("--> Clicking on the next button to go to paypal page");
					returnLocator("SPC_PAYPAL_NEXT").sendKeys(Keys.RETURN);
					Thread.sleep(20000);
					
					if(isElementPresent("PAYPAL1") == true) {
					pageLoadEndTime = System.currentTimeMillis();
					System.out.println("--> Paypal page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
					APP_LOGS.debug("--> Paypal page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
					} else if(isElementPresent("PAYPAL2") == true) {
					pageLoadEndTime = System.currentTimeMillis();
					System.out.println("--> Paypal page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
					APP_LOGS.debug("--> Paypal page loaded successfully in: "+((pageLoadEndTime-pageLoadStartTime)/1000)+" seconds");
					} else {
					failure(testcasename);
					Assert.fail();
				}
				}catch(Exception e){
				failure(testcasename);
				Assert.fail();
			}	
			}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ METHOD CAPTURE ON SCRIPT FAILURE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	    public static void failure(String testcasename) throws Exception{
			String currentURL=driver.getCurrentUrl();
			ErrorList.add("Failed while performing " + testcasename +" ("+currentURL + ")");
			verifyServerResponse();
			System.out.println(":=> Failed while performing "+ testcasename +" ("+currentURL + ")");
			APP_LOGS.debug(":=> Failed while performing "+ testcasename +" ("+currentURL + ")");
			System.out.println("Capturing the screenshot");
			APP_LOGS.debug("Capturing the screenshot");
			captureScreenShot(sitename + "_" + envname + "_");
			System.out.println("Capturing the response headers on the page");
			APP_LOGS.debug("Capturing the response header on the page");
			captureHeaders(sitename + "_" + envname + "_");
	    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	}  
