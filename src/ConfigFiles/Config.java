package ConfigFiles;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.varia.NullAppender;
import org.apache.log4j.LogManager;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.saucelabs.saucerest.SauceREST;

import javax.imageio.ImageIO;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

//import Scripts.CrossBrowsers;

public class Config {
	//public static RemoteWebDriver driver;
	public static WebDriver driver;
	
	public static String testcasename = "";
	public static String scriptStatus = "";
	public static String nodeInfo;
	public static Properties LocatorProps;
	public static Properties LogProps;
	public static String sitename;
	public static String envname;
	public static String nodeURLname;
	public static String hostname;
	public static String browsername;
	public static int priority=3;
	public static File srcFile;
	public static File headerFile;
	public static double pageLoadStartTime;
	public static double pageLoadEndTime;
	public static double scriptStartTime;
	public static double scriptEndTime;
	public static double scriptExecutionTime;
	public static String sExecutionTime;
	public static Logger APP_LOGS;
	public static ArrayList<String> loginCredsAdmin=new ArrayList<String>();
	public static String customLogMessage="";
	public static ArrayList<String> SiteURLs=new ArrayList<String>();
	public static ArrayList<String> ErrorList =new ArrayList<String>();
	public static ArrayList<String> hardErrors=new ArrayList<String>();
	public static List<String> attachments = new ArrayList<String>();
	public static File[] files = new File[2];
	
	public static Workbook w; 
	public static Sheet sheet;
	public static String siteURLsExcel = "C:/Saucelabs_WD_Ilse/testdata/SiteUrls.xlsx";
	public static String executionResultsPath = "C:\\Saucelabs_WD_Ilse\\ExecutionResults\\";
	public static String LIVE_LocatorRep = "C:\\Saucelabs_WD_Ilse\\Locators\\LIVE\\";
	public static String LIVE_BDD_LocatorRep = "\\Saucelabs_WD_Ilse\\Locators\\LIVE\\BDD\\TRTL";
	public static String Magento_LocatorRep = "C:\\Saucelabs_WD_Ilse\\Locators\\MAGENTO\\";
	public static String Mobile_LIVE_LocatorRep = "C:\\Saucelabs_WD_Ilse\\Locators\\MOBILE_LIVE\\";
	public static String Mobile_QA_LocatorRep = "C:\\Saucelabs_WD_Ilse\\Locators\\MOBILE_QA\\";
	public static String QA_LocatorRep = "C:\\Saucelabs_WD_Ilse\\Locators\\QA\\";
	public static String STAGE_LocatorRep = "C:\\Saucelabs_WD_Ilse_Ilse\\Locators\\STAGE\\";
	public static String ACME_LocatorRep = "C:\\Saucelabs_WD_Ilse\\Locators\\ACME\\";
	public static String CMS_LocatorRep = "C:\\Saucelabs_WD_Ilse\\Locators\\CMS\\";
	public static String DEV_LocatorRep = "C:\\Saucelabs_WD_Ilse\\Locators\\DEV\\";
	public static String Mobile_STAGE_LocatorRep = "C:\\Saucelabs_WD_Ilse\\QA\\Locators\\MOBILE_STAGE\\";
	public static String pageloadErrorOrException = "";
	public static String waitForLocatorError = "";
	public static String returnLocatorError = "";
	public static String isElementPresentException = "";
	public static String isElementPresentError = "";
	public static String dropDownElementSelectionError = "";
	public static String failedOR="";
	public static int waitForElement = 90;
	public static ArrayList<String> HardErrorList = new ArrayList<String>(Arrays.asList("Sorry for the inconvenience","Not Found","Bad Gateway","Runtime Error","Server Error","HTTP Error","The requested resource is not found","Server Error in '/' Application","Gateway Timeout","Server not found","Service Unavailable","The requested URL could not be retrieved","The proxy server did not receive a timely response from the upstream server","Request Timeout","Error Code"));	
	public static ArrayList<String> ServerNotFoundErrorList = new ArrayList<String>(Arrays.asList("Server not found","Firefox can't find the server at","The connection was reset","The connection has timed out","The page isn't redirecting properly","The connection was interrupted","Secure Connection Failed","Unable to connect"));
    public static int pageLoadTime = 300;
    public static String s9Space="         ";
    public static String pageSource ="";
    public static String UT = "utag.js";
    public static String CO = "live.js";
	  
  //-----------------------ADMIN DLL Script variables--------------------------

  	public static String globalmod;
  	public static String OR = "C:\\Saucelabs_WD_Ilse\\Locators\\ADMIN_LOCATORS\\";
  	public static String STAGE_OR = "STAGE\\";
  	public static String DEV_OR = "DEV\\";
  	public static String QA_OR = "QA\\";
  	public static ArrayList<String> loginCreds=new ArrayList<String>();
  	public static ArrayList<String> siteCreds=new ArrayList<String>();
  	public static String adminInformationExcel = "C:/Saucelabs_WD_Ilse/testdata/Information.xlsx";
  	public static String adminModulesExcel = "C:/Saucelabs_WD_Ilse/testdata/Admin Modules with OR.xlsx";
  	public static ArrayList<String> adminhardErrorsList = new ArrayList<String>(Arrays.asList("Sorry for the inconvenience", "Not Found", "Bad Gateway", "Server Error"));
  	public static String locator;
  	public static String category;
  	public static Actions act;
  	public static String Adminmessage = "";
  //--------------------------------------------------
    
  	private static final int IMG_WIDTH =850;
	private static final int IMG_HEIGHT =6944;
	public static final int size=1024*3;
	public static String host = "localhost";	
	public static String mailFrom = "noreply@onestop.com";
	public static String password = "mail_123";	
	public static String mailTO[] = { "sergio.guillen@onestop.com","daniel.solis@onestop.com","qa@onestop.com" ,"fernanda.castro@onestop.com"};
	public static String mailCC[] = { "clayton@onestop.com" };
			
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	public static  ArrayList<String> readTestDataFromExcel(String excelName,int sheetNo,String client) throws Exception
	{
		 ArrayList<String> localArrayList=new ArrayList<String>();
			FileInputStream fis = null;
			Workbook workbook =null;
			try {
				fis = new FileInputStream(excelName);
				if(excelName.contains(".xlsx"))
					workbook = new XSSFWorkbook(fis);
				else if (excelName.contains(".xls")) 
					 workbook = new HSSFWorkbook(fis);
				else
					throw new Exception("Excel file should have xlsx or xls extension!");
				
				Sheet sheet = workbook.getSheetAt(sheetNo);
				Iterator<?> rowIter = sheet.rowIterator();
				while (rowIter.hasNext()) {
					Row myRow = (Row) rowIter.next();
					Iterator<?> cellIter = myRow.cellIterator();
					
					while (cellIter.hasNext()) {
						Cell myCell = (Cell) cellIter.next();
						myCell.setCellType(Cell.CELL_TYPE_STRING);		
						String cellvalue = myCell.getStringCellValue();
						
						if(cellvalue.equals(client)){
							localArrayList.add(cellvalue);			// first cell data i.e. client name
								
							while (cellIter.hasNext()) {
									 myCell = (Cell) cellIter.next();
									 myCell.setCellType(Cell.CELL_TYPE_STRING);		
									 cellvalue = myCell.getStringCellValue();
									 localArrayList.add(cellvalue);		// 2nd cell from selected row till last cell in that row
							}
							break;
						}
					}
				}
			} 
			
			finally {
				if (fis != null) {
					fis.close();
				}
			}
			return localArrayList;
		}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	public static String getCurrentTime(){
		SimpleDateFormat sdf = new SimpleDateFormat("dd_MMM_yyyy_hh_mm_ss_aaa(zzz)");
		java.util.Date curDate = new java.util.Date();
		String strDate = sdf.format(curDate);
		String strActDate = strDate.toString();
		return strActDate;
	}
	
	/************Log Info Localhost************/	
	public static String LogInfo(String message) {
		System.out.println(message);
		APP_LOGS.debug(message);
		
		return message;
	}
	
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSOLE LOG ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	public static void createLogFile(String fName){	
		String filenameWithTimestamp = fName + getCurrentTime();
	executionResultsPath = executionResultsPath + filenameWithTimestamp;
		System.setProperty("filename", filenameWithTimestamp);		
		APP_LOGS = Logger.getLogger("AutomationLog");
		org.apache.log4j.BasicConfigurator.configure(new NullAppender());
	
	/*	SimpleDateFormat simpleFormat1 = new SimpleDateFormat("dd-MM-yyyy");
		System.setProperty("filename", simpleFormat1.format(Calendar.getInstance().getTime()+"-Log"));		*/
		

	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~SCREEN SHOT~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	public static void captureScreenShot(String fName) throws Exception{
  		System.out.println("-> Capturing Screenshot");
  		APP_LOGS.debug("-> Capturing Screenshot");
		srcFile=((TakesScreenshot)driver).getScreenshotAs(org.openqa.selenium.OutputType.FILE);
		FileUtils.copyFile(srcFile, new File(executionResultsPath + "\\" + fName + " - " + getCurrentTime() + ".png"));
		attachments.add(srcFile.getAbsolutePath());
		File f=new File(executionResultsPath + "\\" + fName + " - " + getCurrentTime() + ".png");
		String originalf = executionResultsPath + "\\" + fName + " - " + getCurrentTime() + ".png";
		String compressf= executionResultsPath + "\\" + fName + " - " + getCurrentTime() + ".png";
		
		if(f.exists()){
		    double bytes = f.length();
			double kb=(bytes/1024);
			
			if(kb>size){
				System.out.println("kilobytes : " + kb);
				
				try{
					BufferedImage originalImage = ImageIO.read(new File(originalf));
					int type = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
					BufferedImage resizeImagePng = resizeImage(originalImage, type);
					ImageIO.write(resizeImagePng, "png", new File(compressf)); 
				}
		
				catch(IOException e){
					System.out.println(e.getMessage());
				}
			}
		}
	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    private static BufferedImage resizeImage(BufferedImage originalImage, int type){
		BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
		g.dispose();
		
		return resizedImage;
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	public static void captureHeaders(String fName) throws Exception{
		System.out.println("--> Capturing Response Headers of page");
  		
  		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(driver.getCurrentUrl());
		HttpResponse response = client.execute(request);
			
		Header[] headers = response.getAllHeaders();
		
		for (Header header : headers) {
			APP_LOGS.debug("Key : " + header.getName() + " ,Value : " + header.getValue());

			PrintWriter pw = null;
			
			try {
				headerFile = new File(executionResultsPath+"\\"+fName + "HeaderFile - "+getCurrentTime()+".txt");
				FileWriter fw = new FileWriter(headerFile, true);
				pw = new PrintWriter(fw);
				pw.println("Key : " + header.getName() 
				      + " ,Value : " + header.getValue());
			} 
			
			catch (IOException e) {
			     e.printStackTrace();
			} 
			
			finally {
				if (pw != null) {
					pw.close();
			    }
			 }
		}
			
			APP_LOGS.debug("--> Getting the Node Info");
			
			nodeInfo = response.getFirstHeader("X-OS-Node").getValue();
			APP_LOGS.debug("--> Node info for the failed script: " + nodeInfo);
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void verifyServerResponse() throws NullPointerException{
		String sPageSource="";
		boolean hardError = false;
		boolean saucelabsError = false;
	
		try{
		    System.out.println("--> Verifying hard errors on the page");
			APP_LOGS.debug("--> Verifying hard errors on the page");
			
			sPageSource=driver.getPageSource();
			
			if(sPageSource.contains("Server not found"))
			{
				for (int i = 0; i < ServerNotFoundErrorList.size(); i++) {
					if (sPageSource.contains(ServerNotFoundErrorList.get(i)))
					{
						saucelabsError=true;
						String currentURL = driver.getCurrentUrl();
						System.out.println("***** ERROR: Saucelabs Network may be down. PLEASE REFER SCREENSHOT AND VERIFY!!");
						APP_LOGS.debug("***** ERROR: Saucelabs Network may be down. PLEASE REFER SCREENSHOT AND VERIFY!!");
						ErrorList.add("-> FAIL!:Saucelabs Network may be down. PLEASE REFER SCREENSHOT AND VERIFY!!");
						break;
					}
					}
			}
			else
			{
				if (saucelabsError!=true)
				{
					System.out.println("--> No Sauce-lab connection errors found.");
					APP_LOGS.debug("--> No Sauce-lab connection errors found.");
				}
			
				for (int i = 0; i < HardErrorList.size(); i++) {
				if (sPageSource.contains(HardErrorList.get(i)))
				{
					hardError=true;
					String currentURL = driver.getCurrentUrl();
					System.out.println("***** ERROR: "+HardErrorList.get(i)+" HARD error found on "+currentURL+" Page");
					APP_LOGS.debug("***** ERROR: "+HardErrorList.get(i)+" HARD error found on "+currentURL+" Page");
					ErrorList.add("-> FAIL!: "+HardErrorList.get(i)+" HARD error found on "+currentURL+" Page while performing " + testcasename);
					//sendTextAlert();
					break;
				}
				}
			if (hardError!=true){
				System.out.println("--> No Hard errors found in the page source");
				APP_LOGS.debug("--> No Hard errors found in the page source");
				}
			}
		}
			catch(Exception e){
			System.out.println("--> WARNING: Unable to verify hard errors on the page");
			APP_LOGS.debug("--> WARNING: Unable to verify hard errors on the page");
			}
		}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void verifyServerResponseOnCMS() throws NullPointerException{
    	String sPageSource="";
		boolean hardError = false;
		try{
		    System.out.println("Verifying Hard Errors on the page");
			APP_LOGS.debug("Verifying Hard Errors on the page");
			
			sPageSource=driver.getPageSource();
			for (int i = 0; i < HardErrorList.size(); i++) {
				if (sPageSource.contains(HardErrorList.get(i)))
				{
					hardError=true;
					String currentURL = driver.getCurrentUrl();
					System.out.println("Inside if ....Checking hard error.!!!");
					System.out.println("***** ERROR: "+HardErrorList.get(i)+" HARD error found on "+currentURL+" Page");
					APP_LOGS.debug("***** ERROR: "+HardErrorList.get(i)+" HARD error found on "+currentURL+" Page");
					ErrorList.add("-> FAIL!: "+HardErrorList.get(i)+" HARD error found on "+currentURL+" Page while performing " + testcasename);
					//sendTextAlert();
					break;
				}
				}
			
			if (hardError!=true){
				System.out.println("--> No Hard Errors found in the page source");
				APP_LOGS.debug("--> No Hard Errors found in the page source");
				}
		}
			catch(Exception e){
			System.out.println("--> WARNING: Unable to verify Hard Errors on the page");
			APP_LOGS.debug("--> WARNING: Unable to verify Hard Errors on the page");
			}
		}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    public static void emailingSpecifications(String host, final String userName, final String password, String toAddress[], String ccAddress[], String subject, String message,  List<String> attachFiles,int priority)
			throws AddressException, MessagingException {
		
		Properties properties = new Properties();
		//properties.setProperty("localhost","25");
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.user", userName);
		properties.put("mail.password", password);

		Authenticator auth = new Authenticator() {
			public PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(userName, password);
			}
		};
		Session session = Session.getInstance(properties, auth);

		// Logic to create a new email
		Message msg = new MimeMessage(session);

		msg.setFrom(new InternetAddress(userName));
		
		for(int i = 0; i < toAddress.length; i++){
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress[i]));
			System.out.println("--> Sending Email to TO:" + toAddress[i]);
			APP_LOGS.debug("--> Sending Email to TO:" + toAddress[i]);
		}
		
		for(int i = 0; i < ccAddress.length; i++){
			msg.addRecipient(Message.RecipientType.CC, new InternetAddress(ccAddress[i]));	
			System.out.println("--> Sending Email to CC:" + ccAddress[i]);
			APP_LOGS.debug("--> Sending Email to CC:" + ccAddress[i]);
		}
			
		msg.setSubject(subject);
		msg.setSentDate(new Date());

		// Logic to create a text message
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setContent(message, "text/html");

		// Logic to create a Multi-part
		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);
		
		
		// Logic to set multi-part as email content
		msg.setContent(multipart);
		
		// Logic to add attachments to email
		if (attachFiles != null && attachFiles.size() > 0) {
			for (String filePath : attachFiles) {
				MimeBodyPart attachPart = new MimeBodyPart();

				try {
					attachPart.attachFile(filePath);
				} catch (IOException ex) {
					ex.printStackTrace();
				}

				multipart.addBodyPart(attachPart);
			}
		}

		// Logic to send the email as high-prioirity
		msg.setHeader("X-Priority", ""+priority);
		
		// Logic to send the email
		Transport.send(msg);
	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//		
	public static void deleteOldFiles() {
		files[0] = new File(executionResultsPath);
		files[1] = new File(executionResultsPath);

		for (int i = 0; i <= files.length-1; i++) {
			if (!files[i].exists()) {
			} else {
				try {
					delete(files[i]);
				} catch (IOException e) {
					e.printStackTrace();
					System.exit(0);
				}
			}
		}
	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	public static void delete(File file) throws IOException {
		if (file.isDirectory()) {
			if (file.list().length == 0) {
				file.delete();
			} else {
				String files[] = file.list();
				for (String temp : files) {
					File fileDelete = new File(file, temp);
					delete(fileDelete);
				}
				if (file.list().length == 0) {
					file.delete();
				}
			}
		} else {
			file.delete();
		}
	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	public static void sendEmail() throws MessagingException {
		String emailStatus = "";
		String failureSteps = "";
		String message;
		String subject = "";
		String mod = "";
		
		if(ErrorList.size()>0){
			failureSteps="<b> Test failed at below methods: </b>";
			if(ErrorList.size()>0)
			{
				emailStatus = ErrorList.get(ErrorList.size()-1);
			}
			for(int i=0;i<ErrorList.size();i++){
				failureSteps=failureSteps+"<br><"+(i+1)+"> "+ErrorList.get(i)+"</br>";
			}
				priority=1;
		}
		else if("Automation Test Successful".equals(scriptStatus)) {
			
			emailStatus = "PASS : Automation Test Successful";
		}
		else if(Adminmessage.equals("Admin_E2EScenario")){
			emailStatus = "Admin Script Unable to Run!- Please check OR/TEST DATA files in framework.";
		}else{
			emailStatus = "Saucelabs Test Time-out Error!- Error communicating with the remote browser and WebDriver. Both may have died";
			
		}
			
		// Email Specifications
		String testingSite = "Client: " + sitename;
		String env = "Test Environment: " + envname;
		String ostype = "OS: Windows 10.0";
		String browser = browsername;
		String executionTime = "Script Execution Time: " + sExecutionTime;
		if(Adminmessage.contains("Admin_E2EScenario"))
		{
			subject = "Admin_Execution_" + sitename + "_" + envname + " : " + emailStatus;
		}
		else
		{
			subject = "SauceLabs_" + sitename + "_" + envname + " : " + emailStatus;
		}
		message = "This is an automated message. ***Do not reply.***<br /><br />" +""
		+ "<br> <b> SauceLabs Automation Test Execution Details:  </b> <br />" + " Execution Time: " + getCurrentTime()
				+ "<br />" + testingSite + "<br />" + env + "<br />" + mod + "<br />"+ ostype + "<br />"
				+ browser + "<br />" + executionTime + "<br /><br />" + failureSteps + "<br />"
				+ " <b> QA Automation Team";
		
		List<String> attachments = new ArrayList<String>();
		File[] files = new File(executionResultsPath).listFiles();

		for (File file : files) {
		    if (file.isFile()) {
		    	attachments.add(file.getAbsolutePath());
		    }
		}
		
		try {
			Config.emailingSpecifications(host, mailFrom, password, mailTO, mailCC, subject, message, attachments, priority);
			System.out.println("--> Email sent successfully");
			APP_LOGS.debug("--> Email sent successfully");
		} catch (Exception ex) {
			System.out.println("--> Could not send email");
			APP_LOGS.debug("--> Could not send email");
			ex.printStackTrace();
		}
	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	public static void sendTextAlert() throws MessagingException {

			System.out.println("--> Sending the text alert");
			APP_LOGS.debug("--> Sending the text alert");
			
			String textStatus = "Hard error found on: " + testcasename;
			String textMessage = "Page URL: " + driver.getCurrentUrl();
			String HeaderNode = "Node Info: " + nodeInfo;
			
			String mailTO[] = {"automation-alerts-phone@onestop.com"};
			String mailCC[] = {"automation1@onestop.com"};

			String subject = "TextAlert: SauceLabs_" + sitename + "_" + envname;
			
			String message = "<br> <b>"  + textStatus + "<br />  </b> <br />"
			+ "<br> <b>"  + textMessage + "<br />  </b> <br />"
			+ "<br> <b>"  + HeaderNode + "<br />  </b> <br />"
			+ "<br> Execution Time: " + getCurrentTime();
			
			try {
				Config.emailingSpecifications(host, mailFrom, password, mailTO, mailCC, subject, message, attachments, priority);
				System.out.println("--> Text message sent successfully");
				APP_LOGS.debug("--> Text message sent successfully");
			} catch (Exception ex) {
				System.out.println("--> Could not send the text message");
				APP_LOGS.debug("--> Could not send the text message");
				ex.printStackTrace();
			}
		}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
     public static WebElement waitForLocator(String ObjectIdentifier) throws Exception {
		
		WebElement element = null;
		String objectIdentifierType="";
		String objectIdentifierValue="";
		String objectArray[]=null;
		WebDriverWait wait = new WebDriverWait(driver, waitForElement);
		String printApplog="";
		String errorArray[]=null;
		
		try {
			String object = LocatorProps.getProperty(ObjectIdentifier);
			objectArray = object.split("__");
			objectIdentifierType=objectArray[0].trim();
			objectIdentifierValue=objectArray[1].trim();

  			System.out.println(s9Space+"Executing waitForLocator method for "+ObjectIdentifier);
  			APP_LOGS.debug(s9Space+"Executing waitForLocator method for "+ObjectIdentifier);
			
			switch (objectIdentifierType) {
			case "id":
				element=wait.until(ExpectedConditions.presenceOfElementLocated(By.id(objectIdentifierValue)));
				Thread.sleep(5000);
				break;
			case "className":
				element=wait.until(ExpectedConditions.presenceOfElementLocated(By.id(objectIdentifierValue)));
				Thread.sleep(5000);
				break;
			case "cssSelector":
				element=wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(objectIdentifierValue)));
				Thread.sleep(5000);		
				break;
			case "linkText":
				element=wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(objectIdentifierValue)));
				Thread.sleep(5000);
				break;
			case "xpath":
				element=wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(objectIdentifierValue)));
				Thread.sleep(5000);
				break;
			case "name":
				element=wait.until(ExpectedConditions.presenceOfElementLocated(By.name(objectIdentifierValue)));
				Thread.sleep(5000);
				break;
			}
		} catch (Exception e) {
			waitForLocatorError=e.getMessage();
			APP_LOGS.debug(waitForLocatorError);
			failedOR=ObjectIdentifier;
			errorArray=waitForLocatorError.split("Build");
			waitForLocatorError=errorArray[0];
			printApplog=("***** Error: OR ISSUE with "+ObjectIdentifier+ " Object!. " + "Unable to locate it @waitForLocator method and "+waitForLocatorError);
			System.out.println(printApplog);
			APP_LOGS.debug(printApplog);
			failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError, dropDownElementSelectionError);
			Assert.fail();

			return null;
		}
		return element;
	}
     
   //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
     public static WebElement selectValueByVisibleText(WebElement inpWebElement,String inpStrLocator ) throws Exception {
   		
   		String strLocatorValue="";
   		String printApplog="";
  		String errorArray[]=null;
  		Select objSelect = null;
   				
   		try {
	   			String object = LocatorProps.getProperty(inpStrLocator);
	   			strLocatorValue = object.trim();
    			System.out.println(s9Space+"Executing selectValueByVisibleText method for "+inpStrLocator);
    			APP_LOGS.debug(s9Space+"Executing selectValueByVisibleText method for "+inpStrLocator);
    			   			
   				objSelect = new Select( inpWebElement );								
				objSelect.selectByVisibleText( strLocatorValue );
				Thread.sleep(250);	
				
				System.out.println(s9Space+"Visible Text selected "+inpStrLocator + " = " + strLocatorValue);
    			APP_LOGS.debug(s9Space+"Visible Text selected "+inpStrLocator + " = " + strLocatorValue); 

   		  }//try 
   		  catch (Exception e) {
  			returnLocatorError=e.getMessage();
  			APP_LOGS.debug(returnLocatorError);
  			failedOR=inpStrLocator;
  			errorArray=returnLocatorError.split("Build");
  			returnLocatorError=errorArray[0];       
  			printApplog=("***** Error: OR ISSUE with "+inpStrLocator+ " Object!. " + "Unable to locate it @selectValueByVisibleText method and "+returnLocatorError);
  			System.out.println(printApplog);
  			APP_LOGS.debug(printApplog);
  			failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError, dropDownElementSelectionError);
  			Assert.fail();
   			return null;
   		}
   		return inpWebElement;
   	}
     
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
   public static WebElement returnLocator(String ObjectIdentifier) throws Exception {

 		WebElement elements = null;
 		String objectIdentifierType="";
 		String objectIdentifierValue="";
 		String objectArray[]=null;
 		String printApplog="";
		String errorArray[]=null;
 				
 		try {
 			String object = LocatorProps.getProperty(ObjectIdentifier);
 			objectArray= object.split("__");
 			objectIdentifierType=objectArray[0].trim();
 			objectIdentifierValue=objectArray[1].trim();
 			
  			System.out.println(s9Space+"Executing returnLocator method for "+ObjectIdentifier);
  			APP_LOGS.debug(s9Space+"Executing returnLocator method for "+ObjectIdentifier);
  			
 			switch (objectIdentifierType) {
 			case "id":
 				elements=driver.findElement(By.id(objectIdentifierValue));
 				Thread.sleep(250);
 				break;
 			case "cssSelector":
 				elements=driver.findElement(By.cssSelector(objectIdentifierValue));
 				Thread.sleep(250);
 				break;
 			case "linkText":
 				elements=driver.findElement(By.linkText(objectIdentifierValue));
 				Thread.sleep(250);
 				break;
 			case "xpath":
 				elements=driver.findElement(By.xpath(objectIdentifierValue));
 				Thread.sleep(250);
 				break;
 			case "name":
 				elements=driver.findElement(By.name(objectIdentifierValue));
 				Thread.sleep(250);
 				break;
 			case "className":
 				elements=driver.findElement(By.className(objectIdentifierValue));
 				Thread.sleep(250);
 				break;	
 			}

 		} catch (Exception e) {
			returnLocatorError=e.getMessage();
			APP_LOGS.debug(returnLocatorError);
			failedOR=ObjectIdentifier;
			errorArray=returnLocatorError.split("Build");
			returnLocatorError=errorArray[0];
			printApplog=("***** Error: OR ISSUE with "+ObjectIdentifier+ " Object!. " + "Unable to locate it @returnLocator method and "+returnLocatorError);
			System.out.println(printApplog);
			APP_LOGS.debug(printApplog);
			failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError, dropDownElementSelectionError);
			Assert.fail();
 			return null;
 		}
 		return elements;
 	}

 //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
   
  public static boolean isElementPresent(String ObjectIdentifier) throws Exception{
		String objectIdentifierType="";
		String objectIdentifierValue="";
		String objectArray[]=null;
		WebDriverWait wait = new WebDriverWait(driver, waitForElement);
 		String printApplog="";
		String errorArray[];
		
		try {
			String object = LocatorProps.getProperty(ObjectIdentifier);
			objectArray = object.split("__");
			objectIdentifierType=objectArray[0].trim();
			objectIdentifierValue=objectArray[1].trim();

  			System.out.println(s9Space+"Executing isElementPresent method for "+ObjectIdentifier);
  			APP_LOGS.debug(s9Space+"Executing isElementPresent method for "+ObjectIdentifier);
			
			switch (objectIdentifierType) {
			case "id":
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(objectIdentifierValue)));
				Thread.sleep(250);
				break;
			case "cssSelector":
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(objectIdentifierValue)));
				Thread.sleep(250);		
				break;
			case "linkText":
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(objectIdentifierValue)));
				Thread.sleep(250);
				break;
			case "xpath":
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(objectIdentifierValue)));
				Thread.sleep(250);
				break;
			case "name":
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(objectIdentifierValue)));
				Thread.sleep(250);
				break;
			case "className":
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(objectIdentifierValue)));
 				Thread.sleep(250);
 				break;	
			}
		} catch (Exception e) {
			isElementPresentException=e.getMessage();
			//APP_LOGS.debug(isElementPresentException);
			if (isElementPresentException!=null)
				{
				isElementPresentError=isElementPresentException;
				failedOR=ObjectIdentifier;
				errorArray=isElementPresentError.split("Build");
				isElementPresentError=errorArray[0];	
				printApplog=("***** Warning: OR ISSUE with "+ObjectIdentifier+ " Object!. " + "Unable to locate it @isElementPresent method and "+isElementPresentError);
				System.out.println(printApplog);
				APP_LOGS.debug(printApplog);
			}
			else
			{
			printApplog=("***** Warning: OR value of "+ObjectIdentifier+ " Element is not present in OR File.");
			System.out.println(printApplog);
			APP_LOGS.debug(printApplog);
			}
			return false;
		}
		
		return true;
	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
		public static boolean isAlertPresent() {
			try 
		    { 
		        driver.switchTo().alert(); 
		        return true; 
			} catch (NoAlertPresentException Ex) {
				return false;
			}
		}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
		
		public static void failure(String testcasename, String pageloadErrorOrException, String waitForLocatorError, String returnLocatorError, String isElementPresentError, String dropDownElementSelectionError) throws Exception{
	 		System.out.println("***** FAILURE OCCURED!!!");
	  		APP_LOGS.debug("***** FAILURE OCCURED!!!");
	  		String currentURL = "";
	  		try{
	  	
	  			
	  		if(pageloadErrorOrException.contains("Timed out waiting for page load"))
	  		{
	  			System.out.println("***** INSIDE pageloadErrorOrException");
	  			APP_LOGS.debug("***** INSIDE pageloadErrorOrException");
	  			currentURL = driver.getCurrentUrl();
	  			System.out.println("***** PAGE LOAD ISSUE!. "+pageloadErrorOrException);
	  			APP_LOGS.debug("***** PAGE LOAD ISSUE!. "+pageloadErrorOrException);
	  			ErrorList.add("-> FAIL!: PAGE LOAD ISSUE while performing " + testcasename +" in "+pageLoadTime+ " sec ("+currentURL + ")");
	  		}
	  		//---
	  		else if(pageloadErrorOrException.contains("not found in the cache"))
	  		{
	  			System.out.println("***** INSIDE pageloadErrorOrException");
	  			APP_LOGS.debug("***** INSIDE pageloadErrorOrException");
	  			currentURL = driver.getCurrentUrl();
	  			System.out.println("***** Web Element not found in the cache - perhaps the page has changed since it was looked up");
	  			APP_LOGS.debug("***** Web Element not found in the cache - perhaps the page has changed since it was looked up");
	  			ErrorList.add("-> Web Element/Web Page SYNC issue!- Element not found in the cache - perhaps the page has changed since it was looked up. @" + testcasename+ " @"+"("+currentURL+")");
	  		}
	  		else if(pageloadErrorOrException.contains("communicating with the remote browser"))
	  		{
	  			System.out.println("***** INSIDE pageloadErrorOrException");
	  			APP_LOGS.debug("***** INSIDE pageloadErrorOrException");
	  			currentURL = driver.getCurrentUrl();
	  			System.out.println("***** Error communicating with the remote browser. It may have died.");
	  			APP_LOGS.debug("***** Error communicating with the remote browser. It may have died.");
	  			ErrorList.add("-> Saucelabs Test Time-out Error!- Error communicating with the remote browser. It may have died. @" + testcasename+ " @"+"("+currentURL+")");
	  		}
	  		
	  		
	  		//---
	  		else if(waitForLocatorError.contains("Timed out after "+ waitForElement +" seconds waiting for presence of element"))
	  		{
	  			System.out.println("***** INSIDE waitForLocatorError");
	  			APP_LOGS.debug("***** INSIDE waitForLocatorError");
	  			currentURL = driver.getCurrentUrl();
	  			System.out.println("***** OR ISSUE! with "+failedOR+" identifier. "+waitForLocatorError);
	  			APP_LOGS.debug("***** OR ISSUE! with "+failedOR+" identifier. "+waitForLocatorError);
	  			ErrorList.add("-> FAIL!: OR ISSUE with "+failedOR+" Identifier. @"+ testcasename +" Method"+" ("+currentURL + ")");
	  		}
	  		else if(returnLocatorError.contains("Unable to locate"))
	  		{
	  			System.out.println("***** INSIDE returnLocatorError");
	  			APP_LOGS.debug("***** INSIDE returnLocatorError");
	  			currentURL = driver.getCurrentUrl();
	  			System.out.println("***** OR ISSUE! with "+failedOR+" identifier. "+returnLocatorError);
	  			APP_LOGS.debug("***** OR ISSUE! with "+failedOR+" identifier. "+returnLocatorError);
	  			ErrorList.add("-> FAIL!: OR ISSUE with "+failedOR+" Identifier. @"+ testcasename +" Method"+" ("+currentURL + ")");
	  		}
	  		else if(dropDownElementSelectionError.contains("Cannot locate"))
	  		{
	  			System.out.println("***** INSIDE dropDownElementSelectionError");
	  			APP_LOGS.debug("***** INSIDE dropDownElementSelectionError");
	  			currentURL = driver.getCurrentUrl();
	  			System.out.println("***** OR ISSUE! with "+failedOR+" identifier. "+dropDownElementSelectionError);
	  			APP_LOGS.debug("***** OR ISSUE! with "+failedOR+" identifier. "+dropDownElementSelectionError);
	  			ErrorList.add("-> FAIL!: OR ISSUE with "+failedOR+" Identifier. @"+ testcasename +" Method"+" ("+currentURL + ")");
	  		}
	  		else if(isElementPresentError.contains("Timed out after "+ waitForElement +" seconds waiting for visibility of element"))
	  		{
	  			System.out.println("***** INSIDE isElementPresentError");
	  			APP_LOGS.debug("***** INSIDE isElementPresentError");
	  			currentURL = driver.getCurrentUrl();
	  			System.out.println("***** OR ISSUE! with "+failedOR+" identifier. "+isElementPresentError);
	  			APP_LOGS.debug("***** OR ISSUE! with "+failedOR+" identifier. "+isElementPresentError);
	  			ErrorList.add("-> FAIL!: OR ISSUE with "+failedOR+" Identifier. @"+ testcasename +" Method"+" ("+currentURL + ")");
	  		}
	  		else{
	  			System.out.println("***** INSIDE defaultelsecase");
	  			APP_LOGS.debug("***** INSIDE defaultelsecase");
	  			currentURL = driver.getCurrentUrl();
	  			System.out.println("in else...");
	  		 	ErrorList.add("-> FAIL!: ERROR: "+pageloadErrorOrException +" "+ testcasename +" ("+currentURL + ")");
	  		}
	  		
	  		verifyServerResponse();
	  		captureScreenShot(sitename + "_" + envname + "_");
	  		captureHeaders(sitename + "_" + envname + "_");
	  		System.out.println("***** Failed while performing "+ testcasename +" ("+currentURL + ")");
	  		APP_LOGS.debug("***** Failed while performing "+ testcasename +" ("+currentURL + ")");
	  		//sendTextAlert();
	  	}
		catch(Exception e)
		{
			System.out.println(e.getMessage());		
			}
			}
		
		
//***********************************************Admin Methods*******************************************************************
		//---------------------------verifyAdminUrlForErrors Method------------------------------------------------------------
		
		public static String verifyAdminUrlForErrors() throws Exception{
			/* @HELP
			@class:				Config
			@method:			verifyUrlAndHardErrors ()
			@parameter:			None
			@notes:				Verifies URL & Hard errors. This method is being called in several methods from keywords package.
			@returns:			None
			@END
			 */
			
			System.out.println(customLogMessage="ACTION: Verifying Errors in the URL");
			APP_LOGS.debug(customLogMessage);
			
			try{
			String pUrl=driver.getCurrentUrl();
			if(!pUrl.contains("error")||!pUrl.contains("fm = 1")){
				System.out.println(customLogMessage="RESULT: PASS - No errors found in the URL");
				APP_LOGS.debug(customLogMessage);
			} else{
				failure(testcasename, pageloadErrorOrException, waitForLocatorError, returnLocatorError, isElementPresentError, dropDownElementSelectionError);
				System.out.println(customLogMessage="\nRESULT: FAIL - Error found in the URL");
				APP_LOGS.debug(customLogMessage);
				return "FAIL";	
			}
			}catch(Exception e){
				System.out.println(customLogMessage="\nRESULT: FAIL - Unable to perform URL error verification");
				APP_LOGS.debug(customLogMessage);
				return "FAIL";	
			}
			return "PASS";
		}
		
		//---------------------------verifyAdminHardErrors Method------------------------------------------------------------------
		
		public static String verifyAdminHardErrors() throws Exception{
			/* @HELP
			@class:				Config
			@method:			verrifyHardErrors ()
			@parameter:			None
			@notes:				Verifies URL & Hard errors. This method is being called in several methods from keywords package.
			@returns:			None
			@END
			 */
			boolean hardError = false;
			System.out.println(customLogMessage="ACTION: Verifying Hard Errors on the page");
			APP_LOGS.debug(customLogMessage);
			
			try{
			String pageSrc=driver.getPageSource();
			for (int i = 0; i < adminhardErrorsList.size(); i++) {
				if (pageSrc.contains(adminhardErrorsList.get(i)))
				{
					hardError=true;
					String currentURL = driver.getCurrentUrl();
					System.out.println("***** ERROR: "+adminhardErrorsList.get(i)+" HARD error found on "+currentURL+" Page");
					APP_LOGS.debug("***** ERROR: "+adminhardErrorsList.get(i)+" HARD error found on "+currentURL+" Page");
					captureScreenShot(sitename+"_"+envname+"_");
			  		captureHeaders(sitename + "_" + envname + "_");
					driver.navigate().back();
					ErrorList.add("-> FAIL! : "+adminhardErrorsList.get(i)+" HARD error found on "+currentURL+" Page while performing " + testcasename);
					//ErrorList.add("-> FAIL! : Please Check the Execution Log Details.");
					break;
				}
				}
			if (hardError!=true){
				System.out.println("--> No Hard errors found in the page source");
				APP_LOGS.debug("--> No Hard errors found in the page source");
				}
			}catch(Exception e){
				System.out.println(customLogMessage="\nRESULT: FAIL - Unable to perform Hard errors verification");
				APP_LOGS.debug(customLogMessage);
				return "FAIL";	
			}
			return "PASS";
		}
		//--------------------------Move To Element Method------------------------------------------------------------------
		public static void moveToElement(String locatorMove) throws Exception {
			Actions	action = new Actions(driver);
			String strMove = locatorMove;
			action.moveToElement(returnLocator(strMove));
			action.perform();	
			System.out.println("Moving to Locator");
			APP_LOGS.debug("Moving to Locator");
		}
		
		//---------------------------single MouseHover Method------------------------------------------------------------------
		public static void singleMouseHover(String adminModule, String adminLocator) throws Exception {
			WebElement root=returnLocator(adminModule);
			act.moveToElement(root).moveToElement(driver.findElement(By.id(adminLocator))).click().build().perform();
			
		}
		//---------------------------double MouseHover Method------------------------------------------------------------------
		public static void doubleMouseHover(String adminModule, String adminSubModule, String adminLocator) throws Exception {
			
			WebElement root1=returnLocator(adminModule);
			act.moveToElement(root1).click().build().perform();
			Thread.sleep(2000);
			WebElement root2=returnLocator(adminSubModule);
			Thread.sleep(2000);
			act.moveToElement(root2).click().build().perform();
			act.moveToElement(driver.findElement(By.id(adminLocator))).click().build().perform();
		}
		
		
		//---------------------------CloseFancybox Method------------------------------------------------------------------
				public static void closeFancybox() throws Exception {		
					System.out.println("Closing Fancybox!");
					APP_LOGS.debug("Closing Fancybox!");
					((RemoteWebDriver) driver).getKeyboard().sendKeys(Keys.ESCAPE);
				}

		//---------------------------Clear Cookies Method------------------------------------------------------------------
				public static void clearCookies() throws Exception {		
					//LogInfo("Deleting all the cookies!");
					System.out.println("Deleting all the cookies!");
					APP_LOGS.debug("Deleting all the cookies!");
					driver.manage().deleteAllCookies();

				}
			
		//---------------------------Print Messages Method------------------------------------------------------------------
				public static void printConsoleAndLog(String message) {
					System.out.println(message);
					APP_LOGS.debug(message);
				}
				
}