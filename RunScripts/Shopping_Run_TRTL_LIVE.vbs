Dim oShell,objWMIService, objProcess, colProcess, strComputer, strProcessKill
strComputer = "."
strProcessKill = "'cmd.exe'"
Set oShell = WScript.CreateObject("WScript.Shell")

REM WScript.Echo oShell.CurrentDirectory
oShell.CurrentDirectory = "C:\Saucelabs_WD"

REM To compile all Java files
oShell.run "cmd /K compile.bat"
'WScript.Sleep 10000

oShell.CurrentDirectory = "C:\Saucelabs_WD\testngxml"
REM To start running scripts
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=TRTL -Denv=LIVE org.testng.TestNG LIVE_testng.xml",2,FALSE
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=TRUK -Denv=LIVE org.testng.TestNG LIVE_testng.xml",2,FALSE

WScript.Sleep 600000
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=TRDE -Denv=LIVE org.testng.TestNG LIVE_testng.xml",2,FALSE
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=TRAU -Denv=LIVE org.testng.TestNG LIVE_testng.xml",2,FALSE

WScript.Sleep 600000
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=TRIT -Denv=LIVE org.testng.TestNG LIVE_testng.xml",2,FALSE
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=TRFR -Denv=LIVE org.testng.TestNG LIVE_testng.xml",2,FALSE

WScript.Sleep 600000
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=TRES -Denv=LIVE org.testng.TestNG LIVE_testng.xml",2,FALSE
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=TRNL -Denv=LIVE org.testng.TestNG LIVE_testng.xml",2,FALSE


WScript.Sleep 600000
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=TREU -Denv=LIVE org.testng.TestNG LIVE_testng.xml",2,FALSE
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=TRNZ -Denv=LIVE org.testng.TestNG LIVE_testng.xml",2,FALSE


'Close all command prompt windows
Set objWMIService = GetObject("winmgmts:" & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
Set colProcess = objWMIService.ExecQuery ("Select * from Win32_Process Where Name = " & strProcessKill)
For Each objProcess in colProcess
	objProcess.Terminate()	
Next

WScript.Quit