Dim oShell,objWMIService, objProcess, colProcess, strComputer, strProcessKill
strComputer = "."
strProcessKill = "'cmd.exe'"
Set oShell = WScript.CreateObject("WScript.Shell")

REM WScript.Echo oShell.CurrentDirectory
oShell.CurrentDirectory = "C:\Saucelabs_WD"

REM To compile all Java files
oShell.run "cmd /K compile.bat"
WScript.Sleep 10000

oShell.CurrentDirectory = "C:\Saucelabs_WD\testngxml"
REM To start running scripts
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=FRYE -Denv=LIVE org.testng.TestNG FRYE_ExtraTCs_testng.xml",2,FALSE
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=AQUA -Denv=LIVE org.testng.TestNG LIVE_testng.xml",2,FALSE
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=JCTR -Denv=LIVE org.testng.TestNG LIVE_testng.xml",2,FALSE
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=JOES -Denv=LIVE org.testng.TestNG LIVE_testng.xml",2,FALSE

WScript.Sleep 600000
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=JONY -Denv=LIVE org.testng.TestNG LIVE_testng.xml",2,FALSE
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=KATY -Denv=LIVE org.testng.TestNG LIVE_testng.xml",2,FALSE
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=LYCA -Denv=LIVE org.testng.TestNG LIVE_testng.xml",2,FALSE
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=NYDJ -Denv=LIVE org.testng.TestNG LIVE_testng.xml",2,FALSE

WScript.Sleep 600000
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=PAIG -Denv=LIVE org.testng.TestNG LIVE_testng.xml",2,FALSE
'oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=SEVN -Denv=LIVE org.testng.TestNG LIVE_testng.xml",2,FALSE
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=SPDR -Denv=LIVE org.testng.TestNG LIVE_testng.xml",2,FALSE
'oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=SPLD -Denv=LIVE org.testng.TestNG LIVE_testng.xml",2,FALSE

WScript.Sleep 600000
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=TRYN -Denv=LIVE org.testng.TestNG LIVE_testng.xml",2,FALSE

'Close all command prompt windows
Set objWMIService = GetObject("winmgmts:" & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
Set colProcess = objWMIService.ExecQuery ("Select * from Win32_Process Where Name = " & strProcessKill)
For Each objProcess in colProcess
	objProcess.Terminate()	
Next

WScript.Quit