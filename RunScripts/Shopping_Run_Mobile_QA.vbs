Dim oShell,objWMIService, objProcess, colProcess, strComputer, strProcessKill
strComputer = "."
strProcessKill = "'cmd.exe'"
Set oShell = WScript.CreateObject("WScript.Shell")

REM WScript.Echo oShell.CurrentDirectory
oShell.CurrentDirectory = "C:\Saucelabs_WD"

REM To compile all Java files
oShell.run "cmd /K compile.bat"
WScript.Sleep 10000

oShell.CurrentDirectory = "C:\Saucelabs_WD\testngxml"
REM To start running scripts
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=CAML -Denv=MOBILE_QA org.testng.TestNG Mobile_QA_testng.xml",2,FALSE

WScript.Sleep 400000
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=CBTL -Denv=MOBILE_QA org.testng.TestNG Mobile_QA_testng.xml",2,FALSE
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=HUDS -Denv=MOBILE_QA org.testng.TestNG Mobile_QA_testng.xml",2,FALSE

WScript.Sleep 400000
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=NYDJ -Denv=MOBILE_QA org.testng.TestNG Mobile_QA_testng.xml",2,FALSE
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=PAIG -Denv=MOBILE_QA org.testng.TestNG Mobile_QA_testng.xml",2,FALSE

WScript.Sleep 400000
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=PEBA -Denv=MOBILE_QA org.testng.TestNG Mobile_QA_testng.xml",2,FALSE
oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=SPLD -Denv=MOBILE_QA org.testng.TestNG Mobile_QA_testng.xml",2,FALSE


'Close all command prompt windows
Set objWMIService = GetObject("winmgmts:" & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
Set colProcess = objWMIService.ExecQuery ("Select * from Win32_Process Where Name = " & strProcessKill)
For Each objProcess in colProcess
	objProcess.Terminate()	
Next

WScript.Quit