Dim oShell,objWMIService, objProcess, colProcess, strComputer, strProcessKill
strComputer = "."
strProcessKill = "'cmdlove.exe'"
Set oShell = WScript.CreateObject("WScript.Shell")

REM WScript.Echo oShell.CurrentDirectory
oShell.CurrentDirectory = "C:\Saucelabs_WD"

REM To compile all Java files
oShell.run "cmd /K compile.bat"
WScript.Sleep 5000

oShell.CurrentDirectory = "C:\Saucelabs_WD\testngxml"
REM To start runing scripts

oShell.run "cmd /K java -cp C:\Saucelabs_WD\JarFiles\*;C:\Saucelabs_WD\CompiledJavaFiles -Dsite=NYDJ -Denv=STAGE -Dmodule=All org.testng.TestNG Admin_testng.xml",2,FALSE
WScript.Sleep 3000000

'Close all command prompt windows
Set objWMIService = GetObject("winmgmts:" & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
Set colProcess = objWMIService.ExecQuery ("Select * from Win32_Process Where Name = " & strProcessKill)
For Each objProcess in colProcess
	objProcess.Terminate()	
Next

WScript.Quit